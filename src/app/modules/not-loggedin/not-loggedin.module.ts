import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { ExploreCourseDetailComponent } from 'src/app/components/explore-course-detail/explore-course-detail.component';
//import { ExploreTeacherDetailComponent } from 'src/app/components/explore-teacher-detail/explore-teacher-detail.component';
import { NotLoggedinRoutingModule } from './not-loggedin-routing.module';
import { LandingComponent } from 'src/app/components/landing/landing.component';
import { FeaturesComponent } from 'src/app/components/features/features.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrivacyPolicyComponent } from 'src/app/components/privacy-policy/privacy-policy.component';
import { HelpComponent } from 'src/app/components/help/help.component';
import { from } from 'rxjs';
import { IntlModule } from '@progress/kendo-angular-intl';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
//import { NgxStarsModule } from 'ngx-stars';

@NgModule({
  declarations: [
    LandingComponent,
    FeaturesComponent,
    PrivacyPolicyComponent,
    HelpComponent
    // ExploreTeacherDetailComponent,
    // ExploreCourseDetailComponent
  ],
  imports: [
    CommonModule,
    NotLoggedinRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    IntlModule, DateInputsModule,
    // NgxStarsModule,
  ]
})
export class NotLoggedinModule { }
