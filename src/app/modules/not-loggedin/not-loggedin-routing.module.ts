import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from 'src/app/components/landing/landing.component';
import { FeaturesComponent } from 'src/app/components/features/features.component';
import { PrivacyPolicyComponent } from 'src/app/components/privacy-policy/privacy-policy.component';
import { HelpComponent } from 'src/app/components/help/help.component';
const routes: Routes = [
  { path: '', component: LandingComponent },
  {path : 'privacy-policy' , component : PrivacyPolicyComponent},

  { path: 'features', component: FeaturesComponent },
  { path: 'help', component: HelpComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotLoggedinRoutingModule { }
