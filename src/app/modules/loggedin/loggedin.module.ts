import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoggedinRoutingModule } from './loggedin-routing.module';
import { ScheduleComponent } from 'src/app/components/schedule/schedule.component';
import { CourseComponent } from 'src/app/components/course/course.component';
import { MyTeachersComponent } from 'src/app/components/my-teachers/my-teachers.component';
import { ExploreCourseComponent } from 'src/app/components/explore-course/explore-course.component';
import { ExploreTeacherComponent } from 'src/app/components/explore-teacher/explore-teacher.component';
import { ProfileComponent } from 'src/app/components/profile/profile.component';
import { AccountsComponent } from 'src/app/components/accounts/accounts.component';
import { ExploreCourseDetailComponent } from 'src/app/components/explore-course-detail/explore-course-detail.component';
import { ExploreTeacherDetailComponent } from 'src/app/components/explore-teacher-detail/explore-teacher-detail.component';
import { MyRecordingComponent } from 'src/app/components/my-recording/my-recording.component';
import { MyStudentsComponent } from 'src/app/components/my-students/my-students.component';
import { PaymentStatsComponent } from 'src/app/components/payment-stats/payment-stats.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxStarsModule } from 'ngx-stars';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { IntlModule } from '@progress/kendo-angular-intl';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { ChartsModule } from 'ng2-charts';
import { InvoiceComponent } from 'src/app/components/invoice/invoice.component';

@NgModule({
  declarations: [
    ScheduleComponent,
    CourseComponent,
    MyTeachersComponent,
    ExploreCourseComponent,
    ExploreTeacherComponent,
    MyRecordingComponent,
    ProfileComponent,
    ExploreTeacherDetailComponent,
    AccountsComponent,
    ExploreCourseDetailComponent,
    MyStudentsComponent,
    PaymentStatsComponent,
    InvoiceComponent,
    //FilterPipe,

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    LoggedinRoutingModule,
    CommonModule,
    LoggedinRoutingModule,
   // AngularMultiSelectModule,
   // NgxPrintModule,
    // IntlModule,
    // DateInputsModule,
    NgMultiSelectDropDownModule.forRoot(),
    FormsModule, HttpClientModule, ReactiveFormsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    NgxPaginationModule,
    NgxStarsModule,
    HttpClientModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    IntlModule, DateInputsModule,
    ChartsModule,
  ]
})
export class LoggedinModule { }
