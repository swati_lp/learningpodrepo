import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScheduleComponent } from 'src/app/components/schedule/schedule.component';
import { CourseComponent } from 'src/app/components/course/course.component';
import { MyTeachersComponent } from 'src/app/components/my-teachers/my-teachers.component';
import { ExploreCourseComponent } from 'src/app/components/explore-course/explore-course.component';
import { ExploreTeacherComponent } from 'src/app/components/explore-teacher/explore-teacher.component';
import { ProfileComponent } from 'src/app/components/profile/profile.component';
import { AccountsComponent } from 'src/app/components/accounts/accounts.component';
import { ExploreCourseDetailComponent } from 'src/app/components/explore-course-detail/explore-course-detail.component';
import { ExploreTeacherDetailComponent } from 'src/app/components/explore-teacher-detail/explore-teacher-detail.component';
import { LoginAuthService } from 'src/app/login-auth.service';
import { InvoiceComponent } from 'src/app/components/invoice/invoice.component';


const routes: Routes = [

  //directly used routes

  { path: 'courses', component: ExploreCourseComponent },
  { path: 'profile/invoice', component: InvoiceComponent },
  { path: 'teachers', component: ExploreTeacherComponent },
  { path: 'course', component: ExploreCourseDetailComponent },
  { path: 'teacher', component: ExploreTeacherDetailComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'accounts', component: AccountsComponent },
  { path: 'schedule', component: ScheduleComponent, canActivate: [LoginAuthService] },
  { path: 'mycourses', component: CourseComponent, canActivate: [LoginAuthService] },
  { path: 'affiliation', component: MyTeachersComponent, canActivate: [LoginAuthService] }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoggedinRoutingModule { }
