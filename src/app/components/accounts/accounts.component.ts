import { Component, OnInit, Input, Output } from '@angular/core';
import Swal from 'sweetalert2';
import { CommonServiceService } from '../../common-service.service';
import { MessagingService } from '../../shared/models/messaging.service';
import { LearningpodService } from '../../learningpod.service';
import * as introJs from 'intro.js/intro.js';
import { CBS } from 'src/app/shared/models/CBS.model';
import { EventEmitter } from '@angular/core';
import { Course } from '../../shared/models/course.model';
import { of } from 'rxjs';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {
  picturePath: any;
  ccInfo: any;
  ccInfoToUpdate: any;
  teacherInfo: any;
  teacherInfoToUpdate: any;
  studentInfo: any;
  studentInfoToUpdate: any;
  selectedClass = [];
  diabledButton: boolean;
  selectedClassToUpdate = [];
  selectedSubjects = [];
  selectedBoard = [];
  selectedSubjectsToUpdate = [];
  selectedBoardsToUpdate = [];
  editable: boolean;
  emailerr: boolean;
  phone: number;
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
  loading: boolean;
  teacherStudentCount: any;
  subjectName: any;
  className: any;
  boardName: any;
  classId: any;
  boardId: any;
  subjectId: any;
  startDate: any;
  endDate: any;
  frequencySelected: any;
  frequency = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  startTime: any;
  endTime: any;
  teacherName: any;
  teacherPhoneNumber: any;
  teacherPassword: any;
  studentName: any;
  studentPhoneNumber: any;
  studentPassword: any;
  title: any;
  teacherId: any;
  dropdownSettingsClass = {
    singleSelection: false,
    idField: 'classId',
    textField: 'className',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true,

  };
  dropdownSettingsBoard = {
    singleSelection: false,
    idField: 'boardId',
    textField: 'boardName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
 
  dropdownSettingsSubject = {
    singleSelection: false,
    idField: 'subjectId',
    textField: 'subjectName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };

  isSubjectAddedVal: boolean = false;
  isClassAddedVal: boolean = false;
  isBoardAddedVal: boolean = false;
  courseToAdd: Course;
  classToAdd: any
  subjectToAdd: any
  boardToAdd: any
  classlist: any;
  boardlist: any;
  subjectlist: any;
  cbsStepComplete: boolean; 
  @Output() isTeacherAdded: any = new EventEmitter(); 
  @Output() teacherStudentCountEvent: any = new EventEmitter();
  @Output() isStudentAdded: any = new EventEmitter();
  @Output() isCourseAdded: any = new EventEmitter();
  @Input() teacherDetails: any;
  @Input() studentDetails: any;
  @Input() teacherDetailsForAccountPage: any;
  @Input() ccDetailsForAccountPage: any;
  @Input() studentDetailsForAccountPage: any;
  constructor(public comservice: CommonServiceService, public messagingService: MessagingService,
    private lpservice: LearningpodService, public course: Course) {


  }


  ngOnInit() {
    this.loading = true;
    this.comservice.showNavbar = true;
    this.cbsStepComplete =true;
  
   
   if(this.teacherDetails){
    this.teacherStepComplete = false;
   }
   else{
    this.teacherStepComplete = true;
   }
   if(this.studentDetails){
    this.studentStepComplete = false;
   }
   else{
    this.studentStepComplete = true;
   }
    this.getGlobalSubject();
    if (this.comservice.myUserRole === "teacher") {
      this.getTeacherDetails();
    } else if (this.comservice.myUserRole === "cc") {
      this.getCcDetails();
    } else if (this.comservice.myUserRole === "student") {
      this.getStudentDetails();
    }
    this.emailerr = false;
    this.editable = false;
    this.courseToAdd = new Course();
  }




  startTour() {
    introJs().setOption('showStepNumbers', false).start();
    localStorage.removeItem("firstLogin");
  }


  editInfo() {
    this.editable = true;
  }

  disableEditInfo() {
    this.editable = false;
  }

  removeDuplicates(array, key) {
    return array.reduce((accumulator, element) => {
      if (!accumulator.find(el => el[key] === element[key])) {
        accumulator.push(element);
      }
      return accumulator;
    }, []);
  }

  getTeacherDetails() {
    this.loading = false;
    this.selectedSubjects = [];
    this.selectedClass= [];
     this.selectedBoard=[];
    this.teacherInfo = this.teacherDetailsForAccountPage.teacherInfo;
    this.picturePath = this.teacherInfo.picturePath;
    this.teacherInfoToUpdate = this.teacherDetailsForAccountPage.teacherInfo;
    this.phone = this.teacherInfo.phoneNo;

    for (let i = 0; i < this.teacherDetailsForAccountPage.teacherClassInfo.length; i++) {
      this.selectedClass.push(this.teacherDetailsForAccountPage.teacherClassInfo[i].classInfo);
      this.selectedClassToUpdate.push(this.teacherDetailsForAccountPage.teacherClassInfo[i].classInfo);
    }
  
    this.selectedClass.sort((x, y) => {
      return this.comservice.compareNumberAndChar(x.className, y.className)
    })
   
    for (let i = 0; i < this.teacherDetailsForAccountPage.teacherSubjectInfo.length; i++) {
      this.selectedSubjects.push(this.teacherDetailsForAccountPage.teacherSubjectInfo[i].subjectInfo);
      this.selectedSubjectsToUpdate.push(this.teacherDetailsForAccountPage.teacherSubjectInfo[i].subjectInfo);
    }
  
    this.selectedSubjects.sort((x, y) => {
      return this.comservice.sortCharArray(x.subjectName, y.subjectName); 
  });
 
  for (let i = 0; i < this.teacherDetailsForAccountPage.teacherBoardInfo.length; i++) {
    this.selectedBoard.push(this.teacherDetailsForAccountPage.teacherBoardInfo[i].boardInfo);
    this.selectedBoardsToUpdate.push(this.teacherDetailsForAccountPage.teacherBoardInfo[i].boardInfo);
  }

  this.selectedBoard.sort((x, y) => {
    return this.comservice.sortCharArray(x.boardName, y.boardName); 
});
if(this.selectedSubjects[0] == undefined){
  this.selectedSubjects = [];
}
if(this.selectedClass[0] == undefined){
  this.selectedClass = [];
}
if(this.selectedBoard[0] == undefined){
  this.selectedBoard = [];
}
this.comservice.cbsObj = new CBS(this.selectedSubjects, this.selectedClass, this.selectedBoard);
    this.comservice.changeCcName(this.teacherDetailsForAccountPage.teacherCCInfo.ccName)
    this.comservice.ccDetails(this.teacherDetailsForAccountPage.teacherCCInfo.ccAddress1,
      this.teacherDetailsForAccountPage.teacherCCInfo.ccAddress2,
      this.teacherDetailsForAccountPage.teacherCCInfo.ccName,
      this.teacherDetailsForAccountPage.teacherCCInfo.ccPhoneNo,
      this.teacherDetailsForAccountPage.teacherCCInfo.ccId,
      this.teacherDetailsForAccountPage.teacherCCInfo.picturePath);


  }

  onItemSelect(item: any) {
  }
  onSelectAll(items: any) {
  }

  updateTeacherDetails() {
    this.loading = true;
    this.selectedSubjects = [];
    this.selectedClass= [];
     this.selectedBoard=[];
    let updatedSubjects = [];
    for (let i = 0; i < this.selectedSubjectsToUpdate.length; i++) {
      let sub = this.selectedSubjectsToUpdate[i].subjectId;
      updatedSubjects.push({ "subject": sub });
    }

    let updatedClasses = [];
    for (let i = 0; i < this.selectedClassToUpdate.length; i++) {
      let cls = this.selectedClassToUpdate[i].classId;
      updatedClasses.push({ "classID": cls });
    }
    let updatedBoard = [];
    for (let i = 0; i < this.selectedBoardsToUpdate.length; i++) {
      let brd = this.selectedBoardsToUpdate[i].boardId;
      updatedBoard.push({ "boardInformation": brd });
    }
    if (this.teacherInfoToUpdate.teacherBirthdate == undefined) {
      this.teacherInfoToUpdate.teacherBirthdate = "";
    }

    this.lpservice.updateTeacher(this.teacherInfoToUpdate, updatedClasses,
      updatedSubjects, updatedBoard).subscribe((data) => {
        this.loading = false;
        if (data.status == "success") {

          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: `${data.message}`,
            timer: 2000
          })
          // alert(data.message);
          this.editable = false;
          this.teacherInfo = this.teacherInfoToUpdate;
          this.selectedClass = this.selectedClassToUpdate;
          this.selectedClass.sort((x, y) => {
            return this.comservice.compareNumberAndChar(x.className, y.className)
          })
          this.selectedSubjects = this.selectedSubjectsToUpdate;
          this.selectedSubjects.sort((x, y) => {
            return this.comservice.sortCharArray(x.subjectName, y.subjectName); 
        });
        this.selectedBoard = this.selectedBoardsToUpdate;
        this.selectedBoard.sort((x, y) => {
          return this.comservice.sortCharArray(x.boardName, y.boardName); 
      });
      this.comservice.cbsObj = new CBS(this.selectedSubjects, this.selectedClass, this.selectedBoard);
          this.comservice.myUserName = this.teacherInfoToUpdate.teacherName;
        }
        else {
          this.loading = false;
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: `${data.message}`,
            timer: 2000
          })
          this.editable = false;
        }
      }, (err) => {
        this.loading = false;
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: `Failed to update: ${err}`,
          timer: 2000
        })
        this.editable = false;
      });
  }
  getCcDetails() {
    this.selectedSubjects = [];
    this.selectedClass= [];
     this.selectedBoard=[];
    this.loading = false;
    this.teacherStudentCount = this.ccDetailsForAccountPage;
    this.teacherStudentCountEvent.emit(this.teacherStudentCount);
    if(this.ccDetailsForAccountPage.studentCount == 0){
this.studentStepComplete = false
    }
    if(this.ccDetailsForAccountPage.teacherCount == 0){
      this.teacherStepComplete = false
          }
    this.ccInfo = this.ccDetailsForAccountPage.ccInfo;
    if (this.ccInfo.picturePath) {
      this.picturePath = this.ccInfo.picturePath;
    } else {
      this.picturePath = null;
    }
if(this.ccDetailsForAccountPage.ccSubjectInfo.length == 0){
  this.cbsStepComplete = false;
}
else{
  this.cbsStepComplete = true;
}
    this.phone = this.ccInfo.phoneNo;
    this.ccInfoToUpdate = this.ccDetailsForAccountPage.ccInfo;
    let ccSubjectInfo = this.ccDetailsForAccountPage.ccSubjectInfo;
   
    for (var i = 0; i < ccSubjectInfo.length; i++) {
      this.selectedSubjects.push(ccSubjectInfo[i].subjectInfo);
      this.selectedSubjectsToUpdate.push(ccSubjectInfo[i].subjectInfo);
    }
  
    this.selectedSubjects.sort((x, y) => {
      return this.comservice.sortCharArray(x.subjectName, y.subjectName); 
  });
    let ccClassInfo = this.ccDetailsForAccountPage.ccClassInfo;

    for (var i = 0; i < ccClassInfo.length; i++) {
      this.selectedClass.push(ccClassInfo[i].classInfo);
      this.selectedClassToUpdate.push(ccClassInfo[i].classInfo);
    }
  
    this.selectedClass.sort((x, y) => {
      return this.comservice.compareNumberAndChar(x.className, y.className)
    })

    let ccBoardInfo = this.ccDetailsForAccountPage.ccBoardInfo;
 
    for (var i = 0; i < ccBoardInfo.length; i++) {
      this.selectedBoard.push(ccBoardInfo[i].boardInfo);
      this.selectedBoardsToUpdate.push(ccBoardInfo[i].boardInfo);
    }
  
    this.selectedBoard.sort((x, y) => {
      return this.comservice.sortCharArray(x.boardName, y.boardName)
    })
    if(this.selectedSubjects[0] == undefined){
      this.selectedSubjects = [];
    }
    if(this.selectedClass[0] == undefined){
      this.selectedClass = [];
    }
    if(this.selectedBoard[0] == undefined){
      this.selectedBoard = [];
    }
    this.comservice.changeCcName(this.ccDetailsForAccountPage.ccInfo.ccName)
    this.comservice.ccDetails(this.ccDetailsForAccountPage.ccInfo.address1, this.ccDetailsForAccountPage.ccInfo.address2,
      this.ccDetailsForAccountPage.ccInfo.ccName, this.ccDetailsForAccountPage.ccInfo.phoneNo,
      this.ccDetailsForAccountPage.ccInfo.ccId, this.ccDetailsForAccountPage.ccInfo.picturePath);
      this.comservice.cbsObj = new CBS(this.selectedSubjects, this.selectedClass, this.selectedBoard);

   
  }

  updateCcDetails() {
    this.loading = true;
    let updatedSubjects = [];
    let updatedBoards=[];
    this.selectedSubjects = [];
    this.selectedClass= [];
     this.selectedBoard=[];
    for (var i = 0; i < this.selectedSubjectsToUpdate.length; i++) {
      let sub = this.selectedSubjectsToUpdate[i].subjectId;
      updatedSubjects.push({ "subject": sub });
    }

    let updatedClasses = [];
    for (var i = 0; i < this.selectedClassToUpdate.length; i++) {
      let cls = this.selectedClassToUpdate[i].classId;
      updatedClasses.push({ "classID": cls }); // to send selectedClasses in a format needed by java
    }
    for (var i = 0; i < this.selectedBoardsToUpdate.length; i++) {
      let brd = this.selectedBoardsToUpdate[i].boardId;
      updatedBoards.push({ "boardInformation": brd }); 
    }
    this.lpservice.updateCc(this.ccInfoToUpdate, updatedSubjects, updatedClasses,updatedBoards).subscribe((data) => {
      this.loading = false;
      if (data.status == "success") {

        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'success',
          title: 'Success!',
          text: `${data.message}`,
          timer: 2000
        })
        this.editable = false;
        this.ccInfo = this.ccInfoToUpdate;
        
        this.selectedClass = this.selectedClassToUpdate;
        this.selectedClass.sort((x, y) => {
          return this.comservice.compareNumberAndChar(x.className, y.className)
        })
        this.selectedSubjects = this.selectedSubjectsToUpdate;
        this.selectedSubjects.sort((x, y) => {
          return this.comservice.sortCharArray(x.subjectName, y.subjectName); 
      });
      if(this.selectedSubjects.length == 0){
        this.cbsStepComplete = false;
      }
      else{
        this.cbsStepComplete = true;
      }
      this.selectedBoard = this.selectedBoardsToUpdate;
      this.selectedBoard.sort((x, y) => {
        return this.comservice.sortCharArray(x.boardName, y.boardName); 
    });
      this.comservice.cbsObj = new CBS(this.selectedSubjects, this.selectedClass, this.selectedBoard);

        this.comservice.myUserName = this.ccInfoToUpdate.ccName;
      }
      else {
        this.loading = false;
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: `${data.message}`,
          timer: 2000
        })
      }
    }, (err) => {
      this.loading = false;
      Swal.fire({
        backdrop: false,
        heightAuto: false,
        type: 'error',
        title: 'Error',
        text: `${err}`,
        timer: 2000
      })
      this.editable = true;
    });
  }

  getStudentDetails() {
    this.selectedSubjects = [];
    this.selectedClass= [];
     this.selectedBoard=[];
    this.loading = false;
    this.studentInfo = this.studentDetailsForAccountPage.studentInfo;
    this.picturePath = this.studentInfo.picturePath;
    this.phone = this.studentInfo.studentContactInfo.phoneNo;
    this.studentInfoToUpdate = this.studentDetailsForAccountPage.studentInfo;
 
      this.selectedClass.push(this.studentDetailsForAccountPage.studentInfo.classInfo);
      this.selectedClassToUpdate.push(this.studentDetailsForAccountPage.studentInfo.classInfo);
    
 
      this.selectedBoard.push(this.studentDetailsForAccountPage.studentInfo.boardInfo);
      this.selectedBoardsToUpdate.push(this.studentDetailsForAccountPage.studentInfo.boardInfo);
    
   
   
    this.comservice.changeCcName(this.studentDetailsForAccountPage.studentInfo.ccInformation.ccName)
    this.comservice.ccDetails(this.studentDetailsForAccountPage.studentInfo.ccInformation.address1,
      this.studentDetailsForAccountPage.studentInfo.ccInformation.address2,
      this.studentDetailsForAccountPage.studentInfo.ccInformation.ccName,
      this.studentDetailsForAccountPage.studentInfo.ccInformation.phoneNo,
      this.studentDetailsForAccountPage.studentInfo.ccInformation.ccId,
      this.studentDetailsForAccountPage.studentInfo.ccInformation.picturePath);
      if(this.selectedSubjects[0] == undefined){
        this.selectedSubjects = [];
      }
      if(this.selectedClass[0] == undefined){
        this.selectedClass = [];
      }
      if(this.selectedBoard[0] == undefined){
        this.selectedBoard = [];
      }
     this.comservice.cbsObj = new CBS(this.selectedSubjects, this.selectedClass, this.selectedBoard);

  }

  updateStudentDetails() {
    this.loading = true;
    this.selectedSubjects = [];
    this.selectedClass= [];
     this.selectedBoard=[];
    let newClassId = null;
    if (this.selectedClassToUpdate != null && this.selectedClassToUpdate.length > 0) {
      newClassId = this.selectedClassToUpdate[0].classId;
    }
    this.lpservice.updateStudent(this.studentInfoToUpdate, newClassId).subscribe((data) => {
      this.loading = false;
      if (data.status == "success") {
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'success',
          title: 'Success!',
          text: `${data.message}`,
          timer: 2000
        })
        this.editable = false;
        this.studentInfo = this.studentInfoToUpdate;
        this.selectedClass = this.selectedClassToUpdate;
        this.comservice.myUserName = this.studentInfoToUpdate.studentName;
      }
      else {
        this.loading = false;
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: `${data.message}`,
          timer: 2000
        })
      }
    }, (err) => {
      this.loading = false;
      Swal.fire({
        backdrop: false,
        heightAuto: false,
        type: 'error',
        title: 'Error!',
        text: "Failed to update",
        timer: 2000
      })
      this.editable = false;
    });
  }
  selectedfile = null;
  uploadPhoto(event) {
    this.loading = true;
    this.selectedfile = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(this.selectedfile);
    reader.onload = (event) => {
    }
    if (this.selectedfile.type == "image/jpeg" || this.selectedfile.type == "image/png") {
      const newfile: File = this.selectedfile;
      this.lpservice.uploadImage(newfile).subscribe((res) => {
        this.loading = false;
        if (res.status = "success") {
          this.showDelete = true;
          this.picturePath = res.fileUrl;
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: `${res.message}`,
            timer: 2000
          })
          this.comservice.userProfilePic(res.fileUrl);

        }
        else {
          this.loading = false;
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: `${res.message}`,
            timer: 2000
          })
        }
      },
        (err) => {
          this.loading = false;
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: `${err}`,
            timer: 2000
          })
        }
      );
    }
    else {
      this.loading = false;
      Swal.fire({
        backdrop: false,
        heightAuto: false,
        type: 'error',
        title: 'Error!',
        text: "Invalid format! Please Select JPEG or PNG format",
        timer: 2000
      })
    }
  }
  showDelete: any;

  deleteImage(userId, picturePath) {
    this.loading = true;
    this.lpservice.deleteImage(userId, picturePath).subscribe((res) => {
      this.loading = false;

      if (res.status == "success") {
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'success',
          title: 'Success!',
          text: `${res.message}`,
          timer: 2000
        })
        this.showDelete = false;
      }
      else {
        this.loading = false;
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: `${res.message}`,
          timer: 2000
        })
      }

      if (this.comservice.myUserRole === "teacher") {
        this.picturePath = "https://res.cloudinary.com/coffeebeans-technologies/image/upload/v1587967444/LearningPod/myAccounts/ccDefault_jwgxz8.jpg";
        this.comservice.userProfilePic("https://res.cloudinary.com/coffeebeans-technologies/image/upload/v1587967445/LearningPod/myAccounts/studentDefault_ovz6b6.jpg");
      } else if (this.comservice.myUserRole === "cc") {
        this.picturePath = "https://res.cloudinary.com/coffeebeans-technologies/image/upload/v1587967444/LearningPod/myAccounts/ccDefault_jwgxz8.jpg";
        this.comservice.userProfilePic("https://res.cloudinary.com/coffeebeans-technologies/image/upload/v1587967444/LearningPod/myAccounts/ccDefault_jwgxz8.jpg");
      } else if (this.comservice.myUserRole === "student") {
        this.picturePath = "https://res.cloudinary.com/coffeebeans-technologies/image/upload/v1587967445/LearningPod/myAccounts/studentDefault_ovz6b6.jpg";
        this.comservice.userProfilePic("https://res.cloudinary.com/coffeebeans-technologies/image/upload/v1587967445/LearningPod/myAccounts/studentDefault_ovz6b6.jpg");
      }

    })
  }

  changepassword(phone, old, pass, confpass) {
    this.loading = true;
    if (phone != this.phone) {
      Swal.fire({
        backdrop: false,
        heightAuto: false,
        type: 'error',
        title: 'Error!',
        text: "Enter your own phone number",
        timer: 2000
      })
      //alert("Enter your own phone number");
    }
    else if (pass != confpass) {
      Swal.fire({
        backdrop: false,
        heightAuto: false,
        type: 'error',
        title: 'Error!',
        text: "Password Mismatch",
        timer: 2000
      })
      // alert("Password Mismatch");
    }
    else {
      this.lpservice.changePassword(phone, old, confpass, this.comservice.myUserRole).subscribe(res => {
        this.loading = false;
        if (res.status == "success") {
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: `${res.message}`,
            timer: 2000
          })
          // alert(res.status);
          // jQuery('#changepassword').modal('hide');
          var modals = document.getElementById('changepassword');
          modals.setAttribute('data-dismiss', 'modal');
          this.phone = null;
          this.oldPassword = "";
          this.newPassword = "";
          this.confirmPassword = "";
        }
        else {
          this.loading = false;
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: `${res.message}`,
            timer: 2000
          })
        }
      })
    };
  }
  ValidateEmail(email) {
    if (email != "") {
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.teacherInfoToUpdate.email)) {
        this.emailerr = false;
      }
      else {
        this.emailerr = true;
      }
    }
  }

  closeChangePassword() {
    this.phone = null;
    this.oldPassword = "";
    this.newPassword = "";
    this.confirmPassword = "";
  }

  removeValidation() {
    document.getElementById("addSubjectBox").style.borderColor = "lightgrey";
    document.getElementById("addClassBox").style.borderColor = "lightgrey";
    document.getElementById("addBoardBox").style.borderColor = "lightgrey";

    this.subjectName = null;
    this.className = null;
    this.boardName = null;

  }
  focusOnFirstInput() {
    $(".modal").on('shown.bs.modal', function () {
      $(this).find("input:visible:first").focus();
    });
  }


  subjectNameToAdd: string
  classNameToAdd: string
  boardNameToAdd: string

  isClassExists(className, arr) {

    return arr.some(function (el) {
      return el.className.toLowerCase() === className.toLowerCase();
    });
  }

  isBoardExists(boardName, arr) {
    return arr.some(function (el) {
      return el.boardName.toLowerCase() === boardName.toLowerCase();
    });
  }

  isSubjectExists(subjectName, arr) {
    return arr.some(function (el) {
      return el.subjectName.toLowerCase() === subjectName.toLowerCase();
    });
  }

   


  saveClass() {

    if (this.classNameToAdd == undefined || this.classNameToAdd.length == 0) {

      Swal.fire({
        backdrop: false,
        heightAuto: false,
        type: 'error',
        title: 'Error!',
        text: 'Enter class name to add!',
        timer: 3000
      })
    } else {
      if (this.isClassExists(this.classNameToAdd, this.classlist)) {
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'warning',
          title: 'Warning!',
          text: "Class alerady exist!",
          timer: 2000
        })
        // alert("class already exist")
      } else {

        this.lpservice.addClass(this.classNameToAdd).subscribe(res => {
          if (res.status == "success") {
          document.getElementById("closeCBSmodal").click();
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: 'Class Information added successfully!',
              timer: 3000
            })
          
             this.classlist.push({classId: res.classId, className: this.classNameToAdd});
          }

        })

      }

    }


  }
  saveBoard() {
    if (this.boardNameToAdd == undefined || this.boardNameToAdd.length == 0) {

      Swal.fire({
        backdrop: false,
        heightAuto: false,
        type: 'error',
        title: 'Error!',
        text: 'Enter board name to add!',
        timer: 3000
      })
    } else {

      if (this.isBoardExists(this.boardNameToAdd, this.boardlist)) {
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'warning',
          title: 'Warning!',
          text: "Board alerady exist!",
          timer: 2000
        })
      } else {
        this.lpservice.addBoard(this.boardNameToAdd).subscribe(res => {
          if (res.status == "success") {
            document.getElementById("closeCBSmodal").click();
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: 'Board Information added successfully!',
              timer: 3000
            })
           
            this.boardlist.push({ boardId: res.boardId, boardName: this.boardNameToAdd });
            //this.getBoards();
          }
        })
      }
    }
  }

  saveSubject() {
    if (this.subjectNameToAdd == undefined || this.subjectNameToAdd.length == 0) {
      Swal.fire({
        backdrop: false,
        heightAuto: false,
        type: 'error',
        title: 'Error!',
        text: 'Enter class subject to add!',
        timer: 3000
      })
    } else {

      if (this.isSubjectExists(this.subjectNameToAdd, this.subjectlist)) {
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'warning',
          title: 'Warning!',
          text: "Subject alerady exist!",
          timer: 2000
        })
      } else {
        this.lpservice.addSubject(this.subjectNameToAdd).subscribe(res => {
          if (res.status == "success") {
            document.getElementById("closeCBSmodal").click();
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: 'Subject Information added successfully!',
              timer: 3000
            })
            this.subjectlist.push({ subjectId: res.subjectId, subjectName: this.subjectNameToAdd });
            }
        })


      }
    }
  }

  // validateCBS() {
  //   if (this.classToAdd1 == undefined || this.classToAdd1.length == 0) {
  //     Swal.fire({
  //       backdrop: false,
  //       heightAuto: false,
  //       type: 'error',
  //       title: 'Error!',
  //       text: 'Please select class!',
  //       timer: 3000
  //     })
  //     return false
  //   }

  //   else if (this.boardToAdd1 == undefined || this.boardToAdd1.length == 0) {
  //     Swal.fire({
  //       backdrop: false,
  //       heightAuto: false,
  //       type: 'error',
  //       title: 'Error!',
  //       text: 'Please select board!',
  //       timer: 3000
  //     })
  //     return false
  //   }
  //   else if (this.subjectToAdd1 == undefined || this.subjectToAdd1.length == 0) {
  //     Swal.fire({
  //       backdrop: false,
  //       heightAuto: false,
  //       type: 'error',
  //       title: 'Error!',
  //       text: 'Please select subject!',
  //       timer: 3000
  //     })
  //     return false
  //   }
  //   return true
  // }

  // ccId: any

  // addClassGroup() {

  //   this.validateCBS();

  //   // for (let i = 0; i < this.teacherDetailsForAccountPage.teacherClassInfo.length; i++) {
  //   //   this.selectedClass.push(this.teacherDetailsForAccountPage.teacherClassInfo[i].classInfo);
  //   //   this.selectedClassToUpdate.push(this.teacherDetailsForAccountPage.teacherClassInfo[i].classInfo);
  //   // }
  //   // "subjectIds":[ {"subjectId" : 6}, {"subjectId" : 8}],
  //   let updatedSubjectsArray = [];
  //   for (let i = 0; i < this.subjectToAdd1.length; i++) {
  //     updatedSubjectsArray.push({ "subjectId": this.subjectToAdd1[i].subjectId });
  //   }
  //   //       if(this.comservice.myUserRole == "cc"){
  //   //  this.ccId = this.comservice.myUserId
  //   //       }

  //   this.lpservice.addCBS(this.classToAdd1[0].classId, this.boardToAdd1[0].boardId, updatedSubjectsArray, this.comservice.myUserId).subscribe(x => {

  //     if (x.status == "success") {
  //       this.classToAdd = undefined
  //       this.subjectToAdd = undefined
  //       this.boardToAdd = undefined
  //       Swal.fire({
  //         backdrop: false,
  //         heightAuto: false,
  //         type: 'success',
  //         title: 'Success!',
  //         text: 'Class, Board and Subject Information added successfully!',
  //         timer: 3000
  //       })

  //       this.classToAdd1 = null;
  //       this.boardToAdd1 = null;
  //       this.subjectToAdd1 = null;
  //       this.subjectNameToAdd = null;
  //       this.classNameToAdd = null;
  //       this.boardNameToAdd = null;

  //     }

  //     if (x.status == "error") {
  //       Swal.fire({
  //         backdrop: false,
  //         heightAuto: false,
  //         type: 'error',
  //         title: 'Error!',
  //         text: `${x.message}`,
  //         timer: 3000
  //       })
  //     }

  //     if (x.status == "warning") {
  //       Swal.fire({
  //         backdrop: false,
  //         heightAuto: false,
  //         type: 'warning',
  //         title: 'Warning!',
  //         text: `${x.message}`,
  //         timer: 3000
  //       })
  //     }

  //   })
  // }
  loadingReport: boolean;
  showTable: boolean;
  scheduleClassInfo: any;
  showPrint: boolean;
  totalCounts: any;

  getTeacherReport(startDate, endDate) {
    this.loadingReport = true;
    this.showTable = false;
    if (startDate != undefined && endDate != undefined) {
      var start = new Date(startDate).toISOString().slice(0, 10);
      var end = new Date(endDate).toISOString().slice(0, 10);
      this.lpservice.getTeacherReport(this.comservice.myUserId, start, end).subscribe((data) => {
        if (data.status == "success") {
          this.showTable = true;
          this.scheduleClassInfo = data.scheduleClassInfo;
          this.showPrint = true;
          this.totalCounts = data;
          this.loadingReport = false;
        }
        else {
          this.showTable = false;
          this.scheduleClassInfo = data.message;
          this.loadingReport = false;
        }
      })
    }
    else {
      this.loading = false;
      this.showTable = false;
      this.scheduleClassInfo = "Please select a valid start date and end date to get report of teacher";
      this.loadingReport = false;
    }
  }
 // updatedClassList = this.classlist;


  printReport() {
    window.print();
  }

  getUserPackage() {
    this.loading = true;
    this.lpservice.getUserPackage().subscribe((data) => {
      this.loading = false;
      if (data.status = "success") {
        this.userUsage = data.userUsage;
        this.userPackageInfo = data.userPackageInfo;
      }

    })
  }
  userUsage: any;
  userPackageInfo: any;
  showPassword() {
    var x = document.getElementById("oldPassword")as HTMLInputElement;
    var y = document.getElementById("newPassword")as HTMLInputElement;
    var z = document.getElementById("confirmPassword")as HTMLInputElement;
    if (x.type === "password") {
      x.type = "text";
      y.type = "text"; 
      z.type = "text";
    } else {
      z.type = "password";
      y.type = "password";
      x.type = "password";
    }
  }

  getGlobalSubject(){
    this.lpservice.getallSubjects().subscribe((subjectData: any) => {
      subjectData.sort((x, y) => {
        return this.comservice.sortCharArray(x.subjectName, y.subjectName);
      });
      this.subjectlist = subjectData; 
      this.getClasses();
      
    });
   }
  
   getClasses() {
    this.lpservice.getallClasses().subscribe((classData: any) => {
      classData.sort((x, y) => {
        return this.comservice.compareNumberAndChar(x.className, y.className)
      })
      this.classlist = classData;
      this.getBoards();
    })
  }
  
  getBoards() {
    this.lpservice.getBoards().subscribe((boardData: any) => {
      boardData.sort((x, y) => {
        return this.comservice.sortCharArray(x.boardName, y.boardName);
      });
      this.boardlist = boardData;  
    })
  }
  teacherStepComplete: boolean;
  studentStepComplete: boolean;
    registerBasicTeacher(teacherName, teacherPhoneNumber, teacherPassword) {
      this.loading = true;
      this.diabledButton = true;
      if (teacherName == undefined || teacherName == "") {
        (document.getElementById('basicTeacherName') as HTMLInputElement).style.borderColor = "red";
        this.diabledButton = false;
        this.loading = false;
      }
      if (teacherPhoneNumber == undefined || teacherPhoneNumber == "") {
        (document.getElementById('basicTeacherPhoneNumber') as HTMLInputElement).style.borderColor = "red";
        this.diabledButton = false;
        this.loading = false;
      }
      if (teacherPassword == undefined || teacherPassword == "") {
        (document.getElementById('basicTeacherPassword') as HTMLInputElement).style.borderColor = "red";
        this.diabledButton = false;
        this.loading = false;
      }
  
      else {
        this.lpservice.registerRestrictedTeacher(this.comservice.myUserId, teacherName, null,
          null, teacherPhoneNumber, null, [], [], teacherPassword,[]).subscribe(res => {
            this.loading = false;
            this.diabledButton = false;
            if (res.status == "success") {
              this.teacherStepComplete = true;
              this.addTeacherValidation();
              this.isTeacherAdded.emit(true);
              Swal.fire({
                backdrop: false,
                heightAuto: false,
                type: 'success',
                title: 'Success!',
                text: `${res.message}`,
                timer: 2000
  
              })
            }
            else {
              Swal.fire({
                backdrop: false,
                heightAuto: false,
                type: 'error',
                title: 'Error!',
                text: `${res.message}`,
                timer: 2000
              })
            }
          })
      }
    }
  
  
    registerBasicStudent(studentName, studentPhoneNumber, studentPassword) {
      this.loading = true;
      this.diabledButton = true;
      if (studentName == undefined || studentName == "") {
        (document.getElementById('basicStudentName') as HTMLInputElement).style.borderColor = "red";
        this.diabledButton = false;
        this.loading = false;
      }
      if (studentPhoneNumber == undefined || studentPhoneNumber == "") {
        (document.getElementById('basicStudentPhoneNumber') as HTMLInputElement).style.borderColor = "red";
        this.diabledButton = false;
        this.loading = false;
      }
      if (studentPassword == undefined || studentPassword == "") {
        (document.getElementById('basicStudentPassword') as HTMLInputElement).style.borderColor = "red";
        this.diabledButton = false;
        this.loading = false;
      }
  
      else {
  
        this.lpservice.registerRestrictedStudent(this.comservice.myUserId, studentName, studentPhoneNumber, undefined, undefined, studentPassword).subscribe(res => {
          this.loading = false;
          this.diabledButton = false;
          if (res.status == "success") {
            this.studentStepComplete = true;
            this.addStudentValidation();
            this.isStudentAdded.emit(true);
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: `${res.message}`,
              timer: 2000
            })
            //this.clearModal()
          }
          else {
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'error',
              title: 'Error!',
              text: `${res.message}`,
              timer: 2000
            })
          } this.loading = false;
        })
  
      }
      // this.loading = false;
    }
    addBasicCourse(formdata, title, classId, boardId, subjectId, startDate, endDate, frequencySelected, startTime, endTime, teacherId, scheduleLaterFlag) {
      this.loading = true;
      if (!formdata) {
        this.loading = false;
        return false;
      }
  
      if (startDate == null || startDate == undefined || endDate == null || endDate == undefined || startTime == null ||
        startTime == undefined || endTime == null || endTime == undefined) {
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: "Please enter a valid date and time",
          timer: 2000
        })
        this.loading = false;
        return false;
      }
      this.courseToAdd.startDate = new Date(startDate).toLocaleDateString("sv-SE"); // Date to yyyy/mm/dd format
      this.courseToAdd.endDate = new Date(endDate).toLocaleDateString("sv-SE");
      if (this.courseToAdd.endDate < this.courseToAdd.startDate) {
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: "End date should be greater then start date of a course",
          timer: 2000
        })
        this.loading = false;
        return false;
      }
      // this.loading = true;
      this.diabledButton = true;
  
  
      var options = { hour: 'numeric', minute: '2-digit' };
      if (startTime instanceof Date) {
        this.courseToAdd.startTime = new Date(startTime).toLocaleTimeString('en-US', options);
      }
      if (endTime instanceof Date) {
        this.courseToAdd.endTime = new Date(endTime).toLocaleTimeString('en-US', options);
      }
      const sorter = {
        "monday": 1,
        "tuesday": 2,
        "wednesday": 3,
        "thursday": 4,
        "friday": 5,
        "saturday": 6,
        "sunday": 7
      }
      if (frequencySelected) {
        frequencySelected.sort(function sortByDay(a, b) {
          let day1 = a.toLowerCase();
          let day2 = b.toLowerCase();
          return sorter[day1] - sorter[day2];
        });
      }
  
      this.courseToAdd.frequency = frequencySelected.join(",");
      this.courseToAdd.title = title;
      this.courseToAdd.classId = + classId;
      this.courseToAdd.boardId = + boardId;
      this.courseToAdd.subjectId = + subjectId;
      this.courseToAdd.teacherId = + teacherId;
      this.courseToAdd.cost = 0;
      this.courseToAdd.doubtClearance = false;
      this.courseToAdd.onlineForum = false;
      this.courseToAdd.studyMaterial = false;
      this.courseToAdd.periodicTests = false;
      this.courseToAdd.availableOnPhone = false;
      this.courseToAdd.homework = false;
      this.courseToAdd.ccId = this.comservice.myUserId;
  
      if (scheduleLaterFlag == false || scheduleLaterFlag == undefined) {
        this.lpservice.addCourseNew(this.courseToAdd).subscribe(res => {
          this.diabledButton = false;
          this.loading = false;
    
          if (res.status == "success") {
            this.isCourseAdded.emit(true);
            this.addCourseValidation();
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: `${res.message}`,
              timer: 2000
            })  
          }
        })
      }
      else {
        this.lpservice.scheduleLaterCourse(this.courseToAdd).subscribe(res => {
          this.diabledButton = false;
          this.loading = false;
          if (res.status == "success") {
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: `${res.message}`,
              timer: 2000
            })  
          }
        })
      }
    }
 
    addTeacherValidation() {
      (document.getElementById('basicTeacherName') as HTMLInputElement).style.borderColor = "lightgrey";
      (document.getElementById('basicTeacherPhoneNumber') as HTMLInputElement).style.borderColor = "lightgrey";
      (document.getElementById('basicTeacherPassword') as HTMLInputElement).style.borderColor = "lightgrey";
      this.teacherName = "";
      this.teacherPhoneNumber = "";
      this.teacherPassword = "";
    }
    addStudentValidation() {
      (document.getElementById('basicStudentName') as HTMLInputElement).style.borderColor = "lightgrey";
      (document.getElementById('basicStudentPhoneNumber') as HTMLInputElement).style.borderColor = "lightgrey";
      (document.getElementById('basicStudentPassword') as HTMLInputElement).style.borderColor = "lightgrey";
      this.studentName = "";
      this.studentPassword = "";
      this.studentPhoneNumber = "";
    }
    addCourseValidation() {
      (document.getElementById("createCourseFormForSetup") as HTMLFormElement).reset();
      this.title = "";
      this.classId = undefined;
      this.subjectId = undefined;
      this.boardId = undefined;
      this.startDate = "";
      this.endDate = "";
      this.startTime = "";
      this.endTime = "";
      this.frequencySelected = [];
      this.teacherId = undefined;
  
    }
  
    disabledDates = (date: Date): boolean => {
      let todayDate = new Date();
      todayDate.setDate(todayDate.getDate() - 1)
      return date <= todayDate;
    }

  
    buyPackage() {
      this.loading = true;
      this.diabledButton = true;
      this.lpservice.buyPackage().subscribe(res => {
        this.loading = false;
        this.diabledButton = false;
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'success',
          title: 'Success!',
          text: `${res.message}`,
          timer: 2000
        })
      })
    }






  
}

