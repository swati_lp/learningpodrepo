import { Component, OnInit } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import { trigger, transition, animate, style } from '@angular/animations'
import { LearningpodService } from '../../learningpod.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
//import undefined = require('firebase/empty-import');

@Component({
  selector: 'app-explore-course',
  templateUrl: './explore-course.component.html',
  styleUrls: ['./explore-course.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translateY(-100%)' }),
        animate('200ms ease-in', style({ transform: 'translateY(0%)' }))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({ transform: 'translateY(-100%)' }))
      ])
    ])
  ]
})
export class ExploreCourseComponent implements OnInit {
  subject = [];
  toggle: any;
  courseList = [];
  filter: any;
  loading: boolean;
  emptydatalist: boolean;
  showExploreDetails: boolean;
  courseDetailsId: any;
  p: any;
  showEnquireEnroll: any;

  constructor(
    public comservice: CommonServiceService,
    public lpservice: LearningpodService, private router: Router) { }
  ngOnInit() {
    //$('body').removeClass('stop-scrolling')
    this.comservice.showNavbar = true;
    this.comservice.exploreHistory = 'courses'
    this.showExploreDetails = false;
    this.showEnquireEnroll = false;
    this.loading = true;
    this.emptydatalist = false;
    this.getCoursesforExplore();
    this.toggle = false;

  }

  getCourseDetailUrl(courseId) {
    return "https://learningpod-test.in/#/course" + courseId;
  }

  showEnquireAndEnrollButton() {
    if (this.comservice.myUserRole == null || this.comservice.myUserRole == "") {
      this.showEnquireEnroll = true;
    }
    else if (this.comservice.myUserRole == 'student') {
      this.showEnquireEnroll = true;
    }
    else {
      this.showEnquireEnroll = false;
    }
  }
  getEnquireValue(isEnquired: string): string {
    if (isEnquired == "true") {
      return "Enquired";
    }
    else {
      return "Enquire";
    }
  }

  getEnrollValue(isEnrolled: string): string {
    if (isEnrolled == "true") {
      return "Enrolled";
    }
    else {
      return "Request Enrollment";
    }
  }

  isEnquireDisabled(isEnquired: string): boolean {
    if (isEnquired == "true") {
      return true;
    }
    else {
      return false;
    }
  }
  isEnrollDisabled(isEnrolled: string): boolean {
    if (isEnrolled == "true") {
      return true;
    }
    else {
      return false;
    }
  }

  payInCashCc(courseId, ccId, teacherId) {
    this.lpservice.payInCashCc(courseId, ccId, this.comservice.myUserId, teacherId).subscribe(res => {
      Swal.fire({
        backdrop:false,
        heightAuto: false,
        text: `${res.message}`,
        timer: 2000
      });
      //alert(res.message);
      (document.getElementById("payOnline" + courseId) as HTMLInputElement).hidden = true;
      (document.getElementById("requestPaidInCash" + courseId) as HTMLInputElement).hidden = true;
      (document.getElementById("enroll" + courseId) as HTMLInputElement).style.backgroundColor = "orange";
      (document.getElementById("enroll" + courseId) as HTMLInputElement).value = "Processing";
    });
  }

  payOnlineAlert() {
    Swal.fire({
      backdrop:false,
      heightAuto: false,
      type: 'info',
      text: 'Comming soon !'
    })
  }


  enquireCcTeacher(courseId, ccId, teacherId) {
    this.loading = true;
 if(teacherId == undefined){
  this.lpservice.enquireRequestTeacherCc(courseId, this.comservice.myUserId, ccId, "").subscribe(res => {
    this.loading = false;
    if (res.status == "success") {
      //  (document.getElementById("enquire" + courseId) as HTMLInputElement).disabled = true;
      Swal.fire({
        backdrop:false,
        heightAuto: false,
        type: 'success',
        title: 'Success!',
        text: `${res.message}`,
        timer: 2000
      })
    }
  })
 }
 else{
    this.lpservice.enquireRequestTeacherCc(courseId, this.comservice.myUserId, ccId, teacherId).subscribe(res => {
      this.loading = false;
      if (res.status == "success") {
        //  (document.getElementById("enquire" + courseId) as HTMLInputElement).disabled = true;
        Swal.fire({
          backdrop:false,
          heightAuto: false,
          type: 'success',
          title: 'Success!',
          text: `${res.message}`,
          timer: 2000
        })
      }
    })
  }
  }

  getCoursesforExplore() {
    this.showEnquireAndEnrollButton();
    if (this.comservice.myUserRole == "cc") {
      this.lpservice.getMyCourses(this.comservice.myUserRole, this.comservice.myUserId).subscribe(res => {
        this.courseList = res;
        if (res.length == 0) {
          this.emptydatalist = true;
        }
        else {
          this.emptydatalist = false;
        }
        this.loading = false;
      });
    }
    else if (this.comservice.myUserRole == "student") {
      this.lpservice.getCoursesForExploreForStudent(this.comservice.myUserId, this.comservice.ccId).subscribe(res => {
        this.courseList = res;
        if (res.length == 0) {
          this.emptydatalist = true;
        }
        else {
          this.emptydatalist = false;
        }
        this.loading = false;
      });
    }
    else if (this.comservice.myUserRole == "teacher") {
      this.lpservice.getMyCourses("cc", this.comservice.ccId).subscribe(res => {
        this.courseList = res;
        if (res.length == 0) {
          this.emptydatalist = true;
        }
        else {
          this.emptydatalist = false;
        }
        this.loading = false;
      });
    }
  }


  getTimeDifference(st, et) {
    if (st != null && et != null) {
      let st_split = st.split(":");
      let et_split = et.split(":");
      let min = 0;
      min += 60 - parseInt(st_split[1]);
      st_split[0]++;
      var hhm = (parseInt(et_split[0]) - parseInt(st_split[0])) * 60;
      min += hhm;
      min += parseInt(et_split[1]);
      return min;
    }
  }



  navigateback() {
    this.showExploreDetails = false;
  }
  courseClicked(courseId) {
    this.router.navigateByUrl('home/course');
    localStorage.setItem("courseId", courseId);
  }

  rolechanged() {
  }


  navigateuser(role) {
  }
  teacherClicked(teacherId) {
    localStorage.setItem("teacherId", teacherId);
  }
}










