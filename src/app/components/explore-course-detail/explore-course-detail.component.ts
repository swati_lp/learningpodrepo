import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LearningpodService } from '../../learningpod.service';
import { CommonServiceService } from '../../common-service.service';
import { ActivatedRoute, Router } from '@angular/router';
//import Swal from 'sweetalert2';
//import { routerNgProbeToken } from '@angular/router/src/router_module';

@Component({
  selector: 'app-course-profile',
  templateUrl: './explore-course-detail.component.html',
  styleUrls: ['./explore-course-detail.component.css']
})
export class ExploreCourseDetailComponent implements OnInit {

  courseId: any;
  loading: boolean;
  courseDetails: any;
  courseReviews: any;
  courseDetailsEnrollValue: boolean;
  teacherDetailsId: any;
  showExploreTeacherDetails: boolean;
  @Input() course: any;
  // @Input() Explore: any;

  constructor(private router: Router, private route: ActivatedRoute, public lpservice: LearningpodService, public comservice: CommonServiceService) { }

  ngOnInit() {
    this.comservice.showNavbar = true;
    this.courseId = localStorage.getItem("courseId");
    this.comservice.exploreHistory = this.comservice.exploreHistory;
    //  this.courseId = this.route.snapshot.params["id"];
    this.getExploreCourseDetails();
    this.showExploreTeacherDetails = false;
  }

  back() {
    this.router.navigateByUrl(`home/${this.comservice.exploreHistory}`)
  }

  getExploreCourseDetails() {
    if (this.comservice.myUserRole == "student") {
      this.lpservice.exploreCourseDetailsForStudents(this.courseId).subscribe(res => {
        this.courseDetailsEnrollValue = res.courseDetails.isEnrolled;
        this.courseDetails = res.courseDetails.details;
        this.courseReviews = res.courseDetails.reviews;
        this.loading = false;
      })
    }
    else {
      this.lpservice.exploreCourseDetails(this.courseId).subscribe(res => {
        this.courseDetails = res.courseDetails.details;
        this.courseReviews = res.courseDetails.reviews;
        this.loading = false;
      })
    }
  }

  getTimeDifference(st, et) {
    if (st != null && et != null) {
      let st_split = st.split(":");
      let et_split = et.split(":");
      let min = 0;
      min += 60 - parseInt(st_split[1]);
      st_split[0]++;
      var hhm = (parseInt(et_split[0]) - parseInt(st_split[0])) * 60;
      min += hhm;
      min += parseInt(et_split[1]);
      return min;
    }
  }

  getNumOfDaysFromFrequency(frequency) {
    this.courseDetails.frequency = this.courseDetails.frequency;
    if (frequency == null || frequency == "")
      return 0;
    return frequency.split(",").length;
  }

  isClassScheduledOnDay(dayOfWeek: string): boolean {
    if (this.courseDetails.frequency != null) {
      this.courseDetails.frequency = this.courseDetails.frequency;
      return this.courseDetails.frequency.includes(dayOfWeek);
    }
  }

  // teacherDetails(teacherId) {
  //   this.teacherDetailsId = teacherId;
  //   this.showExploreTeacherDetails = true;
  // }

  teacherClicked(teacherId) {
    localStorage.setItem("teacherId", teacherId);
  }
}
