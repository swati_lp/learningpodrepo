

import { Component, Input, ChangeDetectionStrategy, ViewChild, TemplateRef, OnInit, ChangeDetectorRef } from '@angular/core';
import { isSameDay, isSameMonth } from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView } from 'angular-calendar';
import { CommonServiceService } from '../../common-service.service';
import { LearningpodService } from '../../learningpod.service';

import Swal from 'sweetalert2';
import * as $ from 'jquery'


const colors: any = {
  red: {
    primary: '#ff0000',
  },
  orange: {
    primary: '#ff5100',
  },
  orangeYellow: {
    primary: '#ffa200',
  },
  yellow: {
    primary: '#ffee00',
  },
  yellowGreen: {
    primary: '#b7ff00',
  },
  neonGreen: {
    primary: '#00ff73',
  },
  neonBlue: {
    primary: '#00ffc3',
  },
  blue: {
    primary: '#e60099',
  },
  brightBlue: {
    primary: '#0022ff',
  },
  lightPurple: {
    primary: '#8000ff',
  },
  purple: {
    primary: '#c800ff',
  },
  black: {
    primary: '#000000',
  },
  brightPink: {
    primary: '#ff00f2',
  },
  pink: {
    primary: '#ff006a',
  },
  maroon: {
    primary: '#730101',
  },
  blackGreen: {
    primary: '#647301',
  },
  teal: {
    primary: '#01734d',
  },
  violet: {
    primary: '#73013e',
  },
  lightGrey: {
    primary: '#a38b8b',
  },
  grey: {
    primary: '#879191',
  },
  lightPink: {
    primary: '#f09cc6',
  },
  darkGrey: {
    primary: '#595055',
  },
  carrotRed: {
    primary: '#d13636',
  },
  lightOrange: {
    primary: '#d17936',
  },
  mustard: {
    primary: '#d1ba36',
  },
  lemonGreen: {
    primary: '#aad136',
  },
  greenBlue: {
    primary: '#36d177',
  },
  lightViolet: {
    primary: '#c936d1',
  },
  darkViolet: {
    primary: '#7e3482',
  },
  pinkPurple: {
    primary: '#823452',
  },
  lightMaroon: {
    primary: '#823434',
  },
  darkBrown: {
    primary: '#826734',
  },
  leafGreen: {
    primary: '#518234',
  },
  darkTeal: {
    primary: '#34826d',
  },
  greyBlue: {
    primary: '#346282',
  },
  navyBlue: {
    primary: '#212552',
  },
  darkPurple: {
    primary: '#442152',
  },
  darkGreen: {
    primary: '#21523c',
  }
};

@Component({
  selector: 'mwl-demo-component',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./schedule.component.css'],
  templateUrl: './schedule.component.html'
})
export class ScheduleComponent implements OnInit {

  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  editable: boolean;
  eventStartDate: any;
  eventStartDateToUpdate: any;
  eventStartTime: any;
  eventEndTime: any;
  loading: boolean;
  showJoinButton: boolean;
  disableStartdate: boolean;
  allCourses: any;
  addCourseId: any;
  topic: any;
  addStartDate: any;
  addStartTime: any;
  addEndTime: any;
  endTimeLessThenStartTimeError: any;
  teacherListForFilterSchedule: any
  scheduledData: any;
  filteredScheduledData: any;
  filterTeacher: any
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  calendarEvents: CalendarEvent[] = [];
  modalData: {
    action: string;
    event: CalendarEvent;
  };
  scId: any;
  courseTitle: any;
  scTeacherError: any;
  scCourseError: any;
  scStartDateError: any;
  scStartTimeError: any;
  scEndTimeError: any;
  
  dropdownSettingsTeacher = {

    idField: 'teacherId',
    textField: 'teacherName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  permissionToDelete: boolean = false;
  @Input() teacherDetails: any;
  @Input() courseList: any;

  
  ngOnInit() {
    this.comservice.showNavbar = true;
    this.disableStartdate = false;
    let startDate = this.comservice.getFirstOfMonth(new Date());
    let endDate = this.comservice.getLastOfMonth(new Date());
    this.plotScheduledClasses(startDate, endDate);
    this.editable = false;
    this.eventStartDate = "";
    this.eventStartDateToUpdate = "";
    this.eventStartTime = "";
    this.eventEndTime = "";

  }



  actions: CalendarEventAction[] = [
    {
      label: ' <i class="fas fa-trash-alt"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        
        this.handleDeleteEvent('Deleted', event);
      }
    }
  ];
  refresh: Subject<any> = new Subject();
  events: Array<CalendarEvent<{ scId: number, courseId: number, class: any, teacherName: any, teacherId: any, subject: any, topic: any }>> = [];
  activeDayIsOpen: boolean = true;
  constructor(
    private modal: NgbModal,
    public comservice: CommonServiceService,
    private changedetetion: ChangeDetectorRef,
    private lpservice: LearningpodService) {

  }
  getDateInDoubleDigit(i) {
    let day = parseInt(i);
    if (day < 10) {
      return "0" + day;
    }
    else {
      return i;
    }
  }
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date))

      ) {
        if (events.length === 0) {
          this.activeDayIsOpen = false;
        } else {
          this.activeDayIsOpen = true;
        }
      }
      if (events.length > 0) {
        this.activeDayIsOpen = true;
      }
    }
  }
  disabledDates = (date: Date): boolean => {
    let todayDate = new Date();
    todayDate.setDate(todayDate.getDate() - 1)
    return date <= todayDate;
  }
  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  startTimeToUpdate: any;
  endTimeToUpdate: any;
  startDateToUpdate: any;
  handleEvent(action: string, event: CalendarEvent): void {


    this.scId = event.meta.scId;
    var courseTitleInArray = event.title.split(" ");
    // var array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
    var forIndex = courseTitleInArray.lastIndexOf("-");
    var filter = courseTitleInArray.slice(0, forIndex)
    this.courseTitle = filter.join(" ");

    this.showJoinButton = false;
    //.fieldsChanges = false;
    this.editable = false;
    this.modalData = { event, action };
    // this.disableStartdate = false;
    this.startTimeToUpdate = event.start;
    this.endTimeToUpdate = event.end;
    this.startDateToUpdate = event.start;
    this.eventStartDate = new Date(event.start).toLocaleDateString("sv-SE");
    var options = { hour: 'numeric', minute: '2-digit' };
    this.eventStartTime = new Date(event.start).toLocaleTimeString('en-US', options);
    this.eventEndTime = new Date(event.end).toLocaleTimeString('en-US', options);
    this.eventStartDateToUpdate = this.eventStartDate;
    let todaydate = new Date();
    let fifteenMinute = new Date(event.start);
    fifteenMinute.setMinutes(fifteenMinute.getMinutes() - 15);
    if (event.end > todaydate && fifteenMinute < todaydate) {
      this.showJoinButton = true;
    }
    // if (event.end < todaydate && fifteenMinute < todaydate) {
    //   this.disableStartdate = true;
    // }
    if (new Date() < new Date(event.end) || (new Date()).toDateString() == (new Date(event.end)).toDateString()) {
      this.disableStartdate = false;
    }
    else {
      this.disableStartdate = true;
    }
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  handleDeleteEvent(action: string, event: CalendarEvent): void {
    
    Swal.fire({
      backdrop:false,
      heightAuto: false,
      type: 'info',
      title: 'Are you sure you want to delete this scheduled class?',
      // text: "Deleting the student will delete all the payment information related to this student",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete class'
    }).then((result) => {
      if (result.value) {
        this.modalData = { event, action };
        this.loading = true;
        
        this.permissionToDelete = false;
    this.lpservice.deleteSc(event.meta.scId, this.permissionToDelete).subscribe((data) => {
      this.loading = false;
      if (data.message == "Delete permission is not available") {
        this.permissionToDelete = true;
        this.lpservice.deleteSc(event.meta.scId, this.permissionToDelete).subscribe((res) => {
          if(res.status == "success"){
            this.events = this.events.filter((iEvent) => iEvent !== event);
            this.loading = false;
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: `${res.message}`,
              timer: 2000
            });
          }
        })
      }
      if (data.message === "Virtual class information available, You can't delete it") {
        this.loading = false;
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: `${data.message}`,
          timer: 3000
        });
      }
      if(data.status == "success"){
        this.events = this.events.filter((iEvent) => iEvent !== event);
        this.loading = false;
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'success',
          title: 'Success!',
          text: `${data.message}`,
          timer: 2000
        });
      }

    });
  }})
  }
  Editform() {
    this.editable = true;
  }




  updateDetails(topic, teacherId) {
    this.loading = true;
    this.showJoinButton = false;

    //this.scTopic ;
    let localStartTime, localEndTime;
    var options = { hour: 'numeric', minute: '2-digit' };
    if (this.startTimeToUpdate instanceof Date) {
      localStartTime = this.startTimeToUpdate;
      this.eventStartTime = new Date(this.startTimeToUpdate).toLocaleTimeString('en-US', options);
    }
    else {
      this.eventStartTime = this.eventStartTime;
      localStartTime = this.eventStartTime;
    }
    if (this.endTimeToUpdate instanceof Date) {
      localEndTime = this.endTimeToUpdate;
      this.eventEndTime = new Date(this.endTimeToUpdate).toLocaleTimeString('en-US', options);

    }
    else {
      this.eventEndTime = this.eventEndTime;
      localEndTime = this.eventEndTime;
    }
    if (this.startDateToUpdate instanceof Date) {
      this.eventStartDateToUpdate = new Date(this.startDateToUpdate).toLocaleDateString("sv-SE");
    }
    else {
      this.eventStartDateToUpdate = this.eventStartDateToUpdate;
    }
    if (localEndTime < localStartTime) {
      this.endTimeLessThenStartTimeError = true;
      (document.getElementById('updateStartTime') as HTMLInputElement).style.borderColor = "red";
      (document.getElementById('updateEndTime') as HTMLInputElement).style.borderColor = "red";
      this.loading = false;
    }
    else {
      this.editable = false;
      var updatedTeacher = [];
      if(this.comservice.myUserRole == 'cc'){
       updatedTeacher = this.teacherDetails.filter(x => x.teacherInfo.teacherId == parseInt(teacherId));
      }
      else{
       updatedTeacher= [];
       updatedTeacher[0]={"teacherInfo": {"teacherId": this.comservice.myUserId, "teacherName": this.comservice.myUserName}}
      }
      this.modalData.event.meta.teacherName = updatedTeacher[0].teacherInfo.teacherName;
      this.lpservice.updateScheduledClass(this.eventStartDateToUpdate, this.eventStartTime, this.eventEndTime,
        this.modalData.event.meta.scId, topic, teacherId, this.modalData.event.meta.courseId).subscribe((data) => {
          this.loading = false;
          this.endTimeLessThenStartTimeError = false;
          if (data.status == "success") {
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: `${data.message}`,
              timer: 2000
            });
            this.events = [];
            var mydata = this.scheduledData;

            var index = mydata.findIndex(x => x.scId === this.modalData.event.meta.scId);


            mydata[index] = {
              "date": this.eventStartDateToUpdate, "startTime": this.eventStartTime,
              "endTime": this.eventEndTime, "topic": topic, "teacherInfo": {
                "teacherId": teacherId,
                "teacherName": updatedTeacher[0].teacherInfo.teacherName
              }, "scId": this.modalData.event.meta.scId, "courseInfo": {
                "classInfo": this.modalData.event.meta.class,
                "courseId": this.modalData.event.meta.courseId, "subjectInfo": this.modalData.event.meta.subject,
                "title": this.courseTitle
              }
            };
            (document.getElementById("closeModalJoin") as HTMLInputElement).click();
            this.plotdata(mydata);
          }
          else {
            this.loading = false;
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'error',
              title: 'Error!',
              text: `${data.message}`,
              timer: 2000
            })
          }
        })
    }
  }
  parseDate(date) {
    let a = new Date(date),
      timezoneOffset = a.getTimezoneOffset(),
      hours = ('00' + Math.floor(Math.abs(timezoneOffset / 60))).slice(-2),
      minutes = ('00' + Math.abs(timezoneOffset % 60)).slice(-2),
      string = (timezoneOffset >= 0 ? '-' : '+') + hours + ':' + minutes;
    return string;
  }

  plotdata(mydata) {
   
    for (let i = 0; i < mydata.length; i++) {

      let startTimeIn24Hr = this.comservice.convert12To24HourFormat(mydata[i].startTime);
      let endTimeIn24Hr = this.comservice.convert12To24HourFormat(mydata[i].endTime);
      var strtTimeIstTimeZone = new Date(mydata[i].date + "T" + startTimeIn24Hr + this.parseDate(mydata[i].date + "T" + startTimeIn24Hr));
      var endTimeIstTimeZone = new Date(mydata[i].date + "T" + endTimeIn24Hr + this.parseDate(mydata[i].date + "T" + endTimeIn24Hr));
      // this.titleToShowInModal = mydata[i].courseInfo.title;
   //   var a = startTimeIn24Hr;
    //  var currentTimeIn24Hr = this.comservice.convert12To24HourFormat(new Date().toLocaleTimeString());
      if(this.comservice.myUserRole == 'cc'){
       
          this.events.push({
            title: mydata[i].courseInfo.title + " - " + mydata[i].startTime + " to " + mydata[i].endTime,
            start: strtTimeIstTimeZone,
            end: endTimeIstTimeZone,
            color: this.getcolorcode(mydata[i].courseInfo.subjectInfo),
            draggable: false,
            actions: this.actions,
            resizable: {
              beforeStart: false,
              afterEnd: false
            },
            meta: {
              scId: mydata[i].scId,
              courseId: mydata[i].courseInfo.courseId,
              class: mydata[i].courseInfo.classInfo,
              teacherName: mydata[i].teacherInfo.teacherName,
              teacherId: mydata[i].teacherInfo.teacherId,
              subject: mydata[i].courseInfo.subjectInfo,
              topic: mydata[i].topic,
            }
          });
        
      }
      else{
        this.events.push({
          title: mydata[i].courseInfo.title + " - " + mydata[i].startTime + " to " + mydata[i].endTime,
          start: strtTimeIstTimeZone,
          end: endTimeIstTimeZone,
          color: this.getcolorcode(mydata[i].courseInfo.subjectInfo),
          draggable: false,
          resizable: {
            beforeStart: false,
            afterEnd: false
          },
          meta: {
            scId: mydata[i].scId,
            courseId: mydata[i].courseInfo.courseId,
            class: mydata[i].courseInfo.classInfo,
            teacherName: mydata[i].teacherInfo.teacherName,
            teacherId: mydata[i].teacherInfo.teacherId,
            subject: mydata[i].courseInfo.subjectInfo,
            topic: mydata[i].topic,
          }
        });
      }

this.events = this.events.sort((a: any, b: any) => a.start - b.start);
this.refresh.next();
}
    
  }
  disabledButton: boolean;
  addScCLassRemoveValidation() {
    (document.getElementById('addCourseId') as HTMLInputElement).style.borderColor = "lightgrey";
    (document.getElementById('addStartDate') as HTMLInputElement).style.borderColor = "lightgrey";
    (document.getElementById('addStartTime') as HTMLInputElement).style.borderColor = "lightgrey";
    (document.getElementById('addEndTime') as HTMLInputElement).style.borderColor = "lightgrey";
    this.disabledButton = false;
    this.allCourses = this.courseList;

  }

  addNewSc(courseId, addTeacherId, topic, startDate, startTime, endTime): void {
    this.disabledButton = true;
    this.loading = true;
    if (courseId == undefined || courseId == "") {
      (document.getElementById('addCourseId') as HTMLInputElement).style.borderColor = "red";
      this.disabledButton = false;
      this.loading = false;
    }
    if (startDate == undefined || startDate == "Invalid Date") {

      (document.getElementById('addStartDate') as HTMLInputElement).style.borderColor = "red";
      this.disabledButton = false;
      this.loading = false;
    }
    if (startTime == undefined || startTime == "Invalid Date") {
      (document.getElementById('addStartTime') as HTMLInputElement).style.borderColor = "red";
      this.disabledButton = false;
      this.loading = false;
    }
    if (endTime == undefined || endTime == "Invalid Date") {
      (document.getElementById('addEndTime') as HTMLInputElement).style.borderColor = "red";
      this.disabledButton = false;
      this.loading = false;
    }
    if (endTime < startTime) {
      (document.getElementById('addStartTime') as HTMLInputElement).style.borderColor = "red";
      (document.getElementById('addEndTime') as HTMLInputElement).style.borderColor = "red";
      this.endTimeLessThenStartTimeError = true;
      this.disabledButton = false;
      this.loading = false;
    }
    else {
      var options = { hour: 'numeric', minute: '2-digit' };
      startTime = new Date(startTime).toLocaleTimeString('en-US', options);
      endTime = new Date(endTime).toLocaleTimeString('en-US', options);
      startDate = new Date(startDate).toLocaleDateString("sv-SE");

      this.lpservice.addScheduledClass(courseId, addTeacherId, topic, startDate, startTime, endTime).subscribe((data) => {
        this.loading = false;
        this.disabledButton = false;
        this.endTimeLessThenStartTimeError = false;
        if (data.status == "success") {
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: `${data.message}`,
            timer: 2000
          })


          this.events = [];
          (document.getElementById("closeAddSc") as HTMLInputElement).click();
          let startDate = this.comservice.getFirstOfMonth(this.viewDate);
          let endDate = this.comservice.getLastOfMonth(this.viewDate);
          this.plotScheduledClasses(startDate, endDate);

          // this.scheduledData = result;
          // this.teacherListForFilterSchedule = result.filter(x=>x.teacherInfo != null);
          // this.teacherListForFilterSchedule = this.teacherListForFilterSchedule.map(x=>x.teacherInfo);
          // this.teacherListForFilterSchedule = this.removeDuplicates(this.teacherListForFilterSchedule,'teacherName');
          // this.loading = false;
          // this.plotdata(result);

          this.clearModal();
        }
        else {
          this.loading = false;
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: `${data.message}`,
            timer: 2000
          })
        }
      })
    }
  }

  clearModal() {
    this.addCourseId = undefined;
    this.addStartDate = "";
    this.addStartTime = "";
    this.addEndTime = "";
    this.topic = "";

  }
  removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject = {};
    for (var i in originalArray) {
      lookupObject[originalArray[i][prop]] = originalArray[i];
    }
    for (i in lookupObject) {
      newArray.push(lookupObject[i]);
    }
    return newArray;
  }



  plotScheduledClasses(startDate: string, endDate: string) {
    this.loading = true;
    this.events = [];
    this.lpservice.getscheduledclass(startDate, endDate).then(result => {
      this.scheduledData = result;
      this.teacherListForFilterSchedule = result.filter(x => x.teacherInfo != null);
      this.teacherListForFilterSchedule = this.teacherListForFilterSchedule.map(x => x.teacherInfo);
      this.teacherListForFilterSchedule = this.removeDuplicates(this.teacherListForFilterSchedule, 'teacherName');
      this.loading = false;
      this.plotdata(result);
      // this.courseListForTeacherSet = result;
    })
      .catch(error => console.log(error));
    this.loading = false;
  }
  teacherInfo: any;
  addTeacherId: any;

  setTeacher(event) {
    this.addTeacherId = undefined;
    let courseIdClicked = + event.target.value;
    for (var i = 0; i < this.allCourses.length; i++) {
      let courseIdFromMasterList = this.allCourses[i].courseId;
      if (this.allCourses[i].teacherInfo != undefined) {
        if (courseIdClicked == courseIdFromMasterList) {
          this.addTeacherId = this.allCourses[i].teacherInfo.teacherId;
        }
      }
    }
    (document.getElementById("teacherIdForCreateNewSc") as HTMLInputElement).value = this.addTeacherId;
  }

  cancelForUpdateSc() {
    this.editable = false;
  }


  joinMeeting(id, title) {
    this.lpservice.joinMeeting(id, title, this.comservice.myUserName, this.comservice.myUserRole).subscribe(res => {
      this.disabledButton = false;
      if (res.status == "success") {
        this.loading = false;
        window.open(res.completeUrl, "_blank");
        document.getElementById('closeModalJoin').click();
      }
      else {
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          title: 'Warning',
          text: `${res.message}`,
          type: 'warning',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Got it'
        }).then((result) => {
          if (result.value) {
            document.getElementById('closeModalJoin').click();
          }
        })
      }
    });

  }



  getcolorcode(subject) {
    if (subject == null) {
      return colors.black;
    }
    else {
      switch (subject.subjectName) {

        case "Mathematics":
          return colors.red;
        case "Science":
          return colors.orange;
        case "English":
          return colors.orangeYellow;
        case "Hindi":
          return colors.yellow;
        case "Social Science":
          return colors.yellowGreen;
        case "Information Technology":
          return colors.neonGreen;
        case "Artificial Intelligence":
          return colors.neonBlue;
        case "Custom":
          return colors.blue;
        case "Physics":
          return colors.brightBlue;
        case "Chemistry":
          return colors.lightPurple;
        case "Biology":
          return colors.purple;
        case "Language":
          return colors.black;
        case "Marathi":
          return colors.brightPink;
        case "SST":
          return colors.pink;
        case "Computer":
          return colors.maroon;
        case "Geography":
          return colors.blackGreen;
        case "ICT":
          return colors.teal;
        case "English-I":
          return colors.violet;
        case "English-II":
          return colors.lightGrey;
        case "History":
          return colors.grey;
        case "PED":
          return colors.lightPink;
        case "Commerce":
          return colors.darkGrey;
        case "Economics":
          return colors.carrotRed;
        case "Science-I":
          return colors.lightOrange;
        case "Science-II":
          return colors.mustard;
        case "BS":
          return colors.lemonGreen;
        case "Home Science":
          return colors.greenBlue;
        case "Business Science":
          return colors.lightViolet;
        case "Pyschology":
          return colors.darkViolet;
        case "Painting":
          return colors.pinkPurple;
        case "Data Entry":
          return colors.lightMaroon;
        case "OCM":
          return colors.darkBrown;
        case "SP":
          return colors.leafGreen;
        case "BM":
          return colors.darkTeal;
        case "Math-I":
          return colors.greyBlue;
        case "Math-II":
          return colors.navyBlue;
        case "Accountancy":
          return colors.darkPurple;
        default:
          return colors.black;
      }
    }
  }

  

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
    let startDate = this.comservice.getFirstOfMonth(this.viewDate);
    let endDate = this.comservice.getLastOfMonth(this.viewDate);
    this.plotScheduledClasses(startDate, endDate);
    //this.events = [];
  }


  filterScheduledClasses() {

    if (this.filterTeacher == undefined || Array.isArray(this.filterTeacher) && this.filterTeacher.length == 0) {
      this.filteredScheduledData = null;
      this.filterTeacher = undefined
      this.events = [];
      this.plotdata(this.scheduledData);
    }
    //for filtering class
    if (Array.isArray(this.filterTeacher) && this.filterTeacher.length) {
      let clearData1 = this.scheduledData.filter(x => x.teacherInfo != null);
      let classForFilter = this.filterTeacher.map(x => x.teacherName)
      this.filteredScheduledData = clearData1.filter(function (e) {
        return classForFilter.includes(e.teacherInfo.teacherName)
      });
      if (Array.isArray(this.filteredScheduledData) && this.filteredScheduledData.length) {
        this.events = [];
        this.plotdata(this.filteredScheduledData)
      } else {
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'warning',
          title: 'Warning!',
          text: 'No schedule courses found',
          timer: 3000
        })
        this.events = [];
        this.plotdata(this.scheduledData);
      }

    }

  }

  clearFilter() {
    this.filteredScheduledData = null;
    this.filterTeacher = undefined
    this.events = [];
    this.plotdata(this.scheduledData);
  }




}