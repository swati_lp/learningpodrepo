import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { CommonServiceService } from '../../common-service.service';
import { LearningpodService } from '../../learningpod.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MessagingService } from '../../shared/models/messaging.service';
import emailjs from 'emailjs-com';

import Swal from 'sweetalert2'
declare let jQuery: any;
declare var $: any;

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  //registerForm: FormGroup;
  submitted: boolean;
  showerrormessage_login: boolean;
  role: any;
  phone_number: any;
  accesscode: boolean;
  access_code: any;
  password: any;
  conf_password: any;
  name: any;
  errorMsg: any;
  ccName: any;
  phoneNo: any;

  showaccesscodeinput: boolean;
  forgetPasswordPhoneNo: any;
  errortext_signup: any;
  showerrormessage_signup: boolean;
  phonenumberdiv: boolean;
  accesscodediv: boolean;
  passworddiv: boolean;
  forgetPassword: boolean;
  //forgetPasswordPhoneNo: any;
  // forgetPasswordAccessCode: any;
  forget_password_password: any;
  forget_password_conf_password: any;
  random_ac: any;
  disableSubmit: boolean = false;
  accessCodeSent: any;
  defaultProfilePic: any;
  loading: boolean;
  userNameInvalid: boolean;
  serverDown: any;
  forgetPasswordAccessCode: any;
  forgetPasswordNewPassword: any;
  forgetPasswordConfirmPassword: any;
  email = ""
  message = ""
  timeToTalk = ""
  constructor(public comservice: CommonServiceService,
    public lpService: LearningpodService,
    public formbuilder: FormBuilder,
    public router: Router,
    public element: ElementRef,
    public messagingService: MessagingService,
    //  private authService: AuthService

  ) {

  }


  socialSignInData = {
    mobileNo: '',
    email: '',
    name: '',
    token: '',
    provider: ''
  };

  count() {
    $('.count').each(function () {
      $(this).prop('Counter', 0).animate({
        Counter: $(this).text()
      }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
          $(this).text(Math.ceil(now));
        }
      });
    });
  }

  ngOnInit() {
    //  this.getUserData();
    //$('body').removeClass('stop-scrolling')
    this.comservice.showNavbar = true;
    // this.count();
    this.accessCodeSent = false;
    this.submitted = false;
    this.showerrormessage_login = false;
    this.phonenumberdiv = true;
    this.accesscodediv = false;
    this.passworddiv = false;
    this.forgetPassword = false;
    this.loading = false;
    // this.findIP(this.addIP);
    // this.registerForm = this.formbuilder.group({
    //   phone_number: ['', Validators.required],
    //   password: ['', [Validators.required, Validators.maxLength(10)]],
    //   role: ['', [Validators.required]]
    // });
    // this.role = "cc";
    this.accesscode = true;
    this.showaccesscodeinput = false;
    this.phone_number = '';
    //this.forgetPasswordAccessCode = '';
    this.forget_password_conf_password = '';
    this.forget_password_password = '';
    //this.forgetPasswordPhoneNo = '';
    this.userRoleNeeded = false;
    this.userNumberNeeded = false;
    this.userPasswordNeeded = false;
    this.userNameInvalid = false;
    this.serverDown = false;
    $(document).keyup(function (e) {
      if (e.key === "Escape") { // escape key maps to keycode `27`
        document.getElementById("close").click();
      }
    });


  }

  getAllPermissions (){
    return (navigator as any).permissions.query({
      name: 'notifications'
    }).then((permission) => {
        if(permission.state === "denied"){
          alert("Allow Notifications from your browser settings to recieve notifications of approaching live class")
        }
    });
  }
  formatusername(name) {
    let formattedName = "";
    if (name.length < 15) {
      return name;
    }
    else {
      for (let i = 0; i < 12; i++) {
        formattedName += name[i];
      }
      formattedName += "...";
      return formattedName;
    }
  }
  removeValidationUserRole() {
    (document.getElementById('loginUserRole') as HTMLInputElement).style.borderColor = "lightgrey";
    (document.getElementById('forgetPasswordRole') as HTMLInputElement).style.borderColor = "lightgrey";
    this.userRoleNeeded = false;
  }
  removeValidationUserNumber() {
    (document.getElementById('loginPhoneNo') as HTMLInputElement).style.borderColor = "lightgrey";
    (document.getElementById('forgetPasswordNo') as HTMLInputElement).style.borderColor = "lightgrey";
    this.userNumberNeeded = false;
  }
  removeValidationUserPassword() {
    (document.getElementById('loginPassword') as HTMLInputElement).style.borderColor = "lightgrey";
    this.userPasswordNeeded = false;
    (document.getElementById('newPassword') as HTMLInputElement).style.borderColor = "lightgrey";
    this.newPasswordNeeded = false;
    (document.getElementById('confirmPassword') as HTMLInputElement).style.borderColor = "lightgrey";
    this.confirmPasswordNeeded = false;
  }
  removeValidationAccessCode() {
    (document.getElementById('forgotPasswordAccessCode') as HTMLInputElement).style.borderColor = "lightgrey";
    this.forgotPassAccessCodeNeeded = false;
  }

  upperLoginClicked() {
    (document.getElementById('loginUserRole') as HTMLInputElement).style.borderColor = "lightgrey";
    this.userRoleNeeded = false;
    (document.getElementById('loginPhoneNo') as HTMLInputElement).style.borderColor = "lightgrey";
    this.userNumberNeeded = false;
    (document.getElementById('loginPassword') as HTMLInputElement).style.borderColor = "lightgrey";
    this.userPasswordNeeded = false;
    this.userNameInvalid = false;
    this.serverDown = false;
    this.updatedPassword = false;
    this.accesscodediv = false;
    this.forgetPasswordAccessCode = undefined;
    this.forgetPasswordNewPassword = undefined;
    this.forgetPasswordConfirmPassword = undefined;
  }

  // get f() { return this.registerForm.controls; }
  userRoleNeeded: boolean;
  userNumberNeeded: boolean;
  userPasswordNeeded: boolean;
  login(loginUserRole, loginPhoneNo, loginPassword) {
    this.userNameInvalid = false;
    this.serverDown = false;
    if (loginUserRole == undefined || loginPhoneNo == undefined || loginPassword == undefined) {
      if (loginUserRole == undefined) {
        (document.getElementById('loginUserRole') as HTMLInputElement).style.borderColor = "red";
        this.userRoleNeeded = true;
      }
      else {
        (document.getElementById('loginUserRole') as HTMLInputElement).style.borderColor = "lightgrey";
        this.userRoleNeeded = false;
      }
      if (loginPhoneNo == undefined) {
        (document.getElementById('loginPhoneNo') as HTMLInputElement).style.borderColor = "red";
        this.userNumberNeeded = true;
      }
      else {
        (document.getElementById('loginPhoneNo') as HTMLInputElement).style.borderColor = "lightgrey";
        this.userNumberNeeded = false;
      }
      if (loginPassword == undefined) {
        (document.getElementById('loginPassword') as HTMLInputElement).style.borderColor = "red";
        this.userPasswordNeeded = true;
      }
      else {
        (document.getElementById('loginPassword') as HTMLInputElement).style.borderColor = "lightgrey";
        this.userPasswordNeeded = false;
      }
    }
    else {
      this.loading = true;
      document.getElementById("loginRegisterForm").style.cursor = "progress";
      this.lpService.login(loginPhoneNo, loginPassword, loginUserRole, 1, 2).subscribe(data => {
        document.getElementById("loginRegisterForm").style.cursor = "default";
        this.handleLoginResponse(data);
      }, (err) => {
        if (err.status == 0) {
          this.serverDown = true;
          this.loading = false;
          document.getElementById("loginRegisterForm").style.cursor = "default";
        }
      });
    }
    
  }
  // onSubmit() {
  //   this.submitted = true;
  //   this.login(this.registerForm.value.phone_number, this.registerForm.value.password, this.registerForm.value.role);
  // };

  handleLoginResponse(data) {
    this.loading = false;
    if (data.body.status == "success") {

      jQuery('#modalLoginForm').modal('hide');
      let myUserRole = data.body.userRole.toLowerCase();
      let myUserName = data.body.userName;
      let myUserId = Number.parseInt(data.body.userId);
      let myAccessToken = data.headers.get("authorization").split(" ");
      let myProfileUrl = data.body.userProfileUrl;
      this.comservice.userLoggedIn(myUserRole, myUserId, myUserName, myAccessToken[1], myProfileUrl);
      this.lpService.setheaders();
 
      
      this.getAllPermissions();
      this.router.navigateByUrl('home/profile');

      // introJs().setOption('showStepNumbers', false).start();
      const userId = 'user' + myUserId;
      this.messagingService.requestPermission(userId);

        

    }

    else if (data.body.status == "error") {
      // Swal.fire({
      //   type: 'error',
      //   title: 'Error!',
      //   text: `${data.body.message}`,
      //   timer: 2000
      // })
      this.userNameInvalid = true;
      this.errorMessage = data.body.message;
    }
  }
  errorMessage: any;


  forgotPasswordSendAccessCode(role, forgetPasswordPhoneNo) {

    if (role == undefined || forgetPasswordPhoneNo == undefined) {
      if (role == undefined) {
        (document.getElementById('forgetPasswordRole') as HTMLInputElement).style.borderColor = "red";
        this.userRoleNeeded = true;
      }
      else {
        (document.getElementById('forgetPasswordRole') as HTMLInputElement).style.borderColor = "lightgrey";
        this.userRoleNeeded = false;
      }
      if (forgetPasswordPhoneNo == undefined) {
        (document.getElementById('forgetPasswordNo') as HTMLInputElement).style.borderColor = "red";
        this.userNumberNeeded = true;
      }
      else {
        (document.getElementById('forgetPasswordNo') as HTMLInputElement).style.borderColor = "lightgrey";
        this.userNumberNeeded = false;
      }
    }
    else {
      // this.passworddiv = true;
      this.accesscodediv = true;
      this.phonenumberdiv = false;
      this.forgetPasswordPhoneNo = forgetPasswordPhoneNo;
      this.role = role;
      this.lpService.forgetPassword(forgetPasswordPhoneNo, role).subscribe(res => {
        if (res.status == "success") {
          this.accessCodeSent = true;
          // this.phonenumberdiv = false;
        }
        else {
          this.serverError = true;
        }
      })
    }
  }
  serverError: any;
  updatedPassword: any;
  passWordMismatch: any;
  forgotPassAccessCodeNeeded: any;
  newPasswordNeeded: any;
  confirmPasswordNeeded: any;
  OtpError: any;
  updatepassword(forgetPasswordAccessCode, forgetPasswordNewPassword, forgetPasswordConfirmPassword) {
    if (forgetPasswordAccessCode == undefined || forgetPasswordNewPassword == undefined || forgetPasswordConfirmPassword == undefined) {
      if (forgetPasswordAccessCode == undefined) {
        (document.getElementById('forgotPasswordAccessCode') as HTMLInputElement).style.borderColor = "red";
        this.forgotPassAccessCodeNeeded = true;
      }
      else {
        (document.getElementById('forgotPasswordAccessCode') as HTMLInputElement).style.borderColor = "lightgrey";
        this.forgotPassAccessCodeNeeded = false;
      }
      if (forgetPasswordNewPassword == undefined) {
        (document.getElementById('newPassword') as HTMLInputElement).style.borderColor = "red";
        this.newPasswordNeeded = true;
      }
      else {
        (document.getElementById('newPassword') as HTMLInputElement).style.borderColor = "lightgrey";
        this.newPasswordNeeded = false;
      }
      if (forgetPasswordConfirmPassword == undefined) {
        (document.getElementById('confirmPassword') as HTMLInputElement).style.borderColor = "red";
        this.confirmPasswordNeeded = true;
      }
      else {
        (document.getElementById('confirmPassword') as HTMLInputElement).style.borderColor = "lightgrey";
        this.confirmPasswordNeeded = false;
      }
    }
    else {
      if (forgetPasswordNewPassword == forgetPasswordConfirmPassword) {
        this.passWordMismatch = false;
        this.lpService.resetPassword(this.forgetPasswordPhoneNo, forgetPasswordAccessCode, forgetPasswordNewPassword, this.role).subscribe(res => {
          if (res.status == "success") {
            this.updatedPassword = true;
            this.passWordMismatch = false;
            this.OtpError = false;
          }
          else {
            this.OtpError = true;
          }
        });
      }
      else {
        this.passWordMismatch = true;
      }
    }
  }

  // resendOtp() {
  //   if (this.phone_number == null || this.phone_number == undefined || this.phone_number == '') {
  //     (document.getElementById("phoneNumber") as HTMLInputElement).style.borderColor = "red";
  //     this.showerror("Phone Number");
  //     return false;
  //   }
  //   else {
  //     (document.getElementById("phoneNumber") as HTMLInputElement).style.borderColor = "lightgrey";
  //     this.accessCodeSent = true;
  //     this.lpService.resetOtp("cc", this.phone_number).subscribe(res => {

  //     })

  //   }
  // }

  showPasswordLogin() {
    var x = document.getElementById("loginPassword") as HTMLInputElement;

    if (x.type === "password") {
      x.type = "text";

    } else {
      x.type = "password";

    }
  }
  viewDemo() {
    document.getElementById("viewDemo").style.display = "block";

  }
  closeDemo() {
    $("#viewDemo").hide("slow"); 
    var videos = document.querySelectorAll('iframe, video');
    Array.prototype.forEach.call(videos, function (video) {
      if (video.tagName.toLowerCase() === 'video') {
        video.pause();
      } else {
        var src = video.src;
        video.src = src;
      }
    });
  }

  validateEmail(name, phoneNo) {
    if (name == undefined) {
      this.errorMsg = "Please enter your name!";
    }
    else if (phoneNo == undefined) {
      this.errorMsg = "Please enter your phone number";
    }
    else {
      return true;
    }
  }


  sendEmail(name, ccName, phoneNo, email, message,timeToTalk) {

    if (this.validateEmail(name, phoneNo) != true) return;

    this.errorMsg = "";
    this.disableSubmit = true;
    var from;
    document.getElementById("submit").innerText = "Submitting...";
    if (email === undefined) {
      from = "support@learningpod.in"
    }
    else {
      from = email;
    }
    var templateParams = {
      to_name: 'support@learningpod.in',
      from_email: from,
      from_name: name,
      cc_name: ccName,
      phone_no: phoneNo,
      message_html: message,
      preferred_time: timeToTalk
    };

    emailjs.send('lp_support', 'template_Vs297hzd', templateParams, 'user_KMQr0RHcABaUcUfw3mwTi')
      .then((response) => {

        Swal.fire({
          type: 'success',
          title: 'Success!',
          text: "Thanks for your information. We will get in touch with you soon",
          timer: 5000
        })
        this.disableSubmit = false;
        document.getElementById("submit").innerText = "Submit";
        document.getElementById("close").click();
      }, (err) => {
        Swal.fire({
          type: 'error',
          title: 'Error!',
          text: "Mail sending failed",
          timer: 4000
        })
        this.disableSubmit = false;
        document.getElementById("submit").innerText = "Submit";
      });

  }

  
}