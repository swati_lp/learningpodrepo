import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import { CourseComponent } from '../course/course.component';
import { ScheduleComponent } from '../schedule/schedule.component';
import { PaymentStatsComponent } from '../payment-stats/payment-stats.component';
import { MyStudentsComponent } from '../my-students/my-students.component';
import { LearningpodService } from '../../learningpod.service';
import Swal from 'sweetalert2';
import * as introJs from 'intro.js/intro.js';
import { AccountsComponent } from '../accounts/accounts.component';
import { MyTeachersComponent } from '../my-teachers/my-teachers.component';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  @ViewChild(MyTeachersComponent) MyTeachersComponent: MyTeachersComponent;
  @ViewChild(MyStudentsComponent) MyStudentsComponent: MyStudentsComponent;
  @ViewChild(CourseComponent) courseComponent: CourseComponent;
  @ViewChild(ScheduleComponent) scheduleComponent: ScheduleComponent;
  @ViewChild(PaymentStatsComponent) PaymentStatsComponent: PaymentStatsComponent;
  @ViewChild(AccountsComponent) AccountsComponent: AccountsComponent;


  constructor(public comservice: CommonServiceService, private lpservice: LearningpodService) { }

  teacherDetails: any;
  teacherList: any;
  isCourseChanged: boolean;
  firstLogin: string;
  isDeleteEvent: any;
  studentDetails: any;
  teacherDetailsForAccountPage: any;
  ccDetailsForAccountPage: any;
  studentDetailsForAccountPage: any;
  loadComponent: boolean;
  displayMyRecordings: boolean;
  displayMyStudents: boolean;
  displayMyPayments: boolean;
  loading: boolean;
  preferenceData: {
    ccPreference: any,
    ccTeacherPreference: any,
    ccStudentPreference: any,
    ccLiveClassPreference: any
  }
  teacherCount: any;
  studentCount: any;

  ngOnInit() {
    this.loadComponent = false;
    this.comservice.showNavbar = true;
    this.firstLogin = localStorage.getItem('firstLogin')
    this.displayMyRecordings = false;
    this.displayMyStudents = false;
    this.displayMyPayments = false;
    this.loading = true;
    document.getElementById("scrollDivTeacher").scrollTop = 0;
    if (this.comservice.myUserRole == 'teacher') {
      this.getTeacherDetails();
    }
    if (this.comservice.myUserRole == 'cc') {
      this.getCcDetails();
    }
    if (this.comservice.myUserRole == 'student') {
      this.getStudentDetails();
    }


    $(document).ready(function () {
      $(".page-wrapper").removeClass("toggled");
      $(".sidebar-dropdown > a").click(function () {
        $(".sidebar-submenu").slideUp(200);
        if (
          $(this)
            .parent()
            .hasClass("active")
        ) {
          $(".sidebar-dropdown").removeClass("active");
          $(this)
            .parent()
            .removeClass("active");
        } else {
          $(".sidebar-dropdown").removeClass("active");
          $(this)
            .next(".sidebar-submenu")
            .slideDown(200);
          $(this)
            .parent()
            .addClass("active");
        }
      });
      $("#close-sidebar").click(function () {
        $(".page-wrapper").removeClass("toggled");
      });
      $("#show-sidebar").click(function () {
        $(".page-wrapper").addClass("toggled");
      });
    });
    //this.getaffiliationdata();

    //  this.getCBSForCreateCourse();
  
  }

  startTour() {
    introJs().setOption('showStepNumbers', false).start();
    localStorage.removeItem("firstLogin");
  }
  openNav() {
    document.getElementById("mobileViewOfMyProfileTabs").style.width = "250px";
  }

  closeNav() {
    document.getElementById("mobileViewOfMyProfileTabs").style.width = "0";
  }

  teacherStepComplete: boolean = true;
  getUpdatedTeacherList(event) {
    this.teacherCount = event.length;
    if (event != undefined && event.length > 0) {
      this.teacherStepComplete = true;
    }
    if (event == undefined) {
      this.teacherDetails = event;
      this.teacherStepComplete = false;
      // this.teacherDetailsForSetupUi = event;
    }
    if (Array.isArray(event)) {
      this.teacherDetails = event;
      // this.teacherDetailsForSetupUi = event;
      this.teacherList = event.map(x => x.teacherInfo);
      // this.PaymentStatsComponent.getCoursePaymentStats();
    }
  }

  courseList: any
  courseStepComplete: boolean = true;
  courseCount: any;
  isNewCourseUpdated(event) {
    this.courseCount = event.length;
    if (event.length > 0) {
      this.courseStepComplete = true;
    }
    else {
      this.courseStepComplete = false;
    }
    //this.scheduleComponent.addScCLassRemoveValidation(event);
    this.courseList = event;
  }
  courseIdDeleted: any;
  deletedCourseId() {
    let startDate = this.comservice.getFirstOfMonth(new Date());
    let endDate = this.comservice.getLastOfMonth(new Date());
    this.scheduleComponent.plotScheduledClasses(startDate, endDate);
    this.PaymentStatsComponent.getCoursePaymentStats();
  }

  studentStepComplete: boolean = true;
  getUpdatedStudentList(event) {
    this.getCcDetails();
    if (event != undefined && event.length > 0) {
      this.studentStepComplete = true;

    }
    if (event == undefined) {
      this.studentDetails = event;
      // this.studentDetailsForSetupUi = event;
      this.studentStepComplete = false;
    }
    if (Array.isArray(event)) {
      this.studentDetails = event;
      // this.studentDetailsForSetupUi = event;
    }
  }

  isDelete(event) {
    this.isDeleteEvent = event;
    if (this.isDeleteEvent) {
      this.courseComponent.getallcourses();
      let startDate = this.comservice.getFirstOfMonth(new Date());
      let endDate = this.comservice.getLastOfMonth(new Date());
      // this.scheduleComponent.plotScheduledClasses(startDate, endDate);
      // this.PaymentStatsComponent.getCoursePaymentStats();
    }

  }

  isCourseUpdated(event) {
    this.isCourseChanged = event;
    if (this.isCourseChanged == true) {

      let startDate = this.comservice.getFirstOfMonth(new Date());
      let endDate = this.comservice.getLastOfMonth(new Date());
      this.scheduleComponent.plotScheduledClasses(startDate, endDate);
      this.PaymentStatsComponent.getCoursePaymentStats();
    }
  }

  isStudentEnrolled(event) {
    if (event == true) {
      this.PaymentStatsComponent.getCoursePaymentStats();
    }
  }



  isPaymentUpdatedFromStudent(event) {
    if (event == true) {
      this.PaymentStatsComponent.getCoursePaymentStats();
    }

  }

  isStudentEnrolledFromStudent(event) {
    if (event == true) {
      this.PaymentStatsComponent.getCoursePaymentStats();
    }
  }

  isPaymentUpdatedFromCourse(event) {
    if (event == true) {
      this.PaymentStatsComponent.getCoursePaymentStats();
    }
  }


  getCcDetails() {
    this.lpservice.getUserDetails(this.comservice.myUserRole, this.comservice.myUserId).subscribe(res => {
      this.loading = false;
      this.preferenceData = res.preferenceDetails;

      if (res.ccInfo.loginFlag === "true") {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
          // some code..
        } else {
          this.startTour();
        }
        this.lpservice.changeFirstLoginStatus(this.comservice.myUserRole, this.comservice.myUserId).subscribe(res => {
        })
      }
      this.ccDetailsForAccountPage = res;
      this.loadComponent = true;
      this.displayMyRecordings = true;
      this.displayMyStudents = true;
      this.displayMyPayments = true;
      this.comservice.ccTeacherPreference(true, true, true, true, true, true, true);

    })
  }

  getTeacherDetails() {
    this.lpservice.getUserDetails(this.comservice.myUserRole, this.comservice.myUserId).subscribe(res => {
      this.loading = false;
      this.preferenceData = res.ccTeacherPreference;
      if (res.teacherInfo.loginFlag === "true") {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        } else {
          setTimeout(() => {
            this.startTour();
          }, 1000);
        }
        this.lpservice.changeFirstLoginStatus(this.comservice.myUserRole, this.comservice.myUserId).subscribe(res => {
        })
      }
      this.teacherDetailsForAccountPage = res;
      this.loadComponent = true;
      this.displayMyPayments = false;
      this.comservice.ccTeacherPreference(res.ccTeacherPreference.allowAddSchedule, res.ccTeacherPreference.allowCreateCourse,
        res.ccTeacherPreference.allowEditSchedule, res.ccTeacherPreference.allowEnrollStudent,
        res.ccTeacherPreference.allowPaymentModification, res.ccTeacherPreference.allowUploadNotes, res.ccTeacherPreference.showExploreCoursesAndTeacher);

      if (res.ccTeacherPreference.displayMyRecordings == true) {
        this.displayMyRecordings = true;
      }
      else {
        this.displayMyRecordings = false;
      }

      if (res.ccTeacherPreference.displayMyStudents == true) {
        this.displayMyStudents = true;
      }
      else {
        this.displayMyStudents = false;
      }
    })
  }

  getStudentDetails() {
    this.lpservice.getUserDetails(this.comservice.myUserRole, this.comservice.myUserId).subscribe(res => {
      this.loading = false;
      this.preferenceData = res.ccStudentPreference;
      if (res.studentInfo.loginFlag === "true") {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
          // some code..
        } else {
          setTimeout(() => {
            this.startTour();
          }, 3000)
        }
        this.lpservice.changeFirstLoginStatus(this.comservice.myUserRole, this.comservice.myUserId).subscribe(res => {
        })
      }
      this.studentDetailsForAccountPage = res;
      this.loadComponent = true;
      this.displayMyStudents = false;
      this.comservice.ccStudentPreference(res.ccStudentPreference.allowRating, res.ccStudentPreference.enquiryForCourse, res.ccStudentPreference.showExploreCoursesAndTeacher);
      if (res.ccStudentPreference.displayMyPayments == true) {
        this.displayMyPayments = true;
      }
      else {
        this.displayMyPayments = false;
      }
      if (res.ccStudentPreference.displayMyRecordings == true) {
        this.displayMyRecordings = true;
      }
      else {
        this.displayMyRecordings = false;
      }

    })
  }

  // isPreferenceAvailable(event) {
  //   this.preferenceData = event;
  // }

  setCcPreference() {
    if (this.comservice.myUserRole == 'cc') {
      if (this.preferenceData.ccPreference) {
        this.preferenceData.ccPreference['ccInformation'] = this.comservice.myUserId;
      }
      if (this.preferenceData.ccTeacherPreference) {
        this.preferenceData.ccTeacherPreference['ccInformation'] = this.comservice.myUserId;
      }
      if (this.preferenceData.ccStudentPreference) {
        this.preferenceData.ccStudentPreference['ccInformation'] = this.comservice.myUserId;
      }
      if (this.preferenceData.ccLiveClassPreference) {
        this.preferenceData.ccLiveClassPreference['ccInformation'] = this.comservice.myUserId;
      }

      this.lpservice.setCcPreferences(this.preferenceData).subscribe(res => {
        if (res.status == "success") {
          this.AccountsComponent.getCcDetails();
          $(".page-wrapper").removeClass("toggled");
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: 'Settings updated successfully!',
            timer: 3000
          })
        } else {
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: 'Preferences failed to save!',
            timer: 3000
          })
        }
      })
    }
  }


  getTeacherStudentCount(event) {
    this.teacherStudentCount = event;
    if( this.comservice.myUserRole == 'cc'){
    this.teacherCount = event.teacherCount;
    this.studentCount = event.studentCount;
    }
    
  }
  teacherStudentCount: any;

  isTeacherAdded(event) {
    this.teacherCount = event.length;
    if (event == true) {
      this.MyTeachersComponent.getaffiliationdata();
    }
  }
  isStudentAdded(event) {
   
    if (event == true) {
      this.MyStudentsComponent.getAffiliatedStudent();
    }
  }
  isCourseAdded(event) {
    if (event == true) {
      this.courseComponent.getallcourses();
      let startDate = this.comservice.getFirstOfMonth(new Date());
      let endDate = this.comservice.getLastOfMonth(new Date());
      this.scheduleComponent.plotScheduledClasses(startDate, endDate);
    }
  }
  tabClicked(){
    document.getElementById("scrollDivTeacher").scrollTop = 0;
  }

}