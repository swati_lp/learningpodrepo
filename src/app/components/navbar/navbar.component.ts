import { Component, OnInit, } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { LearningpodService } from '../../learningpod.service';
import Swal from 'sweetalert2';
import * as introJs from 'intro.js/intro.js';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  notifications: any;
  notificationsCount: number;

  constructor(
    public comservice: CommonServiceService,
    public lpService: LearningpodService,
    public formbuilder: FormBuilder,
    public router: Router,
  ) {

  }
  isLanding: boolean
ccName : any
  ngOnInit() {
  this.comservice.currentCc.subscribe(ccName=> this.ccName = ccName)
    this.notificationsCount = 0;
    // setInterval(() => {
    // }, 5000);
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }
  
   closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  startTour(){
    
    this.router.navigateByUrl('home/profile').then(()=>{
      introJs().setOption('showStepNumbers', false).start();
    });

    
   // localStorage.removeItem("firstLogin");
  }



  formatusername(name) {
    let formattedName = "";
    if (name.length < 15) {
      return name;
    }
    else {
      for (let i = 0; i < 12; i++) {
        formattedName += name[i];
      }
      formattedName += "...";
      return formattedName;
    }
  }


  setExploreHistory(data) {

    this.comservice.exploreHistory = data;
    localStorage.setItem("exploreHistory", data);
  }

  logout() {
    this.comservice.changeCcName("");
    this.comservice.currentCc.subscribe(ccName=> this.ccName = ccName)
    this.lpService.logout(this.comservice.myUserId).subscribe(data => {
      if (data.status == "success") {
        this.notificationsCount = 0;
      }
    });
    this.comservice.removeCcInfo();
    this.comservice.userLoggedOut();
    this.router.navigateByUrl('/')
  }


  getEnrollNotifications() {
    this.lpService.getEnrollNotifications().subscribe((data) => {
      this.notifications = data.enrollmentRequestDetails;
      if (data.enrollmentRequestDetails.length > 0) {
        this.notificationsCount = data.enrollmentRequestDetails.length;
      }
    })

  }

  getEnquiryNotifications() {
    this.lpService.getEnquiryNotifications().subscribe((data) => {
      this.notifications = data.requests;
    })

  }



  rejectRequest(requestId) {
    this.lpService.rejectRequestNotification(requestId).subscribe((data) => {
      (document.getElementById("rejectClicked" + requestId) as HTMLInputElement).disabled = true;
      (document.getElementById("acceptClicked" + requestId) as HTMLInputElement).disabled = true;
      //alert(data.message);
    })
  }
  acceptRequest(requestId) {
    this.lpService.acceptRequestNotification(requestId).subscribe((data) => {

      (document.getElementById("rejectClicked" + requestId) as HTMLInputElement).disabled = true;
      (document.getElementById("acceptClicked" + requestId) as HTMLInputElement).disabled = true;
      if (data.status == "success") {
        Swal.fire({
          backdrop:false,
          heightAuto: false,
          type: 'success',
          title: 'Success!',
          text: `${data.message}`,
          timer: 2000
        })
      }
      // alert(data.message);
    })
  }








}

