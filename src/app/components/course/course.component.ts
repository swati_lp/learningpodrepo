import { Component, OnInit, ChangeDetectorRef, Input, Output } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import { LearningpodService } from '../../learningpod.service';
import { Course } from '../../shared/models/course.model';
import { FormsModule } from '@angular/forms';
import Swal from 'sweetalert2';
import { EventEmitter } from '@angular/core';
//import { TimeSelectorComponent } from '@progress/kendo-angular-dateinputs';


@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {

  isDisabled = true;
  filteredCourseData = null
  isFilteredDataAvailable: boolean;
  dropdownSettingsForFrequency = {};
  showDelete: boolean = true;
  enrollmentPermission: boolean = false;
  deletePermission: boolean = false;
  dropdownSettingsForStudents = {
    singleSelection: false,
    idField: 'studentId',
    textField: 'studentName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  dropdownSettingsClass = {
    singleSelection: true,
    idField: 'classId',
    textField: 'className',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true,

  };
  dropdownSettingsBoard = {
    singleSelection: true,
    idField: 'boardId',
    textField: 'boardName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  dropdownSettingsSubject = {
    singleSelection: true,
    idField: 'subjectId',
    textField: 'subjectName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };

  dropdownSettingsTeacher = {
    singleSelection: true,
    idField: 'teacherId',
    textField: 'teacherName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };

  frequency = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  studentId: any;
  showcolor: boolean;
  courseNotes: any;
  showavailableonPhonemsg: boolean;
  courseToAdd: Course;
  courseToUpdate: Course;
  boardList: any;
  subjectListForAddCourse: any;
  //  loadingVideo: boolean;
  ratingToUpdateRating: string;
  teacherRatingToUpdateRating: any;
  ccRatingToUpdateRating: any;
  frequencyInString: any;
  teacherData: any;
  reviewData: any = [];
  showReview: boolean = false;
  courseUpdated: boolean;
  studentList: any;
  loading: boolean;
  subNotAddedError: any;
  courseFrequencyArr = [];
  selectedfile: any;
  //  videoUrl: any;
  disableStartDate: boolean;

  courseId: any;
  noStudentsAdded: boolean;
  courseIdForEnroll: any
  courseDetails: any;
  isStudentEnrolledVal: boolean = false;
  isPaymentUpdatedVal: boolean = false;
  //editParticularFields: any;
  teacherListForFilter: any;
  classListForFilter: any;
  boardListForFilter: any;
  subjectListForFilter: any;
  notesTitle: any;
  allCourses: any;
  pageNo: number;
  //todayDate: any;
  //currentTime: any;
  @Input() preferenceData: any;
  @Input() cc: any;
  @Input() teacherDetails: any;
  @Input() teacherList: any;
  @Input() isDelete: any;
  @Input() courseList: any;
  @Input() studentDetails: any;
  @Output() isStudentEnrolled: any = new EventEmitter();
  @Output() isPaymentUpdated: any = new EventEmitter();
  @Output() isNewCourseUpdated: any = new EventEmitter();
  @Output() deletedCourseId: any = new EventEmitter();
  @Output() isCourseUpdated: any = new EventEmitter();
  @Input() teacherStudentCount: any;

  constructor(public comservice: CommonServiceService, public changedetection: ChangeDetectorRef, public lpservice: LearningpodService, public course: Course) {
  }
  unenrolledInput: string;
  ngOnInit() {
    this.loading = true;
    // this.courseDetailsById = null;
    this.currentCourseId = null;
    this.pageNo = 1;
    this.comservice.showNavbar = true;
    this.isPaymentUpdatedVal = false;
    this.courseDetails = [];
    //this.editParticularFields = false;
    this.courseToAdd = new Course();
    this.courseToUpdate = new Course();
    this.showavailableonPhonemsg = false;
    this.showcolor = false;
    this.editDiscountVariable = false;
    this.showFeeForEnrolled = false;
    this.noStudentsAdded = false;
    this.getallcourses();
    this.disableStartDate = true;
    this.dropdownSettingsForFrequency = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

  }



  removeDuplicates(array, key) {
    return array.reduce((accumulator, element) => {
      if (!accumulator.find(el => el[key] === element[key])) {
        accumulator.push(element);
      }
      return accumulator;
    }, []);
  }

  getallcourses() {
    this.loading = true;

    this.lpservice.getMyCourses(this.comservice.myUserRole, this.comservice.myUserId).subscribe(res => {
      this.loading = false;
      this.allCourses = res;
      // console.log("after get", res)
      if (this.allCourses != undefined) {
        let clearData = this.allCourses.filter(x => x.teacherInfo != undefined);
        clearData = this.allCourses.map(x => x.teacherInfo);
        this.teacherListForFilter = clearData.filter(x => x != null);
        this.teacherListForFilter = this.removeDuplicates(this.teacherListForFilter, 'teacherName');
        let clearData1 = this.allCourses.map(x => x.classInfo);
        this.classListForFilter = clearData1.filter(x => x != null);
        this.classListForFilter = this.removeDuplicates(this.classListForFilter, 'className');
        let clearData3 = this.allCourses.map(x => x.boardInfo);
        this.boardListForFilter = clearData3.filter(x => x != null);
        this.boardListForFilter = this.removeDuplicates(this.boardListForFilter, 'boardName');
        let clearData2 = this.allCourses.map(x => x.subjectInfo);
        this.subjectListForFilter = clearData2.filter(x => x != null);
        this.subjectListForFilter = this.removeDuplicates(this.subjectListForFilter, 'subjectName');
        this.courseEvent();
        this.changedetection.detectChanges();
        this.loading = false;


        //   if(a < "7:51 PM"){
        // console.log("true;========================")
        //   }
      }
    });
  }


  courseEvent() {
    this.isNewCourseUpdated.emit(this.allCourses);

  }

  //to open file modal


  enrolledStudent: any;
  unEnrolledStudent: any;

  //to get course details by course id
  getCourseDetailsByCourseId(courseId) {
    // c
    this.loading = true;
    this.courseId = courseId;
    this.lpservice.exploreCourseDetails(courseId).subscribe(res => {
      this.loading = false;
      this.enrolledStudent = res.courseDetails.studentInfo;
      let enrolledStudentIds = this.enrolledStudent.map(x => x.studentId);
      if (this.studentDetails != undefined) {
        this.unEnrolledStudent = this.studentDetails.filter(val => !enrolledStudentIds.includes(val.studentId));
      }

      this.courseDetails = res.courseDetails.details;
      if (res.courseDetails.details.startTime != undefined) {
        this.course.startTime = res.courseDetails.details.startTime;
        let myNewStartTime = this.comservice.convert12To24HourFormat(res.courseDetails.details.startTime);
        this.course.startTimeToUpdate = new Date(res.courseDetails.details.startDate + "T" + myNewStartTime)
      }
      if (res.courseDetails.details.endTime != undefined) {
        this.course.endTime = res.courseDetails.details.endTime;
        let myNewEndTime = this.comservice.convert12To24HourFormat(res.courseDetails.details.endTime);
        this.course.endTimeToUpdate = new Date(res.courseDetails.details.endDate + "T" + myNewEndTime)
      }
      this.loading = false;
      this.course.courseId = res.courseDetails.details.courseId;
      this.course.title = res.courseDetails.details.title;
      this.course.toBeScheduled = res.courseDetails.details.toBeScheduled;
      if (res.courseDetails.details.toBeScheduled == "true") {

        this.disableStartDate = false;
      }
      if (res.courseDetails.details.subject && res.courseDetails.details.subject.subjectId != undefined) {
        this.course.subjectId = res.courseDetails.details.subject.subjectId;
        this.course.subjectName = res.courseDetails.details.subject.subjectName;
      }


      this.course.cost = res.courseDetails.details.cost;
      if (res.courseDetails.details.classInfo) {
        this.course.classId = res.courseDetails.details.classInfo.classId;
        this.course.className = res.courseDetails.details.classInfo.className;
      }
      this.course.frequency = res.courseDetails.details.frequency;
      if (this.course.frequency != undefined) {
        this.courseFrequencyArr = this.course.frequency.split(",");
      }

      this.course.cumulativeRating = res.courseDetails.details.cumulativeRating;
      this.course.maxStudent = res.courseDetails.details.maxStudent;
      this.course.videoPath = res.courseDetails.details.videoPath;
      this.course.doubtClearance = res.courseDetails.details.doubtClearance == 'true' ? true : false;
      this.course.onlineForum = res.courseDetails.details.onlineForum == 'true' ? true : false;
      this.course.studyMaterial = res.courseDetails.details.studyMaterial == 'true' ? true : false;
      this.course.periodicTests = res.courseDetails.details.periodicTests == 'true' ? true : false;
      this.course.availableOnPhone = res.courseDetails.details.availableOnPhone == 'true' ? true : false;
      this.course.homework = res.courseDetails.details.homework == 'true' ? true : false;
      if (res.courseDetails.details.startDate != undefined) {
        this.course.startDate = new Date(res.courseDetails.details.startDate); // Date to yyyy/mm/dd format
      }
      if (res.courseDetails.details.endDate != undefined) {
        this.course.endDate = new Date(res.courseDetails.details.endDate);
      }

      var a = new Date();
      var b = this.comservice.convert12To24HourFormat(new Date().toLocaleTimeString());
      var c = this.comservice.convert12To24HourFormat(res.courseDetails.details.startTime);
      if (a < this.course.startDate) {
        this.disableStartDate = false;
      }
      else {
        if (a == this.course.startDate && b < c) {
          this.disableStartDate = false;
        }
        else {
          this.disableStartDate = true;
        }
      }

      // if (new Date() < new Date(res.courseDetails.details.endDate) ||
      //   (new Date()).toDateString() == (new Date(res.courseDetails.details.endDate)).toDateString() ||
      //   res.courseDetails.details.endDate == undefined || res.courseDetails.details.startDate == undefined) {
      //   this.disableStartDate = false;
      // }
      // else {
      //   this.disableStartDate = true;
      // }

      // if (new Date() > new Date(res.courseDetails.details.startDate) || (new Date()).toDateString() == (new Date(res.courseDetails.details.startDate)).toDateString()) {
      //   this.disableStartDate = true;
      // }


      this.course.description = res.courseDetails.details.description;
      this.course.createdByRole = res.courseDetails.details.createdByRole;
      if (res.courseDetails.details.teacher) {
        this.course.teacherName = res.courseDetails.details.teacher.teacherName;
        this.course.teacherId = res.courseDetails.details.teacher.teacherID;
      }
      if (res.courseDetails.details.cc != null || res.courseDetails.details.cc != undefined) {
        if (res.courseDetails.details.cc.ccId != null || res.courseDetails.details.cc.ccId != undefined) {
          this.course.ccName = res.courseDetails.details.cc.ccName;
          this.course.ccId = res.courseDetails.details.cc.ccId;
        }
      }
      this.courseNotes = res.courseDetails.courseNotes;
      this.teacherData = res.courseDetails.details.teacher;
      this.reviewData = res.courseDetails.reviews.studentReviews;
      if (this.reviewData.length != 0) {
        this.showReview = true
      } else {
        this.showReview = false
      }

      if (res.courseDetails.details.boardInfo) {
        this.course.boardName = res.courseDetails.details.boardInfo.boardName;
        this.course.boardId = res.courseDetails.details.boardInfo.boardId;
      }

      this.loading = false;
    })

  }
  //courseDetailsById: any;

  currentCourseId: any;

  // to display the data on view details button click
  opentabDetails(data) {
    this.getCourseDetailsByCourseId(data.courseId);
    this.currentCourseId = data.courseId;
    // this.courseDetailsById = data;
    this.disableStartDate = false;

  }

  //to clear course details modal on close button click
  clearCourseDetailsModal() {
    this.course.courseId = null;
    this.course.title = null;
    this.course.toBeScheduled = null;
    this.course.subjectId = undefined;
    this.course.subjectName = null;
    this.course.cost = null;
    this.course.classId = undefined;
    this.course.className = null;
    this.course.boardName = null;
    this.course.frequency = null;
    this.course.cumulativeRating = null;
    this.course.startTime = undefined;
    this.course.endTime = undefined;
    this.course.startTimeToUpdate = undefined;
    this.course.endTimeToUpdate = undefined;
    this.course.maxStudent = null;
    this.course.videoPath = null;
    this.course.startDate = undefined;
    this.course.endDate = undefined;
    this.course.description = null;
    this.course.createdByRole = null;
    this.course.teacherName = null;
    this.course.teacherId = undefined;
    this.course.ccName = null;
    this.course.ccId = null;
    this.notesTitle = null;
    this.courseFrequencyArr = null;
    this.course.boardId = undefined;

    // if (this.reviewData.length != 0) {

    //   document.getElementById('courseReviewTab').classList.remove('active');
    //   document.getElementById('courseReviewLink').classList.remove('active');
    //   document.getElementById('courseReviewLink1').classList.remove('active');
    // }
  }

  //to update course details
  //loadingUpdateCourse: any;
  updateCourseTitleError: boolean = false;
  updateCourseBoardError: boolean = false;
  updateCourseSubjectError: boolean = false;
  updateCourseFrequencyError: boolean = false;
  updateCourseClassError: boolean = false;
  updateCourseTeacherError: boolean = false;
  endTimeLessThenStartTimeError: boolean = false;
  enterValidDate: boolean = false;

  validateUpdateCourseDetails() {
    if (this.courseToUpdate.title == "") {
      this.updateCourseTitleError = true;
      return false;
    } else {
      this.updateCourseTitleError = false;
    }
    if (this.courseToUpdate.teacherId == null) {
      this.updateCourseTeacherError = true;
      return false;
    } else {
      this.updateCourseTeacherError = false;
    }

    if (this.courseToUpdate.subjectId == null) {
      this.updateCourseSubjectError = true;
      return false;
    } else {
      this.updateCourseSubjectError = false;
    }
    if (this.courseFrequencyArr.length == 0) {
      this.updateCourseFrequencyError = true;
      return false;
    } else {
      this.updateCourseFrequencyError = false;
    }


    return true;
  }

  Updatecoursedetails(courseId) {

    this.loading = true;
    this.enterValidDate = false;
    //this.loadingUpdateCourse = true;
    this.disableCourseDetailsEdit();
    if (this.course.startDate != undefined) {
      this.courseToUpdate.startDate = new Date(this.course.startDate).toLocaleDateString("sv-SE");
      this.courseToUpdate.endDate = new Date(this.course.endDate).toLocaleDateString("sv-SE");
    }
    if (this.comservice.myUserRole == 'cc') {
      this.courseToUpdate.teacherId = + this.course.teacherId;
      this.courseToUpdate.ccId = this.comservice.myUserId;
      var d = this.teacherDetails.filter(x => x.teacherInfo.teacherId == this.course.teacherId);
      this.course.teacherName = d[0].teacherInfo.teacherName;
    }
    if (this.comservice.myUserRole == 'teacher') {
      this.courseToUpdate.ccId = + this.comservice.ccId;
      this.courseToUpdate.teacherId = this.comservice.myUserId;
    }
    const sorter = {
      "monday": 1,
      "tuesday": 2,
      "wednesday": 3,
      "thursday": 4,
      "friday": 5,
      "saturday": 6,
      "sunday": 7
    }
    this.courseFrequencyArr.sort(function sortByDay(a, b) {
      let day1 = a.toLowerCase();
      let day2 = b.toLowerCase();
      return sorter[day1] - sorter[day2];
    });
    this.frequencyInString = this.courseFrequencyArr.toString();
    this.courseToUpdate.courseId = courseId;
    this.courseToUpdate.title = this.course.title;
    this.courseToUpdate.cumulativeRating = this.course.cumulativeRating;
    this.courseToUpdate.description = this.course.description;

    var options = { hour: 'numeric', minute: '2-digit' };
    if (this.course.startTimeToUpdate instanceof Date) {
      this.courseToUpdate.startTime = new Date(this.course.startTimeToUpdate).toLocaleTimeString('en-US', options);
    }
    else {
      this.courseToUpdate.startTime = this.course.startTime;
    }
    if (this.course.endTimeToUpdate instanceof Date) {
      this.courseToUpdate.endTime = new Date(this.course.endTimeToUpdate).toLocaleTimeString('en-US', options);
    }
    else {
      this.courseToUpdate.endTime = this.course.endTime;
    }
    if (this.course.classId == 0 || this.course.classId == undefined) {
      this.courseToUpdate.classId = null;
    } else {
      this.courseToUpdate.classId = + this.course.classId;
      var a = this.comservice.cbsObj.allClasses.filter(x => x.classId == this.course.classId);
      this.course.className = a[0].className;
    }
    if (this.course.subjectId == 0 || this.course.subjectId == undefined) {
      this.courseToUpdate.subjectId = null;
    } else {
      this.courseToUpdate.subjectId = + this.course.subjectId;
      var b = this.comservice.cbsObj.allSubjects.filter(x => x.subjectId == this.course.subjectId);
      this.course.subjectName = b[0].subjectName;
    }

    this.courseToUpdate.maxStudent = parseInt(this.course.maxStudent);
    this.courseToUpdate.cost = + this.course.cost;
    this.courseToUpdate.doubtClearance = this.course.doubtClearance == (true) ? true : false;
    this.courseToUpdate.onlineForum = this.course.onlineForum == (true) ? true : false;
    this.courseToUpdate.studyMaterial = this.course.studyMaterial == (true) ? true : false;
    this.courseToUpdate.periodicTests = this.course.periodicTests == (true) ? true : false;
    this.courseToUpdate.availableOnPhone = this.course.availableOnPhone == (true) ? true : false;
    if (this.course.boardId == 0 || this.course.boardId == undefined) {
      this.courseToUpdate.boardId = null;
    } else {
      this.courseToUpdate.boardId = + this.course.boardId;
      var c = this.comservice.cbsObj.allBoards.filter(x => x.boardId == this.course.boardId);
      this.course.boardName = c[0].boardName;
    }
    this.courseToUpdate.homework = this.course.homework == (true) ? true : false;

    if (!this.validateUpdateCourseDetails()) {
      this.isDisabled = false;
      this.loading = false;
      return;
    }
    if (this.course.startDate == null || this.course.startDate == undefined || this.course.endDate == null ||
      this.course.endDate == undefined || this.course.startTime == null || this.course.startTime == undefined ||
      this.course.endTime == null || this.course.endTime == undefined) {
      this.enterValidDate = true;
      (document.getElementById("courseUpdateStartTime") as HTMLInputElement).style.borderColor = "red";
      (document.getElementById("courseUpdateEndTime") as HTMLInputElement).style.borderColor = "red";
      (document.getElementById("courseUpdateStartDate") as HTMLInputElement).style.borderColor = "red";
      (document.getElementById("courseUpdateEndDate") as HTMLInputElement).style.borderColor = "red";

      this.loading = false;
      return;
    }
    else {
      (document.getElementById('courseUpdateStartTime') as HTMLInputElement).style.borderColor = "lightgrey";
      (document.getElementById('courseUpdateEndTime') as HTMLInputElement).style.borderColor = "lightgrey";
      (document.getElementById('courseUpdateStartDate') as HTMLInputElement).style.borderColor = "lightgrey";
      (document.getElementById('courseUpdateEndDate') as HTMLInputElement).style.borderColor = "lightgrey";
    }
    if (this.courseToUpdate.endDate < this.courseToUpdate.startDate) {
      this.endDateLessThenStartDateError = true;
      this.enterValidDate = false;
      (document.getElementById("courseUpdateStartDate") as HTMLInputElement).style.borderColor = "red";
      (document.getElementById("courseUpdateEndDate") as HTMLInputElement).style.borderColor = "red";
      this.loading = false;
      return;
    }
    else {
      (document.getElementById("courseUpdateStartDate") as HTMLInputElement).style.borderColor = "lightgrey";
      (document.getElementById("courseUpdateEndDate") as HTMLInputElement).style.borderColor = "lightgrey";
    }
    var end = this.comservice.convert12To24HourFormat(this.courseToUpdate.endTime);
    var strt = this.comservice.convert12To24HourFormat(this.courseToUpdate.startTime);
    if (end < strt) {
      this.endTimeLessThenStartTimeError = true;
      (document.getElementById("courseUpdateStartTime") as HTMLInputElement).style.borderColor = "red";
      (document.getElementById("courseUpdateEndTime") as HTMLInputElement).style.borderColor = "red";
      this.loading = false;
      return;
    }
    else {
      (document.getElementById("courseUpdateStartTime") as HTMLInputElement).style.borderColor = "lightgrey";
      (document.getElementById("courseUpdateEndTime") as HTMLInputElement).style.borderColor = "lightgrey";
    }

    if (this.course.toBeScheduled == "false") {
      this.lpservice.updateCourse(this.courseToUpdate, this.frequencyInString).subscribe(res => {
        //   this.loadingUpdateCourse = false;
        if (res.status == "success") {
          this.loading = false;
          this.courseUpdated = true
          this.isCourseUpdated.emit(this.courseUpdated);
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: `Course updated successfully!`,
            timer: 2000
          })

          //var mydata = this.allCourses;
          var classInfo = this.comservice.cbsObj.allClasses.filter(x => x.classId == this.courseToUpdate.classId);
          var subjectInfo = this.comservice.cbsObj.allSubjects.filter(x => x.subjectId == this.courseToUpdate.subjectId);
          var boardInfo = this.comservice.cbsObj.allBoards.filter(x => x.boardId == this.courseToUpdate.boardId);
          var teacherInfo = this.teacherDetails.filter(x => x.teacherInfo.teacherId == this.courseToUpdate.teacherId);
          var classId, className, boardId, boardName;
          if (classInfo.length == 0) {
            classId = null;
            className = null;
          }
          else {
            classId = classInfo[0].classId;
            className = classInfo[0].className;
          }
          if (boardInfo.length == 0) {
            boardId = null;
            boardName = null;
          }
          else {
            boardId = classInfo[0].classId;
            boardName = classInfo[0].className;
          }
          var updatedCourseJson = {
            "boardInfo": { "boardId": boardId, "boardName": boardName },
            "ccInfo": { "ccId": this.courseToUpdate.ccId, "ccName": this.course.ccName }, "classInfo": {
              "classId": classId, "className": className }, "subjectInfo": { "subjectId": subjectInfo[0].subjectId, "subjectName": subjectInfo[0].subjectName },
            "teacherInfo": { "teacherId": teacherInfo[0].teacherInfo.teacherId, "teacherName": teacherInfo[0].teacherInfo.teacherName }, "toBeScheduled": this.course.toBeScheduled,
            "frequency": this.frequencyInString, "cost": this.courseToUpdate.cost, "courseId": this.courseToUpdate.courseId,
            "cumulativeRating": this.courseToUpdate.cumulativeRating, "description": this.courseToUpdate.description,
            "endDate": this.courseToUpdate.endDate, "endTime": this.courseToUpdate.endTime, "startDate": this.courseToUpdate.startDate,
            "startTime": this.courseToUpdate.startTime, "title": this.courseToUpdate.title
          };

          var index = this.allCourses.findIndex(x => x.courseId === this.courseToUpdate.courseId);
          this.allCourses[index] = updatedCourseJson;

          if (this.allCourses != undefined) {
            let clearData = this.allCourses.filter(x => x.teacherInfo != undefined);
            clearData = this.allCourses.map(x => x.teacherInfo);
            this.teacherListForFilter = clearData.filter(x => x != null);
            this.teacherListForFilter = this.removeDuplicates(this.teacherListForFilter, 'teacherName');
            let clearData1 = this.allCourses.map(x => x.classInfo);
            this.classListForFilter = clearData1.filter(x => x != null);
            this.classListForFilter = this.removeDuplicates(this.classListForFilter, 'className');
            let clearData3 = this.allCourses.map(x => x.boardInfo);
            this.boardListForFilter = clearData3.filter(x => x != null);
            this.boardListForFilter = this.removeDuplicates(this.boardListForFilter, 'boardName');
            let clearData2 = this.allCourses.map(x => x.subjectInfo);
            this.subjectListForFilter = clearData2.filter(x => x != null);
            this.subjectListForFilter = this.removeDuplicates(this.subjectListForFilter, 'subjectName');
            this.courseEvent();
            this.changedetection.detectChanges();
            this.loading = false;
          }
        }
        else if (res.status == "error") {
          this.loading = false;
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: `${res.message}`,
            timer: 2000
          })
        }
      });
    }
    else {

      this.lpservice.updateManualCourse(this.courseToUpdate, this.frequencyInString).subscribe(res => {
        //  this.loadingUpdateCourse = false;
        if (res.status == "success") {
          this.loading = false;
          this.courseUpdated = true
          this.isCourseUpdated.emit(this.courseUpdated);
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: `Course updated successfully!`,
            timer: 2000
          })

          // this.getCourseDetailsByCourseId(courseId)
          // this.getallcourses();
          var classInfo = this.comservice.cbsObj.allClasses.filter(x => x.classId == this.courseToUpdate.classId);
          var subjectInfo = this.comservice.cbsObj.allSubjects.filter(x => x.subjectId == this.courseToUpdate.subjectId);
          var boardInfo = this.comservice.cbsObj.allBoards.filter(x => x.boardId == this.courseToUpdate.boardId);
          var teacherInfo = this.teacherDetails.filter(x => x.teacherInfo.teacherId == this.courseToUpdate.teacherId);

          var updatedCourseJson = {
            "boardInfo": { "boardId": boardInfo[0].boardId, "boardName": boardInfo[0].boardName },
            "ccInfo": { "ccId": this.courseToUpdate.ccId, "ccName": this.course.ccName }, "classInfo": {
              "classId": classInfo[0].classId,
              "className": classInfo[0].className
            }, "subjectInfo": { "subjectId": subjectInfo[0].subjectId, "subjectName": subjectInfo[0].subjectName },
            "teacherInfo": { "teacherId": teacherInfo[0].teacherInfo.teacherId, "teacherName": teacherInfo[0].teacherInfo.teacherName },
            "toBeScheduled": this.course.toBeScheduled, "frequency": this.frequencyInString, "cost": this.courseToUpdate.cost, "courseId": this.courseToUpdate.courseId,
            "cumulativeRating": this.courseToUpdate.cumulativeRating, "description": this.courseToUpdate.description,
            "endDate": this.courseToUpdate.endDate, "endTime": this.courseToUpdate.endTime, "startDate": this.courseToUpdate.startDate,
            "startTime": this.courseToUpdate.startTime, "title": this.courseToUpdate.title
          };

          var index = this.allCourses.findIndex(x => x.courseId === this.courseToUpdate.courseId);
          this.allCourses[index] = updatedCourseJson;

          if (this.allCourses != undefined) {
            let clearData = this.allCourses.filter(x => x.teacherInfo != undefined);
            clearData = this.allCourses.map(x => x.teacherInfo);
            this.teacherListForFilter = clearData.filter(x => x != null);
            this.teacherListForFilter = this.removeDuplicates(this.teacherListForFilter, 'teacherName');
            let clearData1 = this.allCourses.map(x => x.classInfo);
            this.classListForFilter = clearData1.filter(x => x != null);
            this.classListForFilter = this.removeDuplicates(this.classListForFilter, 'className');
            let clearData3 = this.allCourses.map(x => x.boardInfo);
            this.boardListForFilter = clearData3.filter(x => x != null);
            this.boardListForFilter = this.removeDuplicates(this.boardListForFilter, 'boardName');
            let clearData2 = this.allCourses.map(x => x.subjectInfo);
            this.subjectListForFilter = clearData2.filter(x => x != null);
            this.subjectListForFilter = this.removeDuplicates(this.subjectListForFilter, 'subjectName');
            this.courseEvent();
            this.changedetection.detectChanges();
            this.loading = false;
          }
        }
        else if (res.status == "error") {
          this.loading = false;
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: `${res.message}`,
            timer: 2000
          })
        }
      });
    }
    //this.loadingUpdateCourse = false;
  }
  disabledButton: boolean;
  //to create course

  endDateLessThenStartDateError: boolean = false;
  addcourse(formdata) {
    this.loading = true;
    // $('#addCourseBtn').prop('disabled', true);
    if (!formdata) {
      //  $('#addCourseBtn').prop('disabled', false);
      this.loading = false;
      return false;
    }
    if (this.courseToAdd.startDate == null || this.courseToAdd.startDate == undefined || this.courseToAdd.endDate == null ||
      this.courseToAdd.endDate == undefined || this.courseToAdd.startTime == null || this.courseToAdd.startTime == undefined ||
      this.courseToAdd.endTime == null || this.courseToAdd.endTime == undefined) {
      this.enterValidDate = true;
      (document.getElementById("createCourseStartDate") as HTMLInputElement).style.borderColor = "red";
      (document.getElementById("createCourseEndDate") as HTMLInputElement).style.borderColor = "red";
      (document.getElementById("createCourseStartTime") as HTMLInputElement).style.borderColor = "red";
      (document.getElementById("createCourseEndTime") as HTMLInputElement).style.borderColor = "red";

      this.loading = false;
      return false;
    }

    this.courseToAdd.startDate = new Date(this.courseToAdd.startDate).toLocaleDateString("sv-SE"); // Date to yyyy/mm/dd format
    this.courseToAdd.endDate = new Date(this.courseToAdd.endDate).toLocaleDateString("sv-SE");

    if (this.courseToAdd.endDate < this.courseToAdd.startDate) {
      this.endDateLessThenStartDateError = true;
      this.enterValidDate = false;
      (document.getElementById("createCourseStartDate") as HTMLInputElement).style.borderColor = "red";
      (document.getElementById("createCourseEndDate") as HTMLInputElement).style.borderColor = "red";
      this.loading = false;
      return false;
    }

    if (this.courseToAdd.endTime < this.courseToAdd.startTime) {
      this.endTimeLessThenStartTimeError = true;
      (document.getElementById("createCourseStartTime") as HTMLInputElement).style.borderColor = "red";
      (document.getElementById("createCourseEndTime") as HTMLInputElement).style.borderColor = "red";
      this.loading = false;
      return false;
    }
    var options = { hour: 'numeric', minute: '2-digit' };
    if (this.courseToAdd.startTime instanceof Date) {
      this.courseToAdd.startTime = new Date(this.courseToAdd.startTime).toLocaleTimeString('en-US', options);
    }
    if (this.courseToAdd.endTime instanceof Date) {
      this.courseToAdd.endTime = new Date(this.courseToAdd.endTime).toLocaleTimeString('en-US', options);
    }

    this.disabledButton = true;
    if (this.comservice.myUserRole == 'cc') {
      this.courseToAdd.teacherId = + this.courseToAdd.teacherId;
      this.courseToAdd.ccId = this.comservice.myUserId;
    }
    if (this.comservice.myUserRole == 'teacher') {
      this.courseToAdd.ccId = + this.comservice.ccId;
      this.courseToAdd.teacherId = this.comservice.myUserId;
    }

    const sorter = {
      "monday": 1,
      "tuesday": 2,
      "wednesday": 3,
      "thursday": 4,
      "friday": 5,
      "saturday": 6,
      "sunday": 7
    }
    if (this.courseToAdd.frequencySelected) {
      this.courseToAdd.frequencySelected.sort(function sortByDay(a, b) {
        let day1 = a.toLowerCase();
        let day2 = b.toLowerCase();
        return sorter[day1] - sorter[day2];
      });
    }

    this.courseToAdd.frequency = this.courseToAdd.frequencySelected.join(",");
    this.courseToAdd.subjectId = + this.courseToAdd.subjectId;
    if (this.courseToAdd.classId == undefined) {
      this.courseToAdd.classId = null;
    }
    else {
      this.courseToAdd.classId = + this.courseToAdd.classId;
    }
    if (this.courseToAdd.boardId == undefined) {
      this.courseToAdd.boardId = null;
    }
    else {
      this.courseToAdd.boardId = + this.courseToAdd.boardId;
    }


    this.courseToAdd.maxStudent = this.courseToAdd.maxStudent;
    if (this.courseToAdd.cost == undefined) {
      this.courseToAdd.cost = 0;
    }
    else {
      this.courseToAdd.cost = + this.courseToAdd.cost;
    }

    this.courseToAdd.doubtClearance = this.courseToAdd.doubtClearance == undefined ? false : this.courseToAdd.doubtClearance;
    this.courseToAdd.onlineForum = this.courseToAdd.onlineForum == undefined ? false : this.courseToAdd.onlineForum;
    this.courseToAdd.studyMaterial = this.courseToAdd.studyMaterial == undefined ? false : this.courseToAdd.studyMaterial;
    this.courseToAdd.periodicTests = this.courseToAdd.periodicTests == undefined ? false : this.courseToAdd.periodicTests;
    this.courseToAdd.availableOnPhone = this.courseToAdd.availableOnPhone == undefined ? false : this.courseToAdd.availableOnPhone;
    this.courseToAdd.homework = this.courseToAdd.homework == undefined ? false : this.courseToAdd.homework;

    //var coure = this.courseToAdd;
    if (this.courseToAdd.scheduleLaterFlag == false || this.courseToAdd.scheduleLaterFlag == undefined) {
      this.lpservice.addCourseNew(this.courseToAdd).subscribe(res => {
        this.loading = false;
        //var addCourseJson ={ "boardInfo" : this.courseToAdd.boardId, "" }
        //  console.log("after add", this.courseToAdd.startDate)
        //this.courseId = res;
        this.disabledButton = false;
        if (res.status == "success") {
          //  $('#addCourseBtn').prop('disabled', false);
          this.courseUpdated = true
          this.isCourseUpdated.emit(this.courseUpdated);

          this.lpservice.getStudentList(res.courseId).subscribe((data) => {
            if (data.studentInformation.length == 0) {
              this.noStudentsAdded = true;
            }
          });

          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: "Success!",
            text: "Course created successfully!",
            timer: 3000
          });
          var modals = document.getElementById('addCourse');
          modals.setAttribute('data-dismiss', 'modal');
          var closeCreateCourseButton = document.getElementById("closeCreateCourse");
          closeCreateCourseButton.click();
          this.getallcourses();
        }
        else {
          // $('#addCourseBtn').prop('disabled', false);
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: `${res.message}`,
            timer: 2000
          })
        }
      }, err => {
        // $('#addCourseBtn').prop('disabled', false);
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: `${err}`,
          timer: 2000
        })
        this.disabledButton = false;
      })
    }
    else {
      this.lpservice.scheduleLaterCourse(this.courseToAdd).subscribe(res => {
        this.courseId = res;
        this.disabledButton = false;
        if (res.status == "success") {
          //  $('#addCourseBtn').prop('disabled', false);
          this.courseUpdated = true
          this.isCourseUpdated.emit(this.courseUpdated);

          this.lpservice.getStudentList(res.courseId).subscribe((data) => {
            if (data.studentInformation.length == 0) {
              this.noStudentsAdded = true;
            }
          });
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: "Success!",
            text: "Course created successfully!",
            timer: 3000
          });
          var modals = document.getElementById('addCourse');
          modals.setAttribute('data-dismiss', 'modal');
          var closeCreateCourseButton = document.getElementById("closeCreateCourse");
          closeCreateCourseButton.click();
          this.getallcourses();
        }
        else {
          //  $('#addCourseBtn').prop('disabled', false);
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: `${res.message}`,
            timer: 2000
          })
        }
      }, err => {
        //  $('#addCourseBtn').prop('disabled', false);
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: `${err}`,
          timer: 2000
        })
        this.disabledButton = false;
      })
    }

  }
  removeDateValidation() {
    if (this.courseToAdd.endDate instanceof Date) {
      this.endDateLessThenStartDateError = false;
      (document.getElementById("createCourseEndDate") as HTMLInputElement).style.borderColor = "lightgrey";
    }
    if (this.courseToAdd.startDate instanceof Date) {
      (document.getElementById("createCourseStartDate") as HTMLInputElement).style.borderColor = "lightgrey";
      this.endDateLessThenStartDateError = false;
    }
    if (this.course.startDate instanceof Date) {
      this.endDateLessThenStartDateError = false;
      (document.getElementById("courseUpdateStartDate") as HTMLInputElement).style.borderColor = "lightgrey";
    }
    if (this.course.endDate instanceof Date) {
      (document.getElementById("courseUpdateEndDate") as HTMLInputElement).style.borderColor = "lightgrey";
      this.endDateLessThenStartDateError = false;
    }


  }

  removeTimeValidation() {
    if (this.courseToAdd.endTime instanceof Date) {
      this.endTimeLessThenStartTimeError = false;
      (document.getElementById("createCourseStartTime") as HTMLInputElement).style.borderColor = "lightgrey";
    }
    if (this.courseToAdd.startTime instanceof Date) {
      (document.getElementById("createCourseEndTime") as HTMLInputElement).style.borderColor = "lightgrey";
      this.endTimeLessThenStartTimeError = false;
    }

    if (this.course.startTimeToUpdate instanceof Date) {
      this.endTimeLessThenStartTimeError = false;
      (document.getElementById("courseUpdateStartTime") as HTMLInputElement).style.borderColor = "lightgrey";
    }
    if (this.course.endTimeToUpdate instanceof Date) {
      (document.getElementById("courseUpdateEndTime") as HTMLInputElement).style.borderColor = "lightgrey";
      this.endTimeLessThenStartTimeError = false;
    }
  }
  showcolorcode() {
    this.showcolor = true;
    for (var i = 0; i < this.subjectListForAddCourse.length; i++) {
      if (this.subjectListForAddCourse[i].subjectId == this.courseToAdd.subjectId) {
        (document.getElementById("square") as HTMLInputElement).style.backgroundColor = this.getcolor(this.subjectListForAddCourse[i].subjectName);
      }
    }
  }

  //to get colour code in creaete course
  getcolor(name) {
    switch (name) {
      case "Mathematics":
        return "#ff0000";
      case "Science":
        return "#ff5100";
      case "English":
        return "#ffa200";
      case "Hindi":
        return "#ffee00";
      case "Social Science":
        return "#b7ff00";
      case "Information Technology":
        return "#00ff73";
      case "Artificial Intelligence":
        return "#00ffc3";
      case "Custom":
        return "#0099ff";
      case "Physics":
        return "#0022ff";
      case "Chemistry":
        return "#8000ff";
      case "Biology":
        return "#c800ff";
      case "Language":
        return "#ff00f2";
      case "Marathi":
        return "#ff006a";
      case "SST":
        return "#730101";
      case "Computer":
        return "#647301";
      case "Geography":
        return "#01734d";
      case "ICT":
        return "#73013e";
      case "English-I":
        return "#a38b8b";
      case "English-II":
        return "#879191";
      case "History":
        return "#f09cc6";
      case "PED":
        return "#595055";
      case "Commerce":
        return "#d13636";
      case "Economics":
        return "#d17936";
      case "Science-I":
        return "#d1ba36";
      case "Science-II":
        return "#aad136";
      case "BS":
        return "#36d177";
      case "Home Science":
        return "#c936d1";
      case "Business Science":
        return "#7e3482";
      case "Pyschology":
        return "#823452";
      case "Painting":
        return "#823434";
      case "Data Entry":
        return "#826734";
      case "OCM":
        return "#518234";
      case "SP":
        return "#34826d";
      case "BM":
        return "#346282";
      case "Math-I":
        return "#212552";
      case "Math-II":
        return "#442152";
      case "Accountancy":
        return "#21523c";
      default:
        return "Black";
    }
  }
  ratingReview: any;
  //to show data in review tab
  openReviewDetails(data) {
    this.ratingReview = data;
  }

  // rating set event
  onRatingSet(event) {
    this.ratingToUpdateRating = event;
    this.teacherRatingToUpdateRating = event;
    this.ccRatingToUpdateRating = event;
  }


  //to add course review
  addCourseReview(ratingReview) {
    this.loading = true;
    this.lpservice.postCourseReview(this.ratingToUpdateRating, ratingReview.courseReview,
      ratingReview.courseId).subscribe(res => {
        this.loading = false;
        if (res.status == "success") {
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: 'Course review added successfully!',
            timer: 2000
          })
          this.getallcourses();
        }
      }, (err => {
      }));
  }

  //to add teacher review
  addTeacherReview(ratingReview) {
    this.loading = true;
    this.lpservice.postTeacherReview(this.teacherRatingToUpdateRating, ratingReview.teacherReview,
      ratingReview.teacherInfo.teacherId).subscribe(res => {
        this.loading = false;
        if (res.status == "success") {
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: 'Teacher review added successfully!',
            timer: 2000
          })
          this.getallcourses();
        }
        else {
        }
      }, (err => {
      }));
  }

  //to add cc review

  addCcReview(ratingReview) {
    this.loading = true;
    this.lpservice.postCcReview(this.ccRatingToUpdateRating, ratingReview.ccReview,
      ratingReview.ccInfo.ccId).subscribe(res => {
        this.loading = false;
        if (res.status == "success") {
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: 'Coaching class review added successfully!',
            timer: 2000
          })
          this.getallcourses();
        }
      }, (err => {
      }));
  }


  clearCreateCourse() {
    this.courseToAdd.title = "";
    this.courseToAdd.classId = undefined;
    this.courseToAdd.boardId = undefined;
    this.courseToAdd.subjectId = undefined;
    this.courseToAdd.teacherId = undefined;
    this.courseToAdd.frequencySelected = "";
    this.courseToAdd.startDate = "";
    this.courseToAdd.endDate = "";
    this.courseToAdd.startTime = "";
    this.courseToAdd.endTime = "";
    this.courseToAdd.ccId = undefined;
    this.courseToAdd.cost = "";
    this.courseToAdd.description = "";
    this.courseToAdd.maxStudent = "";
    this.courseToAdd.doubtClearance = "";
    this.courseToAdd.onlineForum = "";
    this.courseToAdd.homework = "";
    this.courseToAdd.periodicTests = "";
    this.courseToAdd.studyMaterial = "";
    this.courseToAdd.availableOnPhone = "";
    this.showcolor = false;
    (document.getElementById("square") as HTMLInputElement).style.backgroundColor = "white";
    //this.getClassBoardSubjectForCreateCourse();
  }

  // to reset create course form
  resetForm() {
    (document.getElementById("createCourseForm") as HTMLFormElement).reset();;
  }
  studentListForMultipleSelect: any;


  startDate: any;
  endDate: any;
  studentFeeId: any;
  studentFeeInfo: any;
  studentTransactionInfo: any;
  discountEnrolled: any;
  info: any;
  showFeeForEnrolled: any;
  courseTitle: any;
  cost: any;
  studentName: any;
  editDiscountVariable: any;
  payable: any;

  enrolledStudentsForPayment(x) {
    this.showFeeForEnrolled = false;
    this.courseTitle = x.title;
    this.cost = x.cost;
    this.startDate = x.startDate;
    this.endDate = x.endDate;
    this.courseId = x.courseId;
    this.lpservice.getStudentList(x.courseId).subscribe((data) => {
      this.studentList = data.studentInformation;
    });
  }

  pay(paymentStartDate, paymentEndDate, amountPaid, transactionDate) {
    this.isCoursePaymentDetailsLoading = true;
    this.lpservice.studentTransaction(paymentStartDate, paymentEndDate, amountPaid, this.studentFeeId, this.courseId, this.studentId, transactionDate).subscribe((data) => {
      if (data.status = "success") {
        this.isCoursePaymentDetailsLoading = false;
        this.isStudentEnrolledVal = true;
        this.isStudentEnrolled.emit(this.isStudentEnrolledVal)
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'success',
          title: 'Success!',
          text: `${data.message}`,
          timer: 2000
        })
      } else {
        this.isCoursePaymentDetailsLoading = false;
      }
    })
  }
  selectedItem: any;
  passCourseDetailsEnrolled(x) {
    this.selectedItem = x;
    this.studentId = x.studentId;
    this.studentName = x.studentName;
    this.lpservice.getFeeAndTransactionDetails(x.studentId, this.courseId).subscribe((data) => {
      this.studentFeeInfo = data.studentCourseFee;
      this.discountEnrolled = data.studentCourseFee.courseCost - data.studentCourseFee.payableAmount;
      this.payable = data.studentCourseFee.payableAmount;
      this.studentFeeId = data.studentCourseFee.stduentCourseFeeId
      this.studentTransactionInfo = data.studentCourseFeeTransaction;
      if (data.studentCourseFeeTransaction == "No transaction yet") {
        this.showFeeForEnrolled = true;
        this.studentTransactionInfo = []
      }
      else {
        this.showFeeForEnrolled = true;
      }
    }
    )
  }
  isCoursePaymentDetailsLoading: boolean
  editDiscount() {
    this.editDiscountVariable = true;
  }

  updateFees(courseCost, discountEnrolled, startDate, endDate, frequency) {
    this.isCoursePaymentDetailsLoading = true;
    this.payable = courseCost - discountEnrolled;
    this.editDiscountVariable = false;
    this.lpservice.updateFees(courseCost, this.payable, startDate, endDate, frequency, this.studentId, this.courseId, this.studentFeeId).subscribe((data) => {
      Swal.fire({
        backdrop: false,
        heightAuto: false,
        type: 'success',
        title: 'Success!',
        text: "Discount updated!",
        timer: 2000
      })
      this.isPaymentUpdatedVal = true;
      this.isCoursePaymentDetailsLoading = false;
      this.isPaymentUpdated.emit(this.isPaymentUpdatedVal)
      this.info = data;
    })
  }
  enableEdit = false;
  enableEditIndex = null;
  addEvent: any;
  enableEditMethod(i, event) {
    this.enableEdit = true;
    this.enableEditIndex = i;
    (document.getElementById("addNewRow") as HTMLInputElement).disabled = true;
    this.addEvent = event.target.id;
  }


  cancel() {

    if (this.addEvent == "editRow") {
      this.enableEditIndex = null;
      this.enableEdit = false;
      (document.getElementById("addNewRow") as HTMLInputElement).disabled = false;
    }
    else {
      this.enableEditIndex = null;
      this.enableEdit = false;
      (document.getElementById("addNewRow") as HTMLInputElement).disabled = false;
      this.studentTransactionInfo.pop();
    }

  }
  Modify(startDate, endDate, paidAmount, studentFeeTransactionId, transactionDate) {
    this.isCoursePaymentDetailsLoading = true;
    (document.getElementById("addNewRow") as HTMLInputElement).disabled = false;
    this.enableEditIndex = null;
    this.enableEdit = false;
    if (studentFeeTransactionId != undefined) {
      this.lpservice.modifyTransactionDetails(startDate, endDate, paidAmount, this.studentId, this.courseId, this.studentFeeId, studentFeeTransactionId, transactionDate).subscribe((data) => {
        this.isStudentEnrolledVal = true;
        this.isCoursePaymentDetailsLoading = false;
        this.isStudentEnrolled.emit(this.isStudentEnrolledVal);
        this.isPaymentUpdatedVal = true;
        this.isPaymentUpdated.emit(this.isPaymentUpdatedVal)
        this.info = data;
      })
    }
    else {
      if (startDate == undefined || endDate == undefined || paidAmount == undefined) {
        this.isCoursePaymentDetailsLoading = false;
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'error!',
          text: "Please fill all details",
          timer: 2000
        })
      }
      else {
        this.isCoursePaymentDetailsLoading = false;
        this.pay(startDate, endDate, paidAmount, transactionDate);
      }
    }
  }

  addRow() {
    this.addEvent = " ";
    if (this.studentTransactionInfo == "No transaction yet") {
      this.studentTransactionInfo = [{}];
      this.enableEditMethod(0, null);
    }
    else {
      this.studentTransactionInfo.push({});
      let i = this.studentTransactionInfo.length;
      this.enableEditMethod(i - 1, null);
    }

  }



  isUploadNotesPending: boolean;
  checkTitle() {
    this.isUploadNotesPending = true;
    if (this.notesTitle == undefined || this.notesTitle == "") {
      document.getElementById("notesTitle").style.borderColor = "red";
      this.isUploadNotesPending = false;
    }
    else {
      this.isUploadNotesPending = true;
      document.getElementById("notesTitle").style.borderColor = "lightgrey";
      document.getElementById("myNotes").click();

    }
  }

  isCourseNotesLoading: boolean;

  uploadNotes(event, courseId) {
    this.isUploadNotesPending = true;
    this.isCourseNotesLoading = true;
    this.loading = true;
    this.selectedfile = event.target.files[0];
    const newfile: File = this.selectedfile;
    this.lpservice.uploadNotes(newfile, courseId, this.notesTitle).subscribe((res) => {
      this.loading = false;
      this.isCourseNotesLoading = false;
      Swal.fire({
        backdrop: false,
        heightAuto: false,
        type: 'success',
        title: 'success!',
        text: res.message,
        timer: 2000
      })
      this.isUploadNotesPending = false;
      this.getCourseDetailsByCourseId(courseId);
      this.notesTitle = "";
    },
      (err) => {
        this.isUploadNotesPending = false;
        this.loading = false;
        this.isCourseNotesLoading = false;
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: `${err}`,
          timer: 2000
        })
      })

  }

  deleteNotes(courseNoteId, courseId) {
    this.isCourseNotesLoading = true;
    this.loading = true;
    this.lpservice.deleteNotes(courseNoteId).subscribe((res) => {
      this.loading = false;
      this.isCourseNotesLoading = false;
      Swal.fire({
        backdrop: false,
        heightAuto: false,
        type: 'success',
        title: 'Success!',
        text: `${res.message}`,
        timer: 2000
      })
      this.getCourseDetailsByCourseId(courseId);
    })
  }

  getValue = value => (typeof value === 'string' ? value.toUpperCase() : value);

  filterPlainArray(array, filters) {
    const filterKeys = Object.keys(filters);
    return array.filter(item => {
      // validates all filter criteria
      return filterKeys.every(key => {
        // ignores an empty filter
        if (!filters[key].length) return true;
        return filters[key].find(filter => this.getValue(filter) === this.getValue(item[key]));
      });
    });
  }


  clearSelection(a, b, c, d) {
    a = [];
    b = [];
    c = [];
    d = [];
  }

  getFilteredData() {
    //this.isFilteredDataAvailable = false;
    this.filteredCourseData = null;
    var classForFilter = [];
    var subjectForFilter = [];
    var teacherForFilter = [];
    var boardForFilter = [];
    var i = 0;
    var j = 0;
    var k = 0;
    var l = 0;

    $('.class_checkbox:checked').each(function () {
      classForFilter[i++] = $(this).val();
    });
    $('.subject_checkbox:checked').each(function () {
      subjectForFilter[j++] = $(this).val();
    });
    $('.teacher_checkbox:checked').each(function () {
      teacherForFilter[k++] = $(this).val();
    });
    $('.board_checkbox:checked').each(function () {
      boardForFilter[l++] = $(this).val();
    });
    (document.getElementById("collapseCourse") as HTMLInputElement).click();
    if (classForFilter.length == 0 && subjectForFilter.length == 0 && teacherForFilter.length == 0 && boardForFilter.length == 0) {
      this.filteredCourseData = null;
      document.getElementById('hideError').click();
    }

    //for filtering class
    if (Array.isArray(classForFilter) && classForFilter.length) {
      let clearData1 = this.allCourses.filter(x => x.classInfo != null);
      this.filteredCourseData = clearData1.filter(function (e) {
        return classForFilter.includes(e.classInfo.className)
      });
      if (Array.isArray(this.filteredCourseData) && this.filteredCourseData.length) {

        document.getElementById('hideError').click();
      } else {

        document.getElementById('showError').click();
      }

    }

    //for filtering subject
    if (Array.isArray(subjectForFilter) && subjectForFilter.length) {
      if (this.filteredCourseData != null) {
        let clearData1 = this.filteredCourseData.filter(x => x.subjectInfo != null)
        this.filteredCourseData = clearData1.filter(function (e) {
          return subjectForFilter.includes(e.subjectInfo.subjectName)
        });

        if (Array.isArray(this.filteredCourseData) && this.filteredCourseData.length) {
          document.getElementById('hideError').click();
        } else {
          document.getElementById('showError').click();
        }

      } else {
        let clearData1 = this.allCourses.filter(x => x.subjectInfo != null)
        this.filteredCourseData = clearData1.filter(function (e) {
          return subjectForFilter.includes(e.subjectInfo.subjectName)
        });
        if (Array.isArray(this.filteredCourseData) && this.filteredCourseData.length) {
          document.getElementById('hideError').click();
        } else {
          document.getElementById('showError').click();
        }
      }
    }


    //for filtering teacher
    if (Array.isArray(teacherForFilter) && teacherForFilter.length) {
      if (this.filteredCourseData != null) {
        let clearData1 = this.filteredCourseData.filter(x => x.teacherInfo != null)
        this.filteredCourseData = clearData1.filter(function (e) {
          return teacherForFilter.includes(e.teacherInfo.teacherName)
        });


        if (Array.isArray(this.filteredCourseData) && this.filteredCourseData.length) {
          document.getElementById('hideError').click();
        } else {
          document.getElementById('showError').click();
        }

      } else {
        let clearData1 = this.allCourses.filter(x => x.teacherInfo != null)
        this.filteredCourseData = clearData1.filter(function (e) {
          return teacherForFilter.includes(e.teacherInfo.teacherName)
        });
        if (Array.isArray(this.filteredCourseData) && this.filteredCourseData.length) {
          document.getElementById('hideError').click();
        } else {
          document.getElementById('showError').click();
        }
      }
    }

    //for filtering board
    if (Array.isArray(boardForFilter) && boardForFilter.length) {
      if (this.filteredCourseData != null) {
        let clearData1 = this.filteredCourseData.filter(x => x.boardInfo != null)
        this.filteredCourseData = clearData1.filter(function (e) {
          return boardForFilter.includes(e.boardInfo.boardName)
        });

        if (Array.isArray(this.filteredCourseData) && this.filteredCourseData.length) {
          document.getElementById('hideError').click();
        } else {
          document.getElementById('showError').click();
        }


      } else {
        let clearData1 = this.allCourses.filter(x => x.boardInfo != null)
        this.filteredCourseData = clearData1.filter(function (e) {
          return boardForFilter.includes(e.boardInfo.boardName)
        });
        if (Array.isArray(this.filteredCourseData) && this.filteredCourseData.length) {
          document.getElementById('hideError').click();
        } else {
          document.getElementById('showError').click();
        }
      }
    }
  }

  clearFilteredData() {
    //this.isFilteredDataAvailable = false;
    document.getElementById('hideError').click();
    this.filteredCourseData = null;
    if (this.comservice.myUserRole != 'student') {
      for (var a = 0; a < this.classListForFilter.length; a++) {
        (document.getElementById("className" + a) as HTMLInputElement).checked = false;
      }
      for (var d = 0; d < this.boardListForFilter.length; d++) {
        (document.getElementById("boardName" + d) as HTMLInputElement).checked = false;
      }
    }
    for (var b = 0; b < this.subjectListForFilter.length; b++) {
      (document.getElementById("subjectName" + b) as HTMLInputElement).checked = false;
    }
    if (this.comservice.myUserRole != 'teacher') {
      for (var c = 0; c < this.teacherListForFilter.length; c++) {
        (document.getElementById("teacherName" + c) as HTMLInputElement).checked = false;
      }
    }

  }


  isFilterErrorShow() {
    this.isFilteredDataAvailable = false;
  }

  isFilterErrorHide() {
    this.isFilteredDataAvailable = true;
  }

  disabledDates = (date: Date): boolean => {
    let todayDate = new Date();
    todayDate.setDate(todayDate.getDate() - 1)
    return date <= todayDate;
  }




  showNoResults: boolean;
  searchCourseByName() {
    this.filteredCourseData = [];
    this.showNoResults = false;
    var input = document.getElementById("searchCourse") as HTMLInputElement;
    var search = input.value.toUpperCase();
    for (var i = 0; i < this.allCourses.length; i++) {
      var a = this.allCourses[i].title;
      if (a.toUpperCase().indexOf(search) > -1) {
        this.showNoResults = false;
        this.filteredCourseData.push(this.allCourses[i]);
      }
    }
    if (this.filteredCourseData.length == 0) {
      this.showNoResults = true;
    }
  }


  backToMyCourses() {
    this.updateCourseTitleError = false;
    this.updateCourseBoardError = false;
    this.updateCourseSubjectError = false;
    this.updateCourseFrequencyError = false;
    this.updateCourseClassError = false;
    this.updateCourseTeacherError = false;
    this.unEnrollErrorMessage = "";
    this.enrollErrorMessage = "";
    this.isDisabled = true;
    // this.courseDetailsById = null;
    this.currentCourseId = null;
    this.endTimeLessThenStartTimeError = false;
    this.endDateLessThenStartDateError = false;

  }



  disableCourseDetailsEdit() {
    this.isDisabled = true;
  }


  enableCourseDetailsEdit() {
    this.isDisabled = false;
  }

  searchUnenrolled() {
    var valThis = this.unenrolledInput.toLowerCase();
    if (valThis == "") {
      $('#unenrolledUL > li').show();
    } else {
      $('#unenrolledUL > li').each(function () {
        var text = $(this).text().toLowerCase();
        (text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
      });
    };
  }




  enrolledInput: String;
  searchEnrolled() {
    var valThis = this.enrolledInput.toLowerCase();
    if (valThis == "") {
      $('#enrolledUL > li').show();
    } else {
      $('#enrolledUL > li').each(function () {
        var text = $(this).text().toLowerCase();
        (text.indexOf(valThis) >= 0) ? $(this).show() : $(this).hide();
      });
    };
  }

  isStudentEnrollLoading: boolean;
  isEnrollPending: boolean;

  enrollErrorMessage = "";

  enrollStudent() {
    this.isEnrollPending = true;
    this.loading = true;
    var selected = new Array();
    var ul = document.getElementById("unenrolledUL");
    var chks = ul.getElementsByTagName("INPUT");
    // Loop and push the checked CheckBox value in Array.
    for (var i = 0; i < chks.length; i++) {
      if ((chks[i] as HTMLInputElement).checked) {
        selected.push((chks[i] as HTMLInputElement).value);
      }
    }
    //Display the selected CheckBox values.
    if (selected.length > 0) {
      this.enrollErrorMessage = "";
      let studentstring = selected.join(",");
      this.lpservice.enrollStudentList(this.currentCourseId, studentstring).subscribe((data) => {
        if (data.status == "success") {
          this.isEnrollPending = false;
          this.loading = false;
          this.isStudentEnrolledVal = true;
          this.isStudentEnrolled.emit(this.isStudentEnrolledVal)
          this.studentId = undefined;
          this.getCourseDetailsByCourseId(this.currentCourseId);
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: 'Students enrolled successfully!',
            timer: 3000
          })
        } else {
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: data.message,
            timer: 3000
          })
          this.isEnrollPending = false;
          this.loading = false;
          this.isStudentEnrolledVal = false;
          this.isStudentEnrolled.emit(this.isStudentEnrolledVal)
        }
      });

    } else {
      this.loading = false;
      this.isEnrollPending = false;
      this.enrollErrorMessage = "*Please select students for enrollment";
    }
  };


  unEnrollErrorMessage = "";
  isUnenrollPending: boolean;
  unEnrollStudent() {
    this.isUnenrollPending = true;
    this.loading = true;
    //Create an Array.
    var selected = new Array();
    //Reference the Table.
    var ul = document.getElementById("enrolledUL");
    //Reference all the CheckBoxes in Table.
    var chks = ul.getElementsByTagName("INPUT");
    // Loop and push the checked CheckBox value in Array.
    for (var i = 0; i < chks.length; i++) {
      if ((chks[i] as HTMLInputElement).checked) {
        selected.push((chks[i] as HTMLInputElement).value);
      }
    }
    //Display the selected CheckBox values.
    if (selected.length > 0) {
      this.unEnrollErrorMessage = "";
      let studentstring = selected.join(",");
      this.lpservice.unEnrollStudentList(this.currentCourseId, studentstring).subscribe((data) => {
        if (data.status == "success") {
          this.isUnenrollPending = false;
          this.loading = false;
          this.isStudentEnrolledVal = true;
          this.isStudentEnrolled.emit(this.isStudentEnrolledVal)
          this.studentId = undefined;
          this.getCourseDetailsByCourseId(this.currentCourseId);
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: 'Students removed successfully!',
            timer: 3000
          })
        } else {
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: data.message,
            timer: 3000
          })
          this.isUnenrollPending = false;
          this.loading = false;
          this.isStudentEnrolledVal = false;
          this.isStudentEnrolled.emit(this.isStudentEnrolledVal)
        }
      });
    } else {
      this.loading = false;
      this.unEnrollErrorMessage = "*Please select students for unenroll"
      this.isUnenrollPending = false;
    }
  };



  enableEnroll() {
    this.isEnrollPending = false;
  }
  contentTotalPages: any;
  showLoadMore: boolean = true;
  loadMoreStudents() {
    this.loading = true;
    var contentTotalPages = Math.ceil(this.teacherStudentCount.studentCount / 20);

    this.lpservice.getAffiliatedStudent(this.comservice.myUserId, this.pageNo, 20).subscribe(res => {
      if (res.status = "success") {
        this.loading = false;
        for (var i = 0; i < res.studentInformation.length; i++) {
          this.studentDetails.push(res.studentInformation[i]);
        }
        this.getCourseDetailsByCourseId(this.courseId);
        this.pageNo++;
        var a = this.pageNo;
        if (a == contentTotalPages) {
          this.showLoadMore = false;
        }
        else {
          this.showLoadMore = true;
        }
      }
    });

  }

  deleteCourse(courseId) {
    this.loading = true;
    Swal.fire({
      backdrop: false,
      heightAuto: false,
      type: 'info',
      title: 'Are you sure you want to delete this course?',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete course'
    }).then((result) => {
      if (result.value) {
        this.lpservice.deleteCourse(courseId, true, this.enrollmentPermission).subscribe(data => {
          this.loading = false;
          if (data.status === "success") {
            //this.getallcourses();

            this.allCourses = this.allCourses.filter(x => x.courseId != courseId);
            this.deletedCourseId.emit();
            this.courseEvent()
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: data.message,
              timer: 3000
            })
          }
          if (data.message === "Do you want to delete this course ?( Enrollment information available )") {
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'info',
              title: 'Are you sure you want to delete this course?',
              text: "(Deleting the course will also delete the enrollment of your students to this course)",
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete course'
            }).then((result) => {
              if (result.value) {
                this.deletePermission = true;
                this.enrollmentPermission = true;
                this.lpservice.deleteCourse(courseId, true, this.enrollmentPermission).subscribe(res => {
                  if (data.status = "success") {
                    //this.getallcourses();

                    this.allCourses = this.allCourses.filter(x => x.courseId != courseId);
                    this.deletedCourseId.emit();
                    Swal.fire({
                      backdrop: false,
                      heightAuto: false,
                      type: 'success',
                      title: 'Success!',
                      text: res.message,
                      timer: 3000
                    })
                  }

                })
              }
            })
          }

          if (data.message === "You can't delete the course, Virtual class information available") {
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'error',
              title: 'Error!',
              text: "You cannot delete this course, as live class has been taken in past for this course",
              timer: 7000
            })
          }
        })
      }
    })

  }
  teacherNotAdded: boolean;
  createCourseClicked(){
    if(this.comservice.cbsObj.allSubjects && this.comservice.cbsObj.allSubjects.length == 0){
this.subNotAddedError = true;
    }
    else{
      this.subNotAddedError = false;
    }
    if(this.teacherDetails && this.teacherDetails.length == 0){
      this.teacherNotAdded = true;

    }
    else{
      this.teacherNotAdded = false;
    }
  }

}








