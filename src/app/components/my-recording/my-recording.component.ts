
import { Component, OnInit } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import { FormBuilder } from '@angular/forms';
import { LearningpodService } from '../../learningpod.service';
import { DomSanitizer } from '@angular/platform-browser';
import Swal from 'sweetalert2';
//import * as $ from 'jquery'
//import 'jqueryui';
@Component({
  selector: 'app-my-recording',
  templateUrl: './my-recording.component.html',
  styleUrls: ['./my-recording.component.css']
})
export class MyRecordingComponent implements OnInit {

  recordingDetailsArray:any;
  scDetails: any;
  url: any;
  isRecordingAvailable: boolean;
  loading: any;
  //edit: boolean;
  checked: boolean;
  filteredRecordingData = null;
  disableDelete: boolean;

  constructor(private sanitizer: DomSanitizer, public comservice: CommonServiceService, public lpService: LearningpodService) {

  }

  ngOnInit() {
    this.comservice.showNavbar = true;
    this.loading = true;
    this.isRecordingAvailable = true;
    this.disableDelete  = true;
    jQuery(document).ready(function ($) {

      // var draggable = $('#recordingPreview'); //element 
      $(document).on('click', '.min', function () {
        $(this).closest('.modal').find('.modal-content').slideUp(0.5);
        $(this).closest('.modal').find('.header').removeClass('hide');
      });

      $(document).on('click', '.max', function () {
        $(this).closest('.modal').find('.modal-content').slideDown(0.5);
        $(this).closest('.modal').find('.header').addClass('hide')
      });

      $(document).on('click', '.close', function () {
        $(this).closest('.modal').fadeOut();
      });
    });

    this.getRecordingFunction();
  }

  getRecordingFunction() {
    this.lpService.getRecording(this.comservice.myUserRole, this.comservice.myUserId).subscribe(data => {
      this.loading = false;
      this.recordingDetailsArray = data.courses;
      if(data.courses && data.courses.length > 0){
        this.isRecordingAvailable = false;
      }
    });
  }

  closeModal() {
    var video = document.getElementById('iframe') as HTMLIFrameElement;
    video.src = '';
  }
  recordingPreviewFunction(data) {
    window.open(data);
  }

  videoPreviewFunction(data) {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(data);
    var video = document.getElementById('video') as HTMLMediaElement;
    var source = document.createElement('source');
    video.controls = true;

    var modal = document.getElementById('videoPreview');
    document.addEventListener('click', function (event) {
      var isClickOutside = modal.contains(event.target as HTMLInputElement);
      if (isClickOutside) {
        var video = document.getElementById('video') as HTMLMediaElement;
        video.pause();
      }
    });
  }

  openFullscreen() {
    var elem = document.getElementById("iframe");
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    }
  }


  enableDeleteOnCheck() {
   
    if( $('.recording_checkbox:checked').length > 0){   
    this.disableDelete  = false;
    }
    else{
      this.disableDelete  = true;
    }
  }

  deleteRecordings() {
    Swal.fire({
      backdrop: false,
      heightAuto: false,
      type: 'info',
      title: 'Are you sure you want to delete these recording(s)?',
      text: "Deleting the recordings will permanently remove all the copies of it from our server",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete recording'
    }).then((result) => {
      if (result.value) {
        this.loading = true;
        var recordingIdToDeleteArray = [];
        var recordingArray = this.recordingDetailsArray;
        var count = 0;
        for (var i = 0; i < recordingArray.length; i++) {
          for (var j = 0; j < recordingArray[i].scDetails.length; j++) {
            var recordingCheckbox = document.getElementById("recording" + recordingArray[i].scDetails[j].recordingId) as HTMLInputElement;
            var checkValuesOnly = recordingCheckbox.checked;
            if (checkValuesOnly) {
              var recordingId = parseInt(recordingCheckbox.value);
              recordingIdToDeleteArray.push({ "recordingId": recordingId });
            }
            //count++;
          }
        }
        this.lpService.deleteRecording(recordingIdToDeleteArray).subscribe(data => {
          //this.edit = false;
          if (data.status == "success") {
            this.getFilteredData()
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: `${data.message}`,
              timer: 3000
            });
            this.loading = false;
          }
          else {
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'error',
              title: 'Error!',
              text: `${data.message}`,
              timer: 2000
            })
            this.loading = false;
          }
        }, (err) => {
          this.loading = false;
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: "Select atleast one recording to delete recording(s)",
            timer: 2000
          })
        });
      }
    });
  }

  getFilteredData() {
    var recordingForFilter = [];
    var j = 0;
    $('.recording_checkbox:checked').each(function () {
      recordingForFilter[j++] = $(this).val();
    });

    if (Array.isArray(recordingForFilter) && recordingForFilter.length) {
      for (var i = 0; i < this.recordingDetailsArray.length; i++) {
       
        this.recordingDetailsArray[i].scDetails = this.recordingDetailsArray[i].scDetails.filter(function (e) {
        return  !recordingForFilter.includes(e.recordingId + "");
    });
    this.recordingDetailsArray = this.recordingDetailsArray.filter(x => x.scDetails.length != 0);
    }
  }
  }

}