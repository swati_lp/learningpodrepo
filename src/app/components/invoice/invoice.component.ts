import { Component, OnInit } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import { Receipt } from '../../shared/models/receipt.model';


@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {


  date: any;
  studentName: any;
 
  constructor(public comservice: CommonServiceService, public receipt: Receipt) { }

  ngOnInit() {

    //let obj = new Receipt();
   // obj.studentName = studentName;
   this.comservice.showNavbar = false;
   var studentName = this.receipt.studentName;


   this.date = (new Date()).toDateString();

  }
  printFunction(){
    window.print();
  }


  
}
