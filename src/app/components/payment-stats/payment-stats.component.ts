import { Component, OnInit } from '@angular/core';
import { LearningpodService } from '../../learningpod.service';
import { CommonServiceService } from '../../common-service.service';

@Component({
  selector: 'app-payment-stats',
  templateUrl: './payment-stats.component.html',
  styleUrls: ['./payment-stats.component.css']
})
export class PaymentStatsComponent implements OnInit {
 
  constructor(public lpservice: LearningpodService,public comservice: CommonServiceService) { }

  coursePaymentInfo: any;
  coursePaymentInfoByCourseId: any;
  loading : boolean;
  //chartLabels: any;
  chartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero : true,
          
        }
      }]
    }
  };

  chartType = "bar";
  totalCoursesPayableAmount: any = 0
  totalCoursesPaidAmount: any = 0
  totalCourseCount: any = 0
  totalCoursesCostAmount: any = 0
  courseWiseTransaction : any

  chartData = [];
   chartLegend = true;

 


  chartColors = [
    { // first color
      backgroundColor: 'rgba(50, 59, 204, 0.86)',
      borderColor: 'rgba(225,10,24,0.2)',
      pointBackgroundColor: 'rgba(225,10,24,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(225,10,24,0.2)'
    },
    { // second color
      backgroundColor: 'rgba(211, 52, 71, 0.86)',
      borderColor: 'rgba(211, 52, 71, 0.86)',
      pointBackgroundColor: 'rgba(225,10,24,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(225,10,24,0.2)'
    },
    
    { // third color
      backgroundColor: 'rgba(23, 162, 184, 1)',
      borderColor: 'rgba(211, 52, 71, 0.86)',
      pointBackgroundColor: 'rgba(225,10,24,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(225,10,24,0.2)'
    },
    { // fourth color
      backgroundColor: 'rgba(255, 226, 154, 1)',
      borderColor: 'rgba(rgba(255, 226, 154, 1)',
      pointBackgroundColor: 'rgba(225,10,24,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(225,10,24,0.2)'
    }
    ];

  chartLabels = ['Overall course statistics'];

studentPaymentDetails : any

  onChartClick(event) {
  }
  ngOnInit() {
    this.loading = true;
    this.comservice.showNavbar = true;
    if(this.comservice.myUserRole == "student"){
      this.getPaymentDetailsByStudentId(this.comservice.myUserId);
      
    }

    

    this.chartType = "bar";
    $(document).ready(function () {

    });
if(this.comservice.myUserRole !='teacher'){
    this.getCoursePaymentStats();
}
  }

  getPaymentDetailsByCourseId(courseId){
    this.lpservice.getPaymentDetailsByCourseId(courseId).subscribe(x=>{
      this.loading = false;
this.coursePaymentInfoByCourseId = x;
    })
  }


getPaymentDetailsByStudentId(studentId){
  this.loading = true;
  this.lpservice.getPaymentDetailsByStudentId(studentId).subscribe(x=>{
    this.loading = false;
    this.studentPaymentDetails = x;

      this.chartData = [
        { data: [this.studentPaymentDetails.totalCourseCost], label: 'Courses Cost Amount' },
        { data: [this.studentPaymentDetails.totalPayableAmount], label: 'Payable Amount' },
        { data: [this.studentPaymentDetails.totalPaidAmount], label: 'Paid Amount' },
        {data : [this.totalCoursesCostAmount - this.totalCoursesPayableAmount],label : "Total discount"}
      ]
    
  })
}

 
  getCoursePaymentStats(){
    this.loading = true;
    this.lpservice.getCoursePaymentStats().subscribe((data) => {
      this.loading = false;
      this.coursePaymentInfo = data;
      this.totalCoursesPayableAmount = this.coursePaymentInfo.totalCoursesPayableAmount;
      this.totalCoursesPaidAmount = this.coursePaymentInfo.totalCoursesPaidAmount
      this.totalCourseCount = this.coursePaymentInfo.totalCourseCount
      this.totalCoursesCostAmount = this.coursePaymentInfo.totalCoursesCostAmount

      if(this.comservice.myUserRole == "cc"){
        this.chartData = [
          { data: [this.totalCoursesCostAmount], label: 'Courses Cost Amount' },
          { data: [this.totalCoursesPayableAmount], label: 'Payable Amount' },
          { data: [this.totalCoursesPaidAmount], label: 'Paid Amount' },
        {data : [this.totalCoursesCostAmount - this.totalCoursesPayableAmount],label : "Total discount"}
        ]
      }
 

     })
  }


  getCourseWiseTransaction(courseId){
this.studentPaymentDetails.studentCourse.filter(x => x.courseId === courseId).map(x => {
  this.courseWiseTransaction = x.studentFeeTransacton
});

  }


}
