import { Component, OnInit, ChangeDetectorRef, Input, EventEmitter, Output } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import { trigger, transition, animate, style } from '@angular/animations'
import { LearningpodService } from '../../learningpod.service';
import { Router } from '@angular/router';
//import { FilterTeacherData } from '../filter-teacher-data';
//import { filter } from 'rxjs/operators';

declare var $: any;
@Component({
  selector: 'app-explore-teacher',
  templateUrl: './explore-teacher.component.html',
  styleUrls: ['./explore-teacher.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({ transform: 'translateY(-100%)' }),
        animate('200ms ease-in', style({ transform: 'translateY(0%)' }))
      ]),
      transition(':leave', [
        animate('200ms ease-in', style({ transform: 'translateY(-100%)' }))
      ])
    ])
  ]
})
export class ExploreTeacherComponent implements OnInit {

  toggle: any;
  teacherList: any;
  loading: boolean;
  emptydatalist: boolean;
  showExploreDetails: boolean;
  teacherDetailsId: any;

  constructor(public comservice: CommonServiceService, public changedtection: ChangeDetectorRef, public lpservice: LearningpodService, public router: Router) {
  }

  ngOnInit() {
  //  $('body').removeClass('stop-scrolling')
    this.comservice.showNavbar = true;
    this.comservice.exploreHistory = 'teachers';
    this.showExploreDetails = false;
    this.loading = true;
    this.emptydatalist = false;
    this.getTeachersForExplore();
    this.toggle = false;
  }

  getTeachersForExplore() {
    if (this.comservice.myUserRole == "cc") {
      this.lpservice.getAffiliatedList(this.comservice.myUserRole, this.comservice.myUserId).subscribe((res) => {
        this.teacherList = res.approvedDetails;
        if (res.length == 0 || this.teacherList == undefined) {
          this.emptydatalist = true;
        }
        else {
          this.emptydatalist = false;
        }
        this.loading = false;
      });
    }
    else {
      this.lpservice.getAffiliatedList("cc", localStorage.getItem("ccId")).subscribe((res) => {
        this.teacherList = res.approvedDetails;
        if (res.length == 0) {
          this.emptydatalist = true;
        }
        else {
          this.emptydatalist = false;
        }
        this.loading = false;
      });
    }
  }


  teacherClicked(teacherId) {
    this.router.navigateByUrl('home/teacher');
    localStorage.setItem("teacherId", teacherId);
  }

  gotoTeacherProfile(data) {

    var res = data.replace("/", "%2F");

    this.router.navigateByUrl(`/profile/${res}`)
  }

  teacherDetails(teacherId) {
    this.teacherDetailsId = teacherId;
    this.showExploreDetails = true;
  }

  navigateback() {
    this.showExploreDetails = false;
  }


}









