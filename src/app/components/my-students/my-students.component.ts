import { Injectable } from '@angular/core';
import { Component, OnInit, Input, Output } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import { LearningpodService } from '../../learningpod.service';
import { MessagingService } from '../../shared/models/messaging.service';
import Swal from 'sweetalert2';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

declare var $: any;
@Injectable()

@Component({
  selector: 'app-my-students',
  templateUrl: './my-students.component.html',
  styleUrls: ['./my-students.component.css']
})

export class MyStudentsComponent implements OnInit {

  studentName: any;
  isFilteredDataAvailable: boolean;
  displayInfoMyAffiliation = [];
  //allClasses = [];
  selectedClassToUpdate: any
  studentPassword: any;
  dropdownSettings2: any
  phoneNumberToAdd: any;
  //p: any;
  loading: boolean;
  subjectlist: any;
  filterClass: any;
  filterBoard: any;
  studentIdForDetails: number
  startDate: any;
  studentId: any
  endDate: any;
  board: any;
  startDateNotEnrolled: any;
  courseListForStudentPayment: any;
  endDateNotEnrolled: any;
  courseIdNotEnrolled: any;
  studentFeeId: any;
  costNotEnrolled: any;
  studentFeeInfo: any;
  studentTransactionInfo: any;
  discountEnrolled: any;
  info: any;
  showFeeForEnrolled: any;
  showFeeForNotEnrolled: any;
  courseTitle: any;
  courseId: any;
  cost: any;
  studentList: any;
  pageNo: any;
  editDiscountVariable: any;
  payable: any;
  selectedItem: any;
  dropdownSettingsBoard = {
    singleSelection: true,
    idField: 'boardId',
    textField: 'boardName',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  arr: any;

  disableButton: boolean;
  isStudentEnrolledVal: boolean = false;
  isPaymentUpdatedVal: boolean = false;
  classListForFilter: any;
  boardListForFilter: any;

  @Output() isStudentUpdated: any = new EventEmitter();
  @Output() isPaymentUpdated: any = new EventEmitter();
  @Output() isStudentEnrolled: any = new EventEmitter();
  @Input() teacherStudentCount: any;
  constructor(public formbuilder: FormBuilder, public comservice: CommonServiceService,
    private lpservice: LearningpodService, private router: Router,
    public messagingService: MessagingService) {

  }

  ngOnInit() {
    this.comservice.showNavbar = true;
    this.isPaymentUpdatedVal = false;
    this.loading = true;
    this.pageNo = 0;
    this.displayInfoMyAffiliation = [];
    this.pageChanged();
    if (this.comservice.myUserRole == 'cc') {
      this.getAffiliatedStudent();
    }
    this.showFeeForEnrolled = false;
    this.showFeeForNotEnrolled = false;
    this.dropdownSettings2 = {
      singleSelection: true,
      idField: 'classId',
      textField: 'className',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    jQuery(document).ready(function ($) {
      var ww = document.body.clientWidth;
      if (ww < 400) {
        $('#filterCollapse').removeClass('show');
      }
    });
  }

  getSubjects() {
    this.lpservice.getallSubjects().subscribe((data) => {
      this.subjectlist = data;
    });
  }

  studentUpdated(studentData) {
    this.isStudentUpdated.emit(studentData);
  }

  clearModal() {
    this.studentName = "";
    this.phoneNumberToAdd = "";
    this.selectedClassToUpdate = [];
    this.board = [];
    this.studentPassword = "";
  }

  removeDuplicates(array, key) {
    return array.reduce((accumulator, element) => {
      if (!accumulator.find(el => el[key] === element[key])) {
        accumulator.push(element);
      }
      return accumulator;
    }, []);
  }
  contentTotalPages: any;
  a: any;
  pageChanged() {
    if (this.comservice.myUserRole == 'cc') {
      this.arr = [];
      this.contentTotalPages = Math.ceil(this.teacherStudentCount.studentCount / 20);
      for (var i = 1; i <= this.contentTotalPages; i++) {
        this.arr.push(i);
      }
    }
  }

  previous() {
    this.pageNo--;
    this.getAffiliatedStudent();
  }
  next() {
    this.pageNo++;
    this.getAffiliatedStudent();
  }
  pageClicked(event) {
    this.pageNo = parseInt(event.target.innerText);
    this.pageNo--
    this.getAffiliatedStudent();
  }
  diasableNext: boolean;
  diasablePrevious: boolean
  getAffiliatedStudent() {
    this.loading = true;
    this.lpservice.getAffiliatedStudent(this.comservice.myUserId, this.pageNo, 20).subscribe(res => {
      this.loading = false;
      this.pageNo++
      this.displayInfoMyAffiliation = res.studentInformation;
      if (this.displayInfoMyAffiliation != undefined) {
        let clearData1 = this.displayInfoMyAffiliation.map(x => x.classInfo);
        this.classListForFilter = clearData1.filter(x => x != null);
        this.classListForFilter = this.removeDuplicates(this.classListForFilter, 'className');
        let clearData3 = this.displayInfoMyAffiliation.map(x => x.boardInfo);
        this.boardListForFilter = clearData3.filter(x => x != null);
        this.boardListForFilter = this.removeDuplicates(this.boardListForFilter, 'boardName');

        this.studentUpdated(this.displayInfoMyAffiliation);
      }


      if (res.status = "success") {
        if (this.pageNo == this.contentTotalPages) {
          this.diasableNext = true;
          this.diasablePrevious = true;
        }
        else {
          this.diasableNext = false;
          this.diasablePrevious = false;

        }
        if (this.pageNo == 1) {
          this.diasablePrevious = true;
        }
        else {
          this.diasablePrevious = false;
        }
        this.pageNo--
      }
      else {
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: `${res.message}`,
          timer: 2000
        })
      }
    });
  }
  //boardlist: any;

  addStudentRemoveValidation() {
    this.nameError = false;
    this.passwordError = false;
    this.phoneError = false;
    $(".modal").on('shown.bs.modal', function () {
      $(this).find("input:visible:first").focus();
    });
    (document.getElementById('studentName') as HTMLInputElement).style.borderColor = "lightgrey";
    (document.getElementById('phoneNumberToAdd') as HTMLInputElement).style.borderColor = "lightgrey";
    (document.getElementById('studentPassword') as HTMLInputElement).style.borderColor = "lightgrey";

    this.disableButton = false;
    this.selectedClassToUpdate = [];
    this.board = [];
    this.studentName = null;
  }

  nameError: boolean = false;
  passwordError: boolean = false;
  phoneError: boolean = false;

  registerRestrictedStudent(studentName, phoneNumber, board, selectedClassToUpdate, studentPassword) {
    this.loading = true;
    //this.regVal(selectedClassToUpdate);
    this.disableButton = true;
    if (studentName == undefined || studentName == "") {
      (document.getElementById('studentName') as HTMLInputElement).style.borderColor = "red";
      this.disableButton = false;
      this.loading = false;
      this.nameError = true;
    } else {
      (document.getElementById('studentName') as HTMLInputElement).style.borderColor = "lightgrey";
      this.disableButton = false;
      //this.loading = false;
      this.nameError = false;
    }
    if (phoneNumber == undefined || phoneNumber == "") {
      (document.getElementById('phoneNumberToAdd') as HTMLInputElement).style.borderColor = "red";
      this.disableButton = false;
      this.loading = false;
      this.phoneError = true;
    } else {
      (document.getElementById('phoneNumberToAdd') as HTMLInputElement).style.borderColor = "lightgrey";
      this.disableButton = false;
      //  this.loading = false;
      this.phoneError = false;
    }
    if (studentPassword == undefined || studentPassword == "") {
      (document.getElementById('studentPassword') as HTMLInputElement).style.borderColor = "red";
      this.disableButton = false;
      this.loading = false;
      this.passwordError = true;
    } else {
      (document.getElementById('studentPassword') as HTMLInputElement).style.borderColor = "lightgrey";
      this.disableButton = false;
      //this.loading = false;
      this.passwordError = false;
    }

    if (studentName != null && phoneNumber != undefined && studentPassword != undefined) {
      if (selectedClassToUpdate != null && selectedClassToUpdate.length > 0) {
        var newClassId = selectedClassToUpdate[0].classId;
      }
      if (board != null && board.length > 0) {
        var newBoardId = board[0].boardId;
      }
      this.lpservice.registerRestrictedStudent(this.comservice.myUserId, studentName, phoneNumber, newBoardId, newClassId, studentPassword).subscribe(res => {
        this.loading = false;
        this.disableButton = false;
        if (res.status == "success") {
          var closeRegModalBtn = document.getElementById('closeRegisterModalStudent');
          closeRegModalBtn.click()
          //var res = displayInfoMyAffiliation.map(obj => arr2.find(o => o.id === obj.id) || obj);
          var studentJson = { "studentId": res.studentId, "studentName": studentName, "phoneNo": phoneNumber, "boardInfo": board[0], "classInfo": selectedClassToUpdate[0] }
          if (this.displayInfoMyAffiliation == undefined) {
            this.displayInfoMyAffiliation = [];
          }

          this.displayInfoMyAffiliation.push(studentJson);
          // this.getAffiliatedStudent();
          if (this.displayInfoMyAffiliation != undefined) {
            let clearData1 = this.displayInfoMyAffiliation.map(x => x.classInfo);
            this.classListForFilter = clearData1.filter(x => x != null);
            this.classListForFilter = this.removeDuplicates(this.classListForFilter, 'className');
            let clearData3 = this.displayInfoMyAffiliation.map(x => x.boardInfo);
            this.boardListForFilter = clearData3.filter(x => x != null);
            this.boardListForFilter = this.removeDuplicates(this.boardListForFilter, 'boardName');

            this.studentUpdated(this.displayInfoMyAffiliation);
          }

          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: `${res.message}`,
            timer: 2000
          })
          this.clearModal()
        }
        else {
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: `${res.message}`,
            timer: 2000
          })
        }
      })
      //this.loading = false;
    }
    //  this.loading = false;
  }

  deleteStudent(id) {
    Swal.fire({
      backdrop: false,
      heightAuto: false,
      type: 'info',
      title: 'Are you sure you want to delete the student?',
      text: "Deleting the student will delete all the payment information related to this student",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete student'
    }).then((result) => {
      if (result.value) {
        this.loading = true;
        this.lpservice.deleteStudent(id).subscribe((data) => {
          this.loading = false;
          if (data.status == "success") {
            this.isPaymentUpdatedVal = true;
            this.isPaymentUpdated.emit(this.isPaymentUpdatedVal)
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: `${data.message}`,
              timer: 2000
            });
            (document.getElementById("deleteHide" + id) as HTMLInputElement).disabled = true;
            this.getAffiliatedStudent()
          }
          else {
            Swal.fire({
              backdrop: false,
              heightAuto: false,
              type: 'error',
              title: 'Error!',
              text: `${data.message}`,
              timer: 2000
            })
          }
        });
      }
    })


  }

  getStudentCourses(studentId, studentName) {
    this.showFeeForNotEnrolled = false;
    this.showFeeForEnrolled = false;
    this.studentId = studentId;
    this.studentName = studentName;
    localStorage.setItem("studentName", studentName);
    localStorage.setItem("studentId", studentId);
    this.loading = true;
    this.lpservice.getCoursesForExploreForStudent(studentId, this.comservice.myUserId).subscribe(res => {
      this.loading = false;
      this.courseListForStudentPayment = res;
    })
  }

  passCourseDetailsNotEnrolled(x) {
    this.selectedItem = x;
    this.showFeeForNotEnrolled = true;
    this.showFeeForEnrolled = false;
    this.courseTitle = x.title;
    this.courseId = x.courseId;
    this.cost = x.cost;
    this.startDateNotEnrolled = x.startDate;
    this.endDateNotEnrolled = x.endDate;

  }
  Enroll(cost, discount) {
    this.loading = true;
    if (discount == undefined) {
      this.payable = cost;
    } else {
      this.payable = cost - discount;
    }
    //  this.loading = true;

    this.lpservice.paymentInitiate(cost, this.payable, this.startDateNotEnrolled, this.endDateNotEnrolled, this.courseId, this.studentId, "Monthly").subscribe((data) => {
      this.loading = false;
      if (data.status = "success") {
        this.isStudentEnrolledVal = true;
        this.isStudentEnrolled.emit(this.isStudentEnrolledVal)
        this.studentFeeId = data.stduentCourseFeeId;
        this.getStudentCourses(this.studentId, this.studentName);
      }
    })
  }
  pay(paymentStartDate, paymentEndDate, amountPaid, transactionDate) {
    this.loading = true;
    this.lpservice.studentTransaction(paymentStartDate, paymentEndDate, amountPaid, this.studentFeeId, this.courseIdNotEnrolled, this.studentId, transactionDate).subscribe((data) => {
      this.loading = false;
      if (data.status = "success") {
        this.isPaymentUpdatedVal = true;
        this.isPaymentUpdated.emit(this.isPaymentUpdatedVal)
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'success',
          title: 'Success!',
          text: `${data.message}`,
          timer: 2000
        })

        this.lpservice.getFeeAndTransactionDetails(this.studentId, this.courseId).subscribe((data) => {
          this.studentFeeInfo = data.studentCourseFee;
          if (data.studentCourseFee) {
            this.discountEnrolled = data.studentCourseFee.courseCost - data.studentCourseFee.payableAmount;
            this.payable = data.studentCourseFee.payableAmount;
            this.studentFeeId = data.studentCourseFee.stduentCourseFeeId;
            this.totalPaidAmount = data.studentCourseFee.paidAmount;
            this.totalPendingAmount = data.studentCourseFee.payableAmount - data.studentCourseFee.paidAmount;
            localStorage.setItem("pendingAmount", this.totalPendingAmount);
          }
        }
        )
      }
    })

  }


  showFeeTansactionTable: any;
  totalPaidAmount: any;
  totalPendingAmount: any;
  passCourseDetailsEnrolled(x) {
    this.selectedItem = x;
    this.courseTitle = x.title;
    this.cost = x.cost;
    this.courseId = x.courseId;
    //this.loading = true;
    localStorage.setItem("courseCost", this.cost);
    localStorage.setItem("courseTitle", this.courseTitle);
    this.lpservice.getFeeAndTransactionDetails(this.studentId, x.courseId).subscribe((data) => {
      this.studentFeeInfo = data.studentCourseFee;
      if (data.studentCourseFee) {
        this.discountEnrolled = data.studentCourseFee.courseCost - data.studentCourseFee.payableAmount;
        this.payable = data.studentCourseFee.payableAmount;
        this.studentFeeId = data.studentCourseFee.stduentCourseFeeId;
        this.totalPaidAmount = data.studentCourseFee.paidAmount;
        this.totalPendingAmount = data.studentCourseFee.payableAmount - data.studentCourseFee.paidAmount;
        localStorage.setItem("pendingAmount", this.totalPendingAmount);
      }
      this.studentTransactionInfo = data.studentCourseFeeTransaction;
      if (data.studentCourseFeeTransaction == "No transaction yet") {
        this.showFeeForNotEnrolled = false;
        this.showFeeForEnrolled = true;
        this.studentTransactionInfo = []
      }
      else {
        this.showFeeForEnrolled = true;
        this.showFeeForNotEnrolled = false;

      }
    }
    )
  }
  editDiscount() {
    this.editDiscountVariable = true;
  }


  updateFees(courseCost, discountEnrolled, startDate, endDate, frequency) {
    this.payable = courseCost - discountEnrolled;
    this.editDiscountVariable = false;
    this.loading = true;
    localStorage.setItem("discount", discountEnrolled);
    this.lpservice.updateFees(courseCost, this.payable, startDate, endDate, frequency, this.studentId, this.courseId, this.studentFeeId).subscribe((data) => {
      this.isPaymentUpdatedVal = true;
      this.isPaymentUpdated.emit(this.isPaymentUpdatedVal)
      this.loading = false;
      this.info = data;
    })
    this.lpservice.getFeeAndTransactionDetails(this.studentId, this.courseId).subscribe((data) => {
      this.studentFeeInfo = data.studentCourseFee;
      if (data.studentCourseFee) {
        this.discountEnrolled = data.studentCourseFee.courseCost - data.studentCourseFee.payableAmount;
        this.payable = data.studentCourseFee.payableAmount;
        this.studentFeeId = data.studentCourseFee.stduentCourseFeeId;
        this.totalPaidAmount = data.studentCourseFee.paidAmount;
        this.totalPendingAmount = data.studentCourseFee.payableAmount - data.studentCourseFee.paidAmount;
        localStorage.setItem("pendingAmount", this.totalPendingAmount);
      }
    }
    )
  }
  enableEdit = false;
  enableEditIndex = null;
  enableEditMethod(i, event) {

    this.enableEdit = true;
    this.enableEditIndex = i;
    (document.getElementById("addNewRow") as HTMLInputElement).disabled = true;
    this.addEvent = event.target.id;
  }

  cancel() {

    if (this.addEvent == "editRow") {
      this.enableEditIndex = null;
      this.enableEdit = false;
      (document.getElementById("addNewRow") as HTMLInputElement).disabled = false;
    }
    else {
      this.enableEditIndex = null;
      this.enableEdit = false;
      (document.getElementById("addNewRow") as HTMLInputElement).disabled = false;
      this.studentTransactionInfo.pop();
    }

  }
  Modify(startDate, endDate, paidAmount, studentFeeTransactionId, transactionDate) {
    // this.editable = false;
    (document.getElementById("addNewRow") as HTMLInputElement).disabled = false;
    this.enableEditIndex = null;
    this.enableEdit = false;
    this.showFeeTansactionTable = true;
    this.loading = true;
    if (studentFeeTransactionId != undefined) {
      this.lpservice.modifyTransactionDetails(startDate, endDate, paidAmount, this.studentId, this.courseId, this.studentFeeId, studentFeeTransactionId, transactionDate).subscribe((data) => {
        this.loading = false;
        this.info = data;
        this.isPaymentUpdatedVal = true;
        this.isPaymentUpdated.emit(this.isPaymentUpdatedVal)
      })

      this.lpservice.getFeeAndTransactionDetails(this.studentId, this.courseId).subscribe((data) => {
        this.studentFeeInfo = data.studentCourseFee;
        if (data.studentCourseFee) {
          this.discountEnrolled = data.studentCourseFee.courseCost - data.studentCourseFee.payableAmount;
          this.payable = data.studentCourseFee.payableAmount;
          this.studentFeeId = data.studentCourseFee.stduentCourseFeeId;
          this.totalPaidAmount = data.studentCourseFee.paidAmount;
          this.totalPendingAmount = data.studentCourseFee.payableAmount - data.studentCourseFee.paidAmount;
          localStorage.setItem("pendingAmount", this.totalPendingAmount);
        }
      }
      )
    }
    else {
      this.loading = false;
      if (startDate == undefined || endDate == undefined || paidAmount == undefined) {
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'error!',
          text: "Please fill all details",
          timer: 2000
        })
      }
      else {
        this.pay(startDate, endDate, paidAmount, transactionDate);
      }
    }
  }

  addEvent: any;
  addRow() {
    this.addEvent = " ";
    if (this.studentTransactionInfo == "No transaction yet") {
      this.studentTransactionInfo = [{}];
      this.enableEditMethod(0, null);
      /// this.editable = true;
    }
    else {
      this.studentTransactionInfo.push({});
      let i = this.studentTransactionInfo.length;
      this.enableEditMethod(i - 1, null);
    }

  }


  studentInfo; any;
  courseClicked(x) {
    this.studentInfo = x.studentInfo;
  }

  updateNameError: boolean = false;
  updatePhoneError: boolean = false;

  updateStudent(updatedStudentInfo, updateClassInfo, updateBoardInfo) {
    this.loading = true;
    if (this.updatedStudentInfo.studentPassword == undefined) {
      this.updatedStudentInfo.studentPassword = "";
    }
    if (this.updateClassInfo != undefined && this.updateClassInfo.length > 0) {
      var classId = this.updateClassInfo[0].classId;
      this.updatedStudentInfo.classInfo = updateClassInfo[0];
    }
    if (this.updateBoardInfo != undefined && this.updateBoardInfo.length > 0) {
      var boardId = this.updateBoardInfo[0].boardId;
      this.updatedStudentInfo.boardInfo = updateBoardInfo[0];
    }
    if (updatedStudentInfo.studentName == "" || updatedStudentInfo.studentName == undefined) {
      this.updateNameError = true
    } else {
      this.updateNameError = false;
    }

    if (updatedStudentInfo.phoneNo == undefined) {
      this.updatePhoneError = true
    } else {
      this.updatePhoneError = false;
    }
    if (updatedStudentInfo.studentName != "" && updatedStudentInfo.phoneNo != undefined) {
      this.lpservice.updateStudentFromCc(updatedStudentInfo, classId, boardId).subscribe(x => {
        this.loading = false;
        if (x.status == "success") {
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: "success",
            title: "Success",
            text: x.message,
            timer: 2000
          });
          (document.getElementById('updateStudent')).click();
          // var a = this.displayInfoMyAffiliation;
          //var b = updatedStudentInfo;
          if (this.displayInfoMyAffiliation != undefined) {
            let clearData1 = this.displayInfoMyAffiliation.map(x => x.classInfo);
            this.classListForFilter = clearData1.filter(x => x != null);
            this.classListForFilter = this.removeDuplicates(this.classListForFilter, 'className');
            let clearData3 = this.displayInfoMyAffiliation.map(x => x.boardInfo);
            this.boardListForFilter = clearData3.filter(x => x != null);
            this.boardListForFilter = this.removeDuplicates(this.boardListForFilter, 'boardName');

            this.studentUpdated(this.displayInfoMyAffiliation);
          }
        }

      })
    } else {
      this.loading = false;
    }

  }
  studentNumber: any;
  class: any;
  updatedStudentInfo: any;
  updateClassInfo: any;
  updateBoardInfo: any;
  getStudentById(x) {
    $(".modal").on('shown.bs.modal', function () {
      $(this).find("input:visible:first").focus();
    });
    let updatedcls = [];
    if (x.classInfo == undefined) {
      updatedcls = undefined;
    }
    else {
      let clId = x.classInfo.classId;
      let clName = x.classInfo.className;
      updatedcls.push({ "classId": clId, "className": clName });
    }

    let updatedBoard = [];

    if (x.boardInfo == undefined) {
      updatedBoard = undefined;
    }
    else {
      let boardId = x.boardInfo.boardId;
      let boardName = x.boardInfo.boardName;
      updatedBoard.push({ "boardId": boardId, "boardName": boardName });
    }
    this.updateClassInfo = updatedcls;
    this.updateBoardInfo = updatedBoard;
    this.updatedStudentInfo = x;
    this.studentName = x.studentName;
    this.studentId = x.studentId;
    this.showTable = false;
    this.courseInfo = [];
    this.startDate = undefined;
    this.endDate = undefined;
    this.showPrint = false;
  }

  printIndex(i) {
    var x = (document.getElementById("myTable") as HTMLTableElement)
    var startDate = x.rows[i].cells[0].innerText;
    var endate = x.rows[i].cells[1].innerText;
    var amountPaid = x.rows[i].cells[2].innerText;

    localStorage.setItem("startDate", startDate);
    localStorage.setItem("endate", endate);
    localStorage.setItem("amountPaid", amountPaid);
    localStorage.setItem("installment", i + 1);
  }

  isFilterErrorShow() {
    this.isFilteredDataAvailable = false;
  }

  isFilterErrorHide() {
    this.isFilteredDataAvailable = true;
  }

  filteredStudentData = null
  getFilteredData() {

    this.filteredStudentData = null;
    var classForFilter = [];
    var boardForFilter = [];
    var i = 0;
    var l = 0;
    $('.class_checkbox:checked').each(function () {
      classForFilter[i++] = $(this).val();
    });
    $('.board_checkbox:checked').each(function () {
      boardForFilter[l++] = $(this).val();
    });
    (document.getElementById("collapseStudent") as HTMLInputElement).click();
    if (classForFilter.length == 0 && boardForFilter.length == 0) {
      this.filteredStudentData = null;
      this.isFilterErrorHide();
    }
    //for filtering class
    if (Array.isArray(classForFilter) && classForFilter.length) {
      let clearData1 = this.displayInfoMyAffiliation.filter(x => x.classInfo != null);
      this.filteredStudentData = clearData1.filter(function (e) {
        return classForFilter.includes(e.classInfo.className)
      });

      if (Array.isArray(this.filteredStudentData) && this.filteredStudentData.length) {
        this.isFilterErrorHide();
      } else {
        this.isFilterErrorShow();
      }
    }

    //for filtering board
    if (Array.isArray(boardForFilter) && boardForFilter.length) {
      if (this.filteredStudentData != null) {
        let clearData1 = this.filteredStudentData.filter(x => x.boardInfo != null)
        this.filteredStudentData = clearData1.filter(function (e) {
          return boardForFilter.includes(e.boardInfo.boardName)
        });
        if (Array.isArray(this.filteredStudentData) && this.filteredStudentData.length != 0) {
          this.isFilterErrorHide();
        } else {
          this.isFilterErrorShow();
        }

      } else {
        let clearData1 = this.displayInfoMyAffiliation.filter(x => x.boardInfo != null)
        this.filteredStudentData = clearData1.filter(function (e) {
          return boardForFilter.includes(e.boardInfo.boardName)
        });
        if (Array.isArray(this.filteredStudentData) && this.filteredStudentData.length) {
          this.isFilterErrorHide();
        } else {
          this.isFilterErrorShow();
        }
      }
    }

  }


  clearFilteredData() {
    this.isFilterErrorHide();
    //document.getElementById('hideError').click();
    this.filteredStudentData = null;
    for (var a = 0; a < this.classListForFilter.length; a++) {
      (document.getElementById("class" + a) as HTMLInputElement).checked = false;
    }
    for (var i = 0; i < this.boardListForFilter.length; i++) {
      (document.getElementById("board" + i) as HTMLInputElement).checked = false;
    }

  }
  filteredStudents: any;
  filter() {

    let filtered: any
    if (this.filterClass == undefined && this.filterBoard == undefined) {
      this.filteredStudents = null;
    }
    if (this.filterClass != undefined) {
      let clearData = this.displayInfoMyAffiliation.filter(x => {
        return x.classInfo != null;
      });
      filtered = clearData.filter(x => x.classInfo.classId == this.filterClass[0].classId);
      if (filtered.length == 0) {

        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'warning',
          title: 'Warning!',
          text: 'No students found with this id'
        })

        this.filteredStudents = null;

      } else {
        this.filteredStudents = filtered;
      }
    }

    if (this.filterBoard != undefined) {
      if (this.filteredStudents != null) {
        let clearData = this.filteredStudents.filter(x => {
          return x.boardInfo != null;
        });
        this.filteredStudents = clearData.filter(x => x.boardInfo.boardName == this.filterBoard[0].boardName);
      } else {


        let clearData = this.displayInfoMyAffiliation.filter(x => {
          return x.boardInfo != null;
        });
        this.filteredStudents = clearData.filter(x => x.boardInfo.boardName == this.filterBoard[0].boardName);
      }
    }
  }

  clearFilter() {
    this.filterClass = undefined;
    this.filterBoard = undefined;
    this.filteredStudents = null;
  }

  openStudentDetails(studentId) {
    this.studentIdForDetails = studentId
    this.router.navigate(['home/studentProfile', studentId]);
  }

  studentDetails: any
  studentCourseDetails: any
  studentPaymentDetails: any

  getStudentDetails(studentId) {
    this.studentId = studentId;
    this.lpservice.getStudentDetails(studentId).subscribe(res => {
      this.studentDetails = res.studentInfo;
      this.studentCourseDetails = res.courseInfo;
      this.studentPaymentDetails = res.studentFeeDetails;
    });

    this.showTable = false;
    this.courseInfo = [];
    this.startDate = undefined;
    this.endDate = undefined;
    this.showPrint = false;

  }

  backToMyStudents() {
    this.studentDetails = null
  }

  showTable: any;
  courseInfo = [];
  showPrint: any;

  loadingReport: any;
  totalCounts: any;
  getStudentReport(startDate, endDate) {
    this.loadingReport = true;
    this.showTable = false;
    if (startDate != undefined && endDate != undefined) {
      var start = new Date(startDate).toISOString().slice(0, 10);
      var end = new Date(endDate).toISOString().slice(0, 10);
      this.lpservice.getStudentReport(this.studentId, start, end).subscribe((data) => {
        this.loadingReport = false;
        if (data.status == "success") {
          this.showTable = true;
          //var courseInfo = [];
          for (var i = 0; i < data.courseInfo.length; i++) {
            for (var j = 0; j < data.courseInfo[i].scClassInfo.length; j++) {
              this.courseInfo.push(data.courseInfo[i].scClassInfo[j]);
            }
          }
          this.showPrint = true;
          this.totalCounts = data;

          //(document.getElementById("hiddenButtonForReport")as HTMLInputElement).click();
        }
        else {
          this.showTable = false;
          this.courseInfo = data.message;
          this.loadingReport = false;
        }
      }, (err) => {
        this.loading = false;
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: "Server side error",
          timer: 2000
        })
        // this.editable = false;
      });
    }
    else {
      this.showTable = false;
      this.courseInfo = ["Please select a valid start date and end date to get report of student"];
      this.loadingReport = false;
    }

  }

  printReport() {
    window.print();
  }

  showNoResults: boolean;

  searchStudentByName() {
    this.filteredStudentData = [];
    //this.displayInfoMyAffiliation = true;
    this.showNoResults = false;
    var input = document.getElementById("searchStudent") as HTMLInputElement;
    var search = input.value.toUpperCase();
    for (var i = 0; i < this.displayInfoMyAffiliation.length; i++) {
      var a = this.displayInfoMyAffiliation[i].studentName;
      if (a.toUpperCase().indexOf(search) > -1) {
        this.showNoResults = false;
        this.filteredStudentData.push(this.displayInfoMyAffiliation[i]);
      }

    }
    if (this.filteredStudentData.length == 0) {
      this.showNoResults = true;
    }


  }
  scrollEvent = (event: any): void => {

    var scroll = document.getElementById("scrollDivStudent").scrollTop;
    if (scroll > 10) {
      // mybutton.style.display = "block";
      $("#scrollBtnStudent").fadeIn();
    } else {
      $("#scrollBtnStudent").fadeOut();
    }
  }


  topFunction() {
    document.getElementById("scrollDivStudent").scrollTop = 0;
  }
  selectedfile: any;
  loadingBulkStudent: boolean =false;
  uploadExcel(event) {
    this.loadingBulkStudent = true;
    this.selectedfile = event.target.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(this.selectedfile);
    reader.onload = (event) => {
    }
    const newfile: File = this.selectedfile;
    this.lpservice.uploadExcel(newfile).subscribe((res) => {
      this.loadingBulkStudent = false;
      document.getElementById("closeUpdateModal").click();
      if (res.status = "success") {
        if (res.registerStudents.length > 0) {
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'success',
            title: 'Success!',
            text: `${res.message}`,
            timer: 2000
          })
          this.getAffiliatedStudent();
        }
        var students = [];
        for (var i = 0; i < res.notRegisterStudents.length; i++) {
          students.push(res.notRegisterStudents[i].studentName);
        }
        students = students.filter(x => x != "")
        if (students.length > 0) {
          Swal.fire({
            backdrop: false,
            heightAuto: false,
            type: 'error',
            title: 'Error!',
            text: `There was an error uploading the student(s) "${students}" as the respective phone number already exist in our database`
          })
        }
      }
      else {
        this.loadingBulkStudent = false;
        document.getElementById("closeUpdateModal").click();
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: `${res.message}`,
          timer: 2000
        })
      }
    },
      (err) => {
        this.loadingBulkStudent = false;
        Swal.fire({
          backdrop: false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: `${err}`,
          timer: 2000
        })
      }
    );
  }

}



