import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonServiceService } from '../../common-service.service';

//import * as $ from 'jquery';
declare var $: any;


@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.css']
})
export class FeaturesComponent implements OnInit {


  constructor(private router : Router,public comservice:CommonServiceService) { }


  ngOnInit() {
    this.comservice.showNavbar = true;
   // this.rollUp()
  // $('body').removeClass('stop-scrolling')
  }

  gotoPolicy(){
    this.router.navigateByUrl('privacy-policy');
  }

  rollUp() {
    $(document).ready(function () {
      $(this).scrollTop(0);
    });
  }



}

