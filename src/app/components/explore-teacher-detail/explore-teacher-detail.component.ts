import { Component, OnInit, Input } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import { LearningpodService } from '../../learningpod.service';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
class ImageSnippet {
  constructor(public src: string, public file: File) { }
}

@Component({
  selector: 'app-explore-profile',
  templateUrl: './explore-teacher-detail.component.html',
  styleUrls: ['./explore-teacher-detail.component.css']
})
export class ExploreTeacherDetailComponent implements OnInit {

  teacherInfo: any;
  selectedClasses = [];
  selectedSubjects = [];
  myCoursesData = [];
  loading: boolean;
  url: any;
  teacherUrl: any;
  ccUrl: any;
  teacherCoursesInfo: any;
  teacherId: any;
  userProfileUrl: string
  showExploreCourseDetails: any;
  courseDetailsId: any;
  @Input() teacher: any;

  constructor(private router: Router, private route: ActivatedRoute, public comservice: CommonServiceService, private lpservice: LearningpodService) { }

  ngOnInit() {
    this.comservice.showNavbar = true;
    this.teacherId = localStorage.getItem("teacherId");
    this.getTeacherDetail();
    this.loading = true;
    this.showExploreCourseDetails = false;
  }

  back() {
    this.router.navigateByUrl(`home/${this.comservice.exploreHistory}`);
  }

  gotoCourse(data) {
    this.router.navigateByUrl(`course/${data}`);
  }

  getTeacherDetail() {

    this.lpservice.exploreTeacherDetails(this.teacherId).subscribe(res => {
      this.teacherInfo = res.teacherDetails.teacherInfo;
      this.teacherCoursesInfo = res.teacherDetails.teacherCoursesInfo;
      for (var i = 0; i < res.teacherDetails.teacherClassInfo.length; i++) {
        this.selectedClasses.push(res.teacherDetails.teacherClassInfo[i].classInfo);
      }
      for (var i = 0; i < res.teacherDetails.teacherSubjectInfo.length; i++) {
        this.selectedSubjects.push(res.teacherDetails.teacherSubjectInfo[i]);
      }
      this.loading = false;
    })
  }

  // getallcourses() {
  //   this.lpservice.getMyCourses(this.comservice.myUserRole, this.comservice.myUserId).subscribe(res => {
  //     this.myCoursesData = res;
  //     this.loading = false;
  //   });
  // }

  courseDetails(courseId) {
    this.courseDetailsId = courseId;
    this.showExploreCourseDetails = true;
  }
  courseClicked(courseId) {
    localStorage.setItem("courseId", courseId);
  }
}