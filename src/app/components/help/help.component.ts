import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})
export class HelpComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.rollUp();
  }
  rollUp() {
    $(document).ready(function () {
      $(this).scrollTop(0);
    });
  }
}
