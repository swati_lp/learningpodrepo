import { Component, OnInit, Input, Output } from '@angular/core';
import { CommonServiceService } from '../../common-service.service';
import { LearningpodService } from '../../learningpod.service';
import Swal from 'sweetalert2';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { MessagingService } from '../../shared/models/messaging.service';
//import undefined = require('firebase/empty-import');
//import undefined = require('firebase/empty-import');
declare var $: any;
@Component({
  selector: 'app-my-teachers',
  templateUrl: './my-teachers.component.html',
  styleUrls: ['./my-teachers.component.css']
})
export class MyTeachersComponent implements OnInit {
  teacherId: any
  scheduleClassInfo: any;
  virtualClassInfo = [];
  showPrint: any;
  loadingReport: any;
  displayInfoMyAffiliation = [];
  updatedTeacherInfo = {
    teacherName: "",
    qualifications: "",
    teacherSkills: "",
    phoneNo: "",
    experience: null,
    teacherPassword: ""
  }
  teacherName: string;
  teacherQualification: string;
  teacherSkill: string;
  phoneNumber: string;
  teacherExperience: number;
  teacherPassword: string
  allSubjects: any[];
  allClasses: any[];
  selectedClassesToUpdate = [];
  selectedSubjectsToUpdate = [];
  selectedClassesToUpdate1 = [];
  selectedSubjectsToUpdate1 = [];
  selectedBoardsToUpdate1 = [];
  selectedBoardsToUpdate=[];
  updatedSubjects = [];
  disableButton: boolean;
  dropdownSettings1: any
  dropdownSettings2: any
  dropdownSettings3: any
  startDate: any;
  endDate: any;
  
  teacherData: any;
  isTeacherDeleted: boolean
  UpdatedTeacherPassword: any;
  loading: boolean;
  showTable: boolean;



  @Input() hero: any;
  @Input('master') masterName: string;
  @Output() isTeacherUpdated: any = new EventEmitter();
  @Output() isDelete: any = new EventEmitter();
  //@Output() isTeacherUpdated: any = new EventEmitter();

  constructor(public formbuilder: FormBuilder, 
    public messagingService: MessagingService,
    public comservice: CommonServiceService, private lpservice: LearningpodService) { }

  ngOnInit() {
    //this.displayInfoMyAffiliation = true;

    this.comservice.showNavbar = true;
    if(this.comservice.myUserRole == 'cc'){
    this.getaffiliationdata();

    }
    this.dropdownSettings1 = {
      singleSelection: false,
      idField: 'subjectId',
      textField: 'subjectName',
      selectAllText: 'Select All',
      unSelectAllText: 'Unselect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.dropdownSettings2 = {
      singleSelection: false,
      idField: 'classId',
      textField: 'className',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.dropdownSettings3 = {
      singleSelection: false,
      idField: 'boardId',
      textField: 'boardName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.loading = true;
   // document.getElementById("scrollBtnTeacher").style.display = "none";
  }

  teacherEvent() {
    this.teacherData = this.displayInfoMyAffiliation
    this.isTeacherUpdated.emit(this.teacherData);
  }


  teacherDeleted(deleted: boolean) {
    this.isDelete.emit(deleted);
  }



  teacherClassInfo: any;
  classInfo = [];
  getaffiliationdata() {
    this.lpservice.getAffiliatedList(this.comservice.myUserRole, this.comservice.myUserId).subscribe(res => {
      this.loading = false;
      this.displayInfoMyAffiliation = res.approvedDetails;
      //console.log(this.displayInfoMyAffiliation);
      if (res.status = "success") {
        this.isTeacherUpdated.emit(this.displayInfoMyAffiliation);
        this.teacherEvent();
   
      }
      else {
        Swal.fire({
          backdrop:false,
          heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: `${res.message}`,
          timer: 2000
        })
        //alert(res.message);
      }
    });
  }

  getSortedClasses(classes){
        classes.sort((x, y) => {
          return this.comservice.compareNumberAndChar(x.classInfo.className, y.classInfo.className)
        })
        return classes;
  }
  getSortedSubjects(subjects){
    subjects.sort((x, y) => {
      return this.comservice.sortCharArray(x.subjectInfo.subjectName, y.subjectInfo.subjectName); 
  });
  return subjects;
  }
  getSortedBoards(boards){
    boards.sort((x, y) => {
      return this.comservice.sortCharArray(x.boardInfo.boardName, y.boardInfo.boardName); 
  });
  return boards;
  }

  
  getAffiliatedTeacherDetails(x) {
    $(".modal").on('shown.bs.modal', function () {
      $(this).find("input:visible:first").focus();
  });
    this.selectedClassesToUpdate1 = [];
    this.updatedTeacherInfo = x.teacherInfo;
    if (x.teacherInfo.classInfo == undefined) {
      this.selectedClassesToUpdate1 = undefined;
    }
    else { 
      for(var i = 0; i < x.teacherInfo.classInfo.length; i++){
      let classId = x.teacherInfo.classInfo[i].classInfo.classId;
      let className = x.teacherInfo.classInfo[i].classInfo.className;
      this.selectedClassesToUpdate1.push({ "classId": classId, "className": className });
      }
    }

    this.selectedSubjectsToUpdate1 = [];

    if (x.teacherInfo.subjectInfo == undefined) {
      this.selectedSubjectsToUpdate1 = undefined;
    }
    else {
      for(var i = 0; i < x.teacherInfo.subjectInfo.length; i++){
      let subjectId = x.teacherInfo.subjectInfo[i].subjectInfo.subjectId;
      let subjectName = x.teacherInfo.subjectInfo[i].subjectInfo.subjectName;
      this.selectedSubjectsToUpdate1.push({ "subjectId": subjectId, "subjectName": subjectName });
      }
     
    }
    this.selectedBoardsToUpdate1 = [];
    if (x.teacherInfo.boardInfo == undefined) {
      this.selectedBoardsToUpdate1 = undefined;
    }
    else {
      for(var i = 0; i < x.teacherInfo.boardInfo.length; i++){
      let boardId = x.teacherInfo.boardInfo[i].boardInfo.boardId;
      let boardName = x.teacherInfo.boardInfo[i].boardInfo.boardName;
      this.selectedBoardsToUpdate1.push({ "boardId": boardId, "boardName": boardName });
      }
    }

    this.updateNameError = false;
    this.updatePhoneNoError = false;
   
    (document.getElementById('updateTeacherName') as HTMLInputElement).style.borderColor = "lightgrey";
    (document.getElementById('updatePhoneNo') as HTMLInputElement).style.borderColor = "lightgrey";
    this.disableButton = false;
    this.teacherId = x.teacherInfo.teacherId;


  }

  addTeacher() {
    this.nameError = false;
    this.phoneNoError = false;
    this.passwordError = false;
    $(".modal").on('shown.bs.modal', function () {
      $(this).find("input:visible:first").focus();
  });
    (document.getElementById('teacherName') as HTMLInputElement).style.borderColor = "lightgrey";
    (document.getElementById('phoneNumber') as HTMLInputElement).style.borderColor = "lightgrey";
    (document.getElementById('password') as HTMLInputElement).style.borderColor = "lightgrey";

 this.teacherName = null;
 this.teacherQualification = null;
 this.teacherSkill= null;
 this.phoneNumber= null;
 this.teacherExperience= null;
 this.teacherPassword = null;
 this.disableButton = false;


  }

  phoneNoError : boolean = false;
  nameError : boolean = false;
  passwordError : boolean = false;
  registerRestrictedTeacher(teacherName, teacherQualification, teacherSkill, phoneNumber, teacherExperience, teacherPassword) {
    this.loading = true;
    this.disableButton = true;
    this.nameError = false;
    this.phoneNoError = false;
    this.passwordError = false;
    if (teacherName == undefined || teacherName == "") {
      (document.getElementById('teacherName') as HTMLInputElement).style.borderColor = "red";
      this.disableButton = false;
      this.loading = false;
      this.nameError = true;
    }else{
      (document.getElementById('teacherName') as HTMLInputElement).style.borderColor = "";
      this.nameError = false;
    }
    if (phoneNumber == undefined || phoneNumber == "") {
      (document.getElementById('phoneNumber') as HTMLInputElement).style.borderColor = "red";
      this.disableButton = false;
      this.loading = false;
      this.phoneNoError = true;
    }else{
      (document.getElementById('phoneNumber') as HTMLInputElement).style.borderColor = "";
      this.phoneNoError =false;
    }
    if (teacherPassword == undefined || teacherPassword == "") {
      (document.getElementById('password') as HTMLInputElement).style.borderColor = "red";
      this.disableButton = false;
      this.loading = false;
      this.passwordError = true;
    }
    else {
      (document.getElementById('password') as HTMLInputElement).style.borderColor = "";
      this.passwordError = false;
      let updatedSubjects = [];
      var subjects = [];
      var classes = [];
      var boards = [];
      
      this.selectedSubjectsToUpdate.sort((x, y) => {
        return this.comservice.sortCharArray(x.subjectName, y.subjectName); 
    });
      for (let i = 0; i < this.selectedSubjectsToUpdate.length; i++) {
        let sub = this.selectedSubjectsToUpdate[i].subjectId;
        updatedSubjects.push({ "subject": sub });

        subjects.push({"subjectInfo":this.selectedSubjectsToUpdate[i]});
      }

      this.selectedClassesToUpdate.sort((x, y) => {
        return this.comservice.compareNumberAndChar(x.className, y.className)
      })
      let updatedClasses = [];
      for (let i = 0; i < this.selectedClassesToUpdate.length; i++) {
        let cls = this.selectedClassesToUpdate[i].classId;
        updatedClasses.push({ "classID": cls });

        classes.push({"classInfo": this.selectedClassesToUpdate[i]});
      }

      let updatedBoards = [];
      for (let i = 0; i < this.selectedBoardsToUpdate.length; i++) {
        let brd = this.selectedBoardsToUpdate[i].boardId;
        updatedBoards.push({ "boardInformation": brd });

        boards.push({"boardInfo": this.selectedBoardsToUpdate[i]});
      }
      this.lpservice.registerRestrictedTeacher(this.comservice.myUserId, teacherName, teacherQualification,
        teacherSkill, phoneNumber, teacherExperience, updatedSubjects, updatedClasses, teacherPassword,updatedBoards).subscribe(res => {
          this.loading = false;
          this.disableButton = false;
          if (res.status == "success") {
            var closeRegModalBtn = document.getElementById('closeRegisterModal');
            closeRegModalBtn.click();
            var teacherJson = {"teacherInfo": {"teacherName": teacherName,"teacherId": res.teacherId, "phoneNo":phoneNumber, "qualifications": teacherQualification,
            "teacherSkills" : teacherSkill,"experience": teacherExperience, "subjectInfo" : subjects,  "classInfo" : classes,"boardInfo":boards}}
            if(this.displayInfoMyAffiliation == undefined){
              this.displayInfoMyAffiliation = [];
            }
           var a = this.displayInfoMyAffiliation;
            this.displayInfoMyAffiliation.push(teacherJson); 
              this.isTeacherUpdated.emit(this.displayInfoMyAffiliation);
              this.teacherEvent();
            Swal.fire({
              backdrop:false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: `${res.message}`,
              timer: 2000

            })
            this.clearModal();
          }
          else {
            Swal.fire({
              backdrop:false,
              heightAuto: false,
              type: 'error',
              title: 'Error!',
              text: `${res.message}`,
              timer: 2000
            })
          }
        })
    }
   // this.loading = false;
    //this.disableButton = false;
  }

  clearModal() {
    this.teacherName = "";
    this.teacherQualification = "";
    this.teacherSkill = "";
    this.phoneNumber = "";
    this.teacherExperience = null;
    this.selectedClassesToUpdate = [];
    this.selectedSubjectsToUpdate = [];
    this.teacherPassword = "";
    this.disableButton = false;
  }

  updatePhoneNoError : boolean = false;
  updateNameError : boolean = false;
  updateRestrictedTeacher(updateTeacherInfo, updatedTeacherPassword) {
    this.loading = true;
    this.disableButton = true;
    this.updateNameError = false;
    this.updatePhoneNoError = false
 
    if (updateTeacherInfo.teacherName == undefined || updateTeacherInfo.teacherName == "") {
      (document.getElementById('updateTeacherName') as HTMLInputElement).style.borderColor = "red";
      this.updateNameError = true;
      this.loading = false;
    }else{
      (document.getElementById('updateTeacherName') as HTMLInputElement).style.borderColor = "";
      this.updateNameError = false;
    }
    if (updateTeacherInfo.phoneNo == undefined || updateTeacherInfo.phoneNo == "") {
      (document.getElementById('updatePhoneNo') as HTMLInputElement).style.borderColor = "red";
      this.updatePhoneNoError = true;
      this.loading = false;
    }
    else {
      (document.getElementById('updatePhoneNo') as HTMLInputElement).style.borderColor = "";
      this.updatePhoneNoError = false;
    }

    if(updateTeacherInfo.teacherName != "" && updateTeacherInfo.phoneNo != undefined){
      let updatedSubjects1 = [];
      var subjects = [];
      this.selectedSubjectsToUpdate1.sort((x, y) => {
        return this.comservice.sortCharArray(x.subjectName, y.subjectName); 
    });
      for (let i = 0; i < this.selectedSubjectsToUpdate1.length; i++) {
        let sub = this.selectedSubjectsToUpdate1[i].subjectId;
        updatedSubjects1.push({ "subject": sub });
      subjects.push({"subjectInfo": this.selectedSubjectsToUpdate1[i]});
      }
      
      updateTeacherInfo.subjectInfo = subjects;

      var classes = [];
      let updatedClasses1 = [];
      this.selectedClassesToUpdate1.sort((x, y) => {
        return this.comservice.compareNumberAndChar(x.className, y.className)
      })
      for (let i = 0; i < this.selectedClassesToUpdate1.length; i++) {
        let cls = this.selectedClassesToUpdate1[i].classId;
        updatedClasses1.push({ "classID": cls });
        classes.push({"classInfo": this.selectedClassesToUpdate1[i]});
      }
     
      updateTeacherInfo.classInfo = classes;

      var boards = [];
      let updatedBoards1 = [];
      this.selectedBoardsToUpdate1.sort((x, y) => {
        return this.comservice.compareNumberAndChar(x.boardName, y.boardName)
      })
      for (let i = 0; i < this.selectedBoardsToUpdate1.length; i++) {
        let brd = this.selectedBoardsToUpdate1[i].boardId;
        updatedBoards1.push({ "boardInformation": brd });
        boards.push({"boardInfo": this.selectedBoardsToUpdate1[i]});
      }
     
      updateTeacherInfo.boardInfo = boards;

      if (!updatedTeacherPassword || updatedTeacherPassword == undefined) {
        updatedTeacherPassword = "";
      }

      this.lpservice.updateRestrictedTeacher(this.comservice.myUserId, this.teacherId, updateTeacherInfo.teacherName, updateTeacherInfo.qualifications,
        updateTeacherInfo.teacherSkills, updateTeacherInfo.phoneNo, updateTeacherInfo.experience, updatedSubjects1, updatedClasses1, updatedTeacherPassword,
        updatedBoards1).subscribe(res => {
          this.loading = false;
          this.disableButton = false;
          if (res.status == "success") {
            var closeUpdateModalBtn = document.getElementById('closeUpdateModal');
            closeUpdateModalBtn.click()
            //this.getaffiliationdata();
            this.isTeacherUpdated.emit(this.displayInfoMyAffiliation);
            this.teacherEvent();
            Swal.fire({
              backdrop:false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: `${res.message}`,
              timer: 2000
            })
          }
          else {
            Swal.fire({
              backdrop:false,
              heightAuto: false,
              type: 'error',
              title: 'Error!',
              text: `${res.message}`,
              timer: 2000
            })
          }
        })
    }
    this.disableButton = false;
  }

  deleteRestrictedRequest(ccId, teacherId) {
    Swal.fire({
      backdrop:false,
      heightAuto: false,
      type: 'info',
      title: 'Are you sure you want to delete the teacher?',
      text: "Deleting the teacher will delete all the courses by this teacher",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete teacher'
    }).then((result) => {
      if (result.value) {
        this.loading = true;
        this.lpservice.deleteRestrictedRequest(ccId, teacherId).subscribe((data) => {
          this.loading = false;
          if (data.status == "success") {
            this.isTeacherDeleted = true
            this.teacherDeleted(true);
            //this.isDelete.emit(this.isTeacherDeleted);
            Swal.fire({
              backdrop:false,
              heightAuto: false,
              type: 'success',
              title: 'Success!',
              text: `${data.message}`,
              timer: 2000
            });
            this.getaffiliationdata()
          }
          else {
            this.isTeacherDeleted = false
            this.teacherDeleted(false);
            //this.isDelete.emit(this.isTeacherDeleted);
            Swal.fire({
              backdrop:false,
              heightAuto: false,
              type: 'error',
              title: 'Error!',
              text: `${data.message}`,
              timer: 2000
            })
          }
        });
      }
    })

  }

  setTeacherId(teacherId, teacherName) {
    this.teacherId = teacherId;
    this.showTable = false;
    this.teacherName = teacherName;
    this.scheduleClassInfo = [] ;
    this.startDate = undefined;
    this.endDate = undefined;
    this.showPrint = false;

  }

  teacherDetails : any
teacherCourseDetails : any
studentPaymentDetails : any
//teacherIdForReport : number


currentTime: any;
  //showTable:any;
  totalCounts: any;
  getTeacherReport(startDate, endDate) {
    this.loadingReport = true;
    this.showTable = false;
    if (startDate != undefined && endDate != undefined) {
      var start = new Date(startDate).toISOString().slice(0, 10);
      var end = new Date(endDate).toISOString().slice(0, 10);
      this.lpservice.getTeacherReport(this.teacherId, start, end).subscribe((data) => {
        if (data.status == "success") {
          this.showTable = true;
          this.scheduleClassInfo = data.scheduleClassInfo;
          this.showPrint = true;
          this.totalCounts = data;
          this.loadingReport = false;
        }
        else {
          this.showTable = false;
          this.scheduleClassInfo = data.message;
          this.loadingReport = false;
        }
      }, (err) => {
        this.loading = false;
        Swal.fire({
          backdrop:false,
            heightAuto: false,
          type: 'error',
          title: 'Error!',
          text: "Server side error",
          timer: 2000
        })
       // this.editable = false;
      });
    } 
    else {
      this.loading = false;
      this.showTable = false;
      this.scheduleClassInfo = "Please select a valid start date and end date to get report of teacher";
      this.loadingReport = false;
    }
  }
  arraySort(arr){
return arr.sort();
  }

  printReport(){
    window.print();
  }
  teacherSubjectInfo: any;
  getTeacherDetails(teacherId){
    this.teacherId = teacherId;
    this.lpservice.exploreTeacherDetails(this.teacherId).subscribe(res => {
  this.teacherDetails = res.teacherDetails;
  this.teacherCourseDetails = res.teacherDetails.teacherCoursesInfo;
  res.teacherDetails.teacherClassInfo.sort((x, y) => {
    return this.comservice.compareNumberAndChar(x.classInfo.className, y.classInfo.className)
  })
  res.teacherDetails.teacherSubjectInfo.sort((x, y) => {
    return this.comservice.sortCharArray(x.subjectName, y.subjectName); 
});
  this.teacherClassInfo = res.teacherDetails.teacherClassInfo;
  this.teacherSubjectInfo = res.teacherDetails.teacherSubjectInfo;
  this.startDate = undefined;
  this.endDate = undefined;
    });
  
    this.showTable = false;
    this.scheduleClassInfo = [] ;
    this.startDate = undefined;
    this.endDate = undefined;
    this.showPrint = false;
  
  }
  
  backToMyTeachers(){
    this.teacherDetails = null
    this.scheduleClassInfo = null
    this.virtualClassInfo = [];
  }
  filteredTeacherData: any
  showNoResults: boolean;
  searchTeacherByName(){
    this.filteredTeacherData = [];
    //this.displayInfoMyAffiliation = true;
    this.showNoResults = false;
    var input = document.getElementById("searchTeacher") as HTMLInputElement;
    var search = input.value.toUpperCase();
    for (var i = 0; i < this.displayInfoMyAffiliation.length; i++) {
         var a = this.displayInfoMyAffiliation[i].teacherInfo.teacherName;
        if (a.toUpperCase().indexOf(search) > -1) {
          this.showNoResults = false;
          this.filteredTeacherData.push(this.displayInfoMyAffiliation[i]);
        } 
      
    }
    if(this.filteredTeacherData.length == 0){
      this.showNoResults = true;
    }
    }

    
  
   
  

    endOngoingMeetingFromTeacherReport(scheduleClassId){
      Swal.fire({
        backdrop: false,
        heightAuto: false,
        type: 'info',
        title: 'Are you sure you want to end this meeting?',
        text: "Ending the meeting from here will immediately end the ongoing live class for all students and teacher",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, end meeting'
      }).then((result) => {
        if (result.value) {
      this.lpservice.endMeetingForcefully(scheduleClassId).subscribe(res => {
        (document.getElementById("endMeeting")as HTMLInputElement).style.display = "none";
      });
    }
  })
    }

}



