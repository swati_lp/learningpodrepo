import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';
import { Course } from './shared/models/course.model';
import { NgxStarsModule } from 'ngx-stars';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { MessagingService } from './shared/models/messaging.service';
import { AsyncPipe } from '@angular/common';
import { environment } from '../environments/environment';
import * as Firebase from 'firebase';
import { IntlModule } from '@progress/kendo-angular-intl';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { ChartsModule } from 'ng2-charts';
//import { ServiceWorkerModule } from '@angular/service-worker';
import { AuthGuardGuard } from './guards/auth-guard.guard';
//import { RecordingComponent } from './components/recording/recording.component';
import { InvoiceComponent } from './components/invoice/invoice.component';
import { Receipt } from './shared/models/receipt.model';
import { MyStudentsComponent } from './components/my-students/my-students.component';
//import { ScheduleTestComponent } from './schedule-test/schedule-test.component';

import { CommonModule } from '@angular/common';

const appRoutes: Routes = [

//{path : 'recording' , component : RecordingComponent},

  {
    path: '', children: [
      {
        path: '',
        loadChildren: './modules/not-loggedin/not-loggedin.module#NotLoggedinModule'
      }
    ]
  },

  {
    path: 'home',
    canActivate: [AuthGuardGuard]
    , children: [
      {
        path: '',
        loadChildren: './modules/loggedin/loggedin.module#LoggedinModule'
      }
    ]
  },
  //{ path: 'help', component: HelpComponent },
];
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
   // HelpComponent,
    //HelpComponent,
   // ScheduleTestComponent,
  
  //  RecordingComponent,
  
  
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    FormsModule,
    RouterModule.forRoot(appRoutes, { useHash: true ,anchorScrolling: 'enabled',}),
    BrowserModule,
    NgMultiSelectDropDownModule.forRoot(),
    FormsModule, HttpClientModule, ReactiveFormsModule,
    NgbModalModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    BrowserAnimationsModule,
    NgxPaginationModule,
    NgxStarsModule,
    HttpClientModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
    IntlModule, DateInputsModule,
    ChartsModule,
 //   ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],

  providers: [
    MessagingService, AsyncPipe,
    HttpClientModule, NavbarComponent, Course, InvoiceComponent, Receipt, MyStudentsComponent
    
  ],
  bootstrap: [AppComponent],

})
export class AppModule { }
