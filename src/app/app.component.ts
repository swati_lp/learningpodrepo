import { Component, ElementRef, AfterViewInit } from '@angular/core';
import { CommonServiceService } from "./common-service.service";
import { Observable, of } from "rxjs";
import { Router } from '@angular/router';
import { MessagingService } from "./shared/models/messaging.service";
import { LearningpodService } from './learningpod.service';
declare var jQuery: any;
import Swal from 'sweetalert2';
//import * as $ from 'jquery'
//import 'jqueryui';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  //public webcamImage: WebcamImage = null;
  title = 'LearningPod';
  scId: any;
  joinmeetingurl: any;
  courseTitle: any;
  showMissedClassDiv: boolean;
//  showNavbar: boolean;

  constructor(
    private lpservice: LearningpodService,
    public messagingService: MessagingService,
    private elementRef: ElementRef, public comservice: CommonServiceService, private router: Router) {
  }
  ngOnInit() {
    window.addEventListener('scroll', this.scrollEvent, true);
    jQuery(document).ready(function ($) {
      $(document).on('click', '.min', function () {
        $(this).closest('.modal').find('#myFrame').slideUp(0.5);
        $(this).closest('.modal').find('.header').removeClass('hide');
      });

      $(document).on('click', '.max', function () {
      
        $(this).closest('.modal').find('#myFrame').slideDown(0.5);
        $(this).closest('.modal').find('.header').addClass('hide')
      });



      $(document).on('click', '.close', function () {
        $(this).closest('.modal').fadeOut();
      });
    });
    if (localStorage.getItem("FCMtoken") != null || localStorage.getItem("FCMtoken") != undefined) {
      this.messagingService.receiveMessage();
    }
  }


  openFullscreen() {
    var elem = document.getElementById("myFrame");
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    }
  }





  joinMeeting() {
    this.scId = this.messagingService.scId;
    this.courseTitle = this.messagingService.courseTitle;
    this.lpservice.joinMeeting(this.scId, this.courseTitle, this.comservice.myUserName, this.comservice.myUserRole).subscribe(res => {
     
  
      if (res.status == "success") {
        this.joinmeetingurl = res.completeUrl;
        window.open(res.completeUrl, "_blank");
      
        
      }
      else {
 
        Swal.fire({
          title: 'Warning',
          text: `${res.message}`,
          type: 'warning',
          showCancelButton: false,
          confirmButtonColor: '#3085d6',
     
          confirmButtonText: 'Got it'
        }).then((result) => {
          if (result.value) {
            document.getElementById('closeModal').click();
          }
        })
      }

    });
  }
  scrollEvent = (event: any): void => {
      
    var scroll = document.getElementById("scrollDivTeacher").scrollTop;
    //var scroll = document.getElementById("scrollDivTeacherDetails").scrollTop;
    if (scroll > 10) {
     // mybutton.style.display = "block";
      $("#scrollBtnTeacher").fadeIn();
    } else {
      $("#scrollBtnTeacher").fadeOut();
    }
  }
  topFunction() {
    document.getElementById("scrollDivTeacher").scrollTop = 0;
   // document.getElementById("scrollDivTeacherDetails").scrollTop = 0;
  }
  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'white';
  }
}
