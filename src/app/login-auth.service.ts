import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CommonServiceService } from './common-service.service';

@Injectable({
  providedIn: 'root'
})
export class LoginAuthService implements CanActivate {

  constructor(private comservice: CommonServiceService, public router: Router) { }
  canActivate(
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.comservice.myUserId == undefined || this.comservice.myUserRole == undefined) {
      this.router.navigateByUrl("/landing");
      return false;
    }
    else {
      return true;
    }
  }
}
