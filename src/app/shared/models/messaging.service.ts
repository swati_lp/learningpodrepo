import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs'
import { CommonServiceService } from '../../common-service.service';
import { LearningpodService } from '../../learningpod.service';
import { FCMNotification } from './fcmNotification.model';
import { Router } from '@angular/router';
//import 'rxjs/add/observable/of';
import * as Firebase from 'firebase';
//import { AppComponent } from './app.component';

@Injectable()
export class MessagingService {
  notifications = [];
  remainTimeInSec: any;
  totalClassTimeInSec: any;
  //fcmToken: any;
  //joinButton: boolean;
  scId: any;
  courseTitle: any;
  showMissedClassDiv: any;
  generateFcmCounter: number = 0;
  constructor(
    private angularFireDB: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private angularFireMessaging: AngularFireMessaging,
    public comservice: CommonServiceService,
    private lpservice: LearningpodService,
    public router: Router
  ) {
    this.angularFireMessaging.messaging.subscribe(
      (_messaging) => {
        _messaging.onMessage = _messaging.onMessage.bind(_messaging);
        _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);

      }
    );
  }

  /**
   * update token in firebase database
   * 
   * @param userId userId as a key 
   * @param token token as a value
   */
  updateToken(userId, token) {
    // we can change this function to request our backend service
    this.angularFireAuth.authState.pipe(take(1)).subscribe(
      () => {
        const data = {};
        data[userId] = token
        this.angularFireDB.object('fcmTokens/').update(data)
      })
  }

  /**
   * request permission for notification from firebase cloud messaging
   * 
   * @param userId userId
   */
  requestPermission(userId) {
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        if(token){
        console.log(token);
        // store token in ls
        localStorage.setItem("FCMtoken", token);
        // this.lpservice.getUserDetails(this.comservice.myUserRole, this.comservice.myUserId, token).subscribe(res => {
        // },
        // );
        this.updateToken(userId, token);

        if (this.comservice.isUserLoggedIn()) {
          this.userdetails(this.comservice.myUserRole, this.comservice.myUserId, token);
        }
       
      }
      },
      (err) => {
         console.error('Unable to get permission to notify.', err);
        if(this.generateFcmCounter < 1){
        const userId = 'user' + this.comservice.myUserId;
        this.requestPermission(userId);
        this.generateFcmCounter ++ ;
      }
      else{
        localStorage.setItem("FCMtoken", "fq2PLAq-V7c:APA91bGLIz2YRiL2uxqLU0R66DAlhBzrCS5PiswN2gXB3czNSqANE8wVEMfLhrZaP73PZmakOS-SogURGsYU6bGWv_BfrIK7VLZ2hUO8gypXgzpR6U71o7ZaYTXGiXAKjxGo2Zq7Waxx");
      }
    }
    );
  }

  userdetails(myUserRole, myUserId, fcmToken) {
    this.lpservice.userDetails(myUserRole, myUserId, fcmToken).subscribe(res => {

    })

  }

  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload) => {
        let title: string = payload.data.title;
        let scId: string = payload.data.scId;
        let body: string = payload.data.body;
        let remainTime: string = payload.data.remainTime;
        let totalClassTime: string = payload.data.totalClassTime;
        let courseTitle: string = payload.data.courseTitle;
        this.scId = scId;
        this.courseTitle = courseTitle;
        //this.joinButton = true;
        this.showMissedClassDiv = true;

        let obj = new FCMNotification(scId, title, body, remainTime, totalClassTime, courseTitle, true);
        this.notifications.push(obj);
        this.remainTimeInSec = parseInt(obj.remainTime) * 60 * 1000;
        this.totalClassTimeInSec = parseInt(obj.totalClassTime) * 60 * 1000;

        setTimeout(() => {
          //this.joinButton = true;
          obj.body = "Your " + obj.courseTitle + " class has started";
          this.classStarted(obj);
        }, this.remainTimeInSec);  //x min timer
      });
  }

  classStarted(myObj) {

    setTimeout(() => {

      if (this.showMissedClassDiv) {
        //this.joinButton = false;
        myObj.body = "You missed your " + myObj.courseTitle + " class";
        myObj.isJoinEnabled = false;
      }
      this.classMissed();
    }, this.totalClassTimeInSec);  //y min timer         



  }

  classMissed() {
    setTimeout(() => {
      var a = this.notifications.shift();
      //this.notifications.splice(this.notifications.findIndex(e=>e.scId === myObj.scId));
    }, 300000);  //hc min timer  
    // myObj = [];

  }

}