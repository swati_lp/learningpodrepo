
export class FCMNotification {

  scId: string;
  title: string;
  body: string;
  remainTime: string;
  totalClassTime: string;
  courseTitle: string;
  isJoinEnabled: boolean;

  constructor(scId: string, title: string, body: string, remainTime: string, totalClassTime: string, courseTitle: string, isJoinEnabled: boolean) {

    this.scId = scId;
    this.title = title;
    this.body = body;
    this.remainTime = remainTime;
    this.totalClassTime = totalClassTime;
    this.courseTitle = courseTitle;
    this.isJoinEnabled = isJoinEnabled;


  }

}