import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Course } from './shared/models/course.model';
//import { FilterTeacherData } from './filter-teacher-data';

//import { FilterCcData } from './filter-cc-data';
import { CommonServiceService } from './common-service.service';
import { environment } from '../environments/environment';
//import { getDate } from 'date-fns';
//import undefined = require('firebase/empty-import');


@Injectable({
  providedIn: 'root'
})
export class LearningpodService {
  url: any;
  authHeaders: any;
  loginHeaders: any;

  constructor(private comservice: CommonServiceService, private http: HttpClient) {
    this.url = environment.url;
    this.setheaders();
  }

  setheaders() {
    this.authHeaders =
      new HttpHeaders({
        'Content-Type': 'application/json',
        'authorization': localStorage.getItem('access_token'),

      });
    this.loginHeaders =
      new HttpHeaders({
        'Content-Type': 'text/plain'
      });
  }

  signUp(phone, name, role): Observable<any> {
    return this.http.post(this.url + "/newuser/register", {
      "phoneNo": phone,
      "name": name,
      "role": role,
      //"testFlag": true
    });
  }
  logout(myUserId): Observable<any> {
    return this.http.get(this.url + "/logoutClient?" + "userId=" + myUserId,
      { headers: this.authHeaders });
  }

  getRecording(userRole, userId): Observable<any> {
    return this.http.get(this.url + "/liveClass/video/" + userRole + "/" + userId,
      { headers: this.authHeaders });
  }
  userDetails(role, id, fcmToken): Observable<any> {
    return this.http.get(this.url + "/userDetails/" + role + "/" + id + "/" + fcmToken,
      { headers: { 'authorization': localStorage.getItem('access_token') } });
  }



  payInCashCc(courseId, ccId, studentId, teacherId): Observable<any> {
    return this.http.post(this.url + "/enroll/course-student/request", {
      "courseId": courseId,
      "studentId": studentId,
      "teacherId": teacherId,
      "ccId": ccId
    },
      { headers: this.authHeaders });
  }

  validateAccessCode(phone, name, role, pass, otp): Observable<any> {
    return this.http.post(this.url + "/newuser/validate", {
      "phoneNo": phone,
      "name": name,
      "role": role,
      "password": pass,
      "otp": otp
    });
  }

  login(phoneNo, password, role, latitude, longitude): Observable<any> {
    return this.http.request<any>("POST", this.url + "/login", {
      body: {
        "username": phoneNo,
        "role": role,
        "password": password,
        "appName": navigator.appName,
        "appVersion": navigator.appVersion,
        "userPlatform": navigator.platform,
        "language": navigator.language,
        "userLat": latitude,
        "userLong": longitude,
        "userIpAddress": 10
      },
      headers: this.loginHeaders,
      observe: 'response'
    });
  }

  updateCc(ccInfoToUpdate: any, subjectId: any, classId: any, updatedBoards): Observable<any> {
    return this.http.put(this.url + "/cc/managed-cc/" + this.comservice.myUserId, {
      "ccName": ccInfoToUpdate.ccName,
      "picturePath": ccInfoToUpdate.picturePath,
      "description": ccInfoToUpdate.description,
      "address1": ccInfoToUpdate.address1,
      "address2": ccInfoToUpdate.address2,
      "pincode": ccInfoToUpdate.pincode,
      "city": ccInfoToUpdate.city,
      "state": ccInfoToUpdate.state,
      "email": ccInfoToUpdate.ccEmail,
      "numberOfTeacher": ccInfoToUpdate.numberOfTeacher,
      "numberOfStudent": ccInfoToUpdate.numberOfStudent,
      "userId": this.comservice.myUserId,
      // "phoneNo": ccInfoToUpdate.phoneNo,
      "subject": subjectId,
      "classDetail": classId,
      "cumulativeRating": ccInfoToUpdate.cumulativeRating,
      "ccBoardInformations": updatedBoards
    }, { headers: this.authHeaders });
  }

  updateStudent(studentInfoToUpdate: any, newClassId: any): Observable<any> {
    return this.http.put(this.url + "/student/managed-student/" + this.comservice.myUserId, {
      "studentName": studentInfoToUpdate.studentName,
      "stduentEmail": studentInfoToUpdate.email,
      "classId": newClassId,
      "isActive": studentInfoToUpdate.isActive,
      "address1": studentInfoToUpdate.address1,
      "address2": studentInfoToUpdate.address2,
      "pincode": studentInfoToUpdate.pincode,
      "city": studentInfoToUpdate.city,
      "state": studentInfoToUpdate.state,
      "picturePath": studentInfoToUpdate.picturePath,
      "ccInformation": this.comservice.ccId,
    }, { headers: this.authHeaders });
  }

  updateTeacher(teacherInfoToUpdate: any, classIds: any, subjectIds: any, boardId): Observable<any> {
    return this.http.put(this.url + "/teacher/managed-teacher/" + this.comservice.myUserId, {
      "description": teacherInfoToUpdate.description,
      "qualifications": teacherInfoToUpdate.qualifications,
      "experience": teacherInfoToUpdate.experience,
      "lpExpeience": teacherInfoToUpdate.lpExpeience,
      "teacherEmail": teacherInfoToUpdate.email,
      "address1": teacherInfoToUpdate.address1,
      "address2": teacherInfoToUpdate.address2,
      "pincode": teacherInfoToUpdate.pincode,
      "city": teacherInfoToUpdate.city,
      "state": teacherInfoToUpdate.state,
      "teacherName": teacherInfoToUpdate.teacherName,
      "teacherGender": teacherInfoToUpdate.teacherGender,
      "teacherBirthdate": teacherInfoToUpdate.teacherBirthdate,
      "teacherSkills": teacherInfoToUpdate.teacherSkills,
      // "user":[{"phoneNo": teacherInfoToUpdate.phoneNo}],
      'subject': subjectIds,
      'classID': classIds,
      'teacherBoardInformations': boardId,
      'cumulativeRating': teacherInfoToUpdate.cumulativeRating,
    }, { headers: this.authHeaders });
  }



  getEnrollNotifications(): Observable<any> {
    return this.http.get(this.url + "/enroll/getRequests/" + this.comservice.myUserRole + "/" + this.comservice.myUserId + "/initiated",
      { headers: this.authHeaders });
  }

  getEnquiryNotifications(): Observable<any> {
    return this.http.get(this.url + "/course/request/" + this.comservice.myUserId,
      { headers: this.authHeaders });
  }

  rejectRequestNotification(requestId): Observable<any> {
    return this.http.put(this.url + "/enroll/updateRequest", {
      "requestId": requestId,
      "requestStatus": "Reject"
    },
      { headers: this.authHeaders });
  }

  acceptRequestNotification(requestId): Observable<any> {
    return this.http.put(this.url + "/enroll/updateRequest", {
      "requestId": requestId,
      "requestStatus": "Approve"
    },
      { headers: this.authHeaders });
  }

  getUserDetails(role, id): Observable<any> {
    return this.http.get(this.url + "/user/managed-user/" + role + "/" + id,
      { headers: this.authHeaders });
  }

  deleteStudent(id): Observable<any> {
    return this.http.request<any>("DELETE",
      this.url + "/student/ghost/" + id,
      { headers: this.authHeaders }
    );
  }
  deleteStudentEnrollment(courseId, studentId): Observable<any> {
    return this.http.request<any>("DELETE",
      this.url + "/enroll/course-student/" + courseId + "/" + studentId,
      { headers: this.authHeaders }
    );
  }

  deleteRestrictedRequest(ccId, teacherId): Observable<any> {
    return this.http.delete(this.url + "/cc/teacher/delete/" + ccId + "/" + teacherId, { headers: this.authHeaders });
  }


  getAffiliatedList(role, id): Observable<any> {
    return this.http.get(this.url + "/affilliation/approved/" + role + "/" + id,
      { headers: this.authHeaders });
  }
  getAffiliatedStudent(ccId, pageNo, pageSize): Observable<any> {
    // /test/cc/student/20?pageNo=2&pageSize=5
    return this.http.get(this.url + "/cc/student/" + ccId + "?pageNo=" + pageNo + "&pageSize=" + pageSize,
      { headers: this.authHeaders });
  }
  getStudentList(courseId): Observable<any> {
    return this.http.get(this.url + "/cc/student/" + this.comservice.ccId + "/" + courseId,
      { headers: this.authHeaders });
  }

  getallClasses(): Observable<Object> {
    var response: Observable<Object> = this.http.get(this.url + "/master/class");
    return response;
  }

  getallSubjects(): Observable<Object> {
    var response: Observable<Object> = this.http.get(this.url + "/master/subject");
    return response;
  }
  getBoards(): Observable<any> {
    return this.http.get(this.url + "/board",
      { headers: this.authHeaders });
  }
  enrollStudentList(courseId, studentId) {
    return this.http.request<any>("POST",
      this.url + "/enroll/course/student",
      {
        body: {
          "courseId": courseId,
          "studentId": studentId
        }, headers: this.authHeaders
      });
  }




  updateCourse(courseToUpdate: Course, courseFrequencyArr: any): Observable<any> {
    return this.http.request<any>("PUT",
      this.url + "/course/managed-courses/" + courseToUpdate.courseId,
      {
        body: {
          "createdByID": this.comservice.myUserId,
          "createdByRole": this.comservice.myUserRole,
          "classID": courseToUpdate.classId,
          "teacher": courseToUpdate.teacherId,
          "cc": courseToUpdate.ccId,
          "subject": courseToUpdate.subjectId,
          "startDate": courseToUpdate.startDate,
          "endDate": courseToUpdate.endDate,
          "frequency": courseFrequencyArr,
          "startTime": courseToUpdate.startTime,
          "endTime": courseToUpdate.endTime,
          "title": courseToUpdate.title,
          "description": courseToUpdate.description,
          "cost": courseToUpdate.cost,
          "doubtClearance": courseToUpdate.doubtClearance,
          "onlineForum": courseToUpdate.onlineForum,
          "periodicTests": courseToUpdate.periodicTests,
          "studyMaterial": courseToUpdate.studyMaterial,
          "availableOnPhone": courseToUpdate.availableOnPhone,
          "maxStudent": courseToUpdate.maxStudent,
          "homework": courseToUpdate.homework,
          "cumulativeRating": courseToUpdate.cumulativeRating,
          "boardInformation": parseInt(courseToUpdate.boardId)
        }, headers: this.authHeaders
      });
  }
  updateManualCourse(courseToUpdate: Course, courseFrequencyArr: any): Observable<any> {
    return this.http.request<any>("PUT",
      this.url + "/course/manualClass/" + courseToUpdate.courseId,
      {
        body: {
          "createdByID": this.comservice.myUserId,
          "createdByRole": this.comservice.myUserRole,
          "classID": courseToUpdate.classId,
          "teacher": courseToUpdate.teacherId,
          "cc": courseToUpdate.ccId,
          "subject": courseToUpdate.subjectId,
          "startDate": courseToUpdate.startDate,
          "endDate": courseToUpdate.endDate,
          "frequency": courseFrequencyArr,
          "startTime": courseToUpdate.startTime,
          "endTime": courseToUpdate.endTime,
          "title": courseToUpdate.title,
          "description": courseToUpdate.description,
          "cost": courseToUpdate.cost,
          "doubtClearance": courseToUpdate.doubtClearance,
          "onlineForum": courseToUpdate.onlineForum,
          "periodicTests": courseToUpdate.periodicTests,
          "studyMaterial": courseToUpdate.studyMaterial,
          "availableOnPhone": courseToUpdate.availableOnPhone,
          "maxStudent": courseToUpdate.maxStudent,
          "homework": courseToUpdate.homework,
          "cumulativeRating": courseToUpdate.cumulativeRating,
          "boardInformation": parseInt(courseToUpdate.boardId)
        }, headers: this.authHeaders
      });
  }

  getMyCourses(role, id): Observable<any> {
    return this.http.get(this.url + "/course/by/" + role + "/" + id,
      { headers: this.authHeaders });
  }


  addCourseNew(courseToAdd: Course) {
    return this.http.request<any>("POST",
      this.url + "/course/managed-courses",
      {
        body: {
          "createdByID": this.comservice.myUserId,
          "createdByRole": this.comservice.myUserRole,
          "classID": courseToAdd.classId,
          "teacher": courseToAdd.teacherId,
          "cc": courseToAdd.ccId,
          "subject": courseToAdd.subjectId,
          "startDate": courseToAdd.startDate,
          "endDate": courseToAdd.endDate,
          "frequency": courseToAdd.frequency,
          "startTime": courseToAdd.startTime,
          "endTime": courseToAdd.endTime,
          "title": courseToAdd.title,
          "description": courseToAdd.description,
          "cost": courseToAdd.cost,
          "doubtClearance": courseToAdd.doubtClearance,
          "onlineForum": courseToAdd.onlineForum,
          "periodicTests": courseToAdd.periodicTests,
          "studyMaterial": courseToAdd.studyMaterial,
          "availableOnPhone": courseToAdd.availableOnPhone,
          "maxStudent": courseToAdd.maxStudent,
          "homework": courseToAdd.homework,
          "boardInformation": courseToAdd.boardId
        }, headers: this.authHeaders
      });
  }

  getscheduledclass(startDate: string, endDate: string): Promise<any> {
    let id = this.comservice.myUserId;
    let role = this.comservice.myUserRole;
    return this.http.get(this.url + "/sc-class/find/" + id + "/" + role + "/" + startDate + "/" + endDate,
      { headers: this.authHeaders }).toPromise()
      .then(result => result)
      .catch(error => error);
  }

  updateScheduledClass(date, startTime, endTime, scId, topic, teacherId, courseId): Observable<any> {
    return this.http.put(this.url + "/sc-class/" + scId, {
      "date": date,
      "startTime": startTime,
      "endTime": endTime,
      "topic": topic,
      "teacherInformation": parseInt(teacherId),
      "course": courseId
    }, { headers: this.authHeaders });
  }

  addScheduledClass(courseId, addTeacherId, topic, date, startTime, endTime): Observable<any> {
    return this.http.post(this.url + "/sc-class/create", {
      "scDate": date,
      "startTime": startTime,
      "endTime": endTime,
      "courseId": courseId,
      "teacherId": addTeacherId,
      "topic": topic
    }, { headers: this.authHeaders });
  }



  enquireRequestTeacherCc(courseId, studentId, ccId, teacherId): Observable<any> {
    return this.http.request<any>("PUT", this.url + "/course/request/" + courseId + "/" + studentId + "?"
      + "ccId=" + ccId + "&teacherId=" + teacherId,
      { headers: this.authHeaders });
  }
  joinMeeting(id, title, userName, userRole): Observable<any> {
    let meetingCreateUrl = "";



    meetingCreateUrl += this.url + "/liveClass/join?scId=" + id + "&courseTitle=" + title + "&userName=" + userName + "&userRole=" + userRole + "&userId=" + this.comservice.myUserId;
    return this.http.get(meetingCreateUrl,
      { headers: this.authHeaders })
  }




  getTeachersForExplore(): Observable<any> {
    let exploreurl = this.url + "/teacher/explore?";
    return this.http.get(exploreurl);
  }

  getCoursesForExploreForStudent(studentId, ccId): Observable<any> {
    var exploreurl = this.url + "/course/explore/" + studentId + "?ccId=" + ccId;
    return this.http.get(exploreurl, { headers: this.authHeaders });
  }

  getCoursesForExplore(): Observable<any> {
    let exploreurl = this.url + "/course/explore?";
    return this.http.get(exploreurl);
  }
  uploadImage(image): Observable<any> {
    let customhearders = new HttpHeaders({
      'authorization': this.comservice.myAccessToken
    });
    const formData = new FormData();
    formData.append('image', image);
    return this.http.post(this.url + "/profilePicture/" + this.comservice.myUserId, formData,
      { headers: customhearders });
  }
  deleteFile(courseId, videoPath) {
    return this.http.request<any>("DELETE",
      this.url + "/course/video/" + courseId,
      {
        // body: {
        //   "videoPath": videoPath,
        // }, 
        headers: this.authHeaders
      }
    );
  }

  deleteImage(userId, picturePath) {
    return this.http.request<any>("DELETE",
      this.url + "/user/image/" + userId,
      {
        //  body: {
        //   "imagePath": picturePath,
        // }, 
        headers: this.authHeaders
      });
  }


  uploadFile(fileToUpload: File, courseId): Observable<any> {
    let customhearders = new HttpHeaders({
      'authorization': this.comservice.myAccessToken
    });
    const formData = new FormData();
    formData.append('video', fileToUpload);
    return this.http.post(this.url + "/course/video/" + courseId, formData,
      { headers: customhearders });
  }


  deleteNotes(courseNoteId) {
    return this.http.request<any>("DELETE",
      this.url + "/cc/note/" + courseNoteId,
      { headers: this.authHeaders }
    );
  }

  uploadStudyMaterial(fileToUpload: File, courseId): Observable<any> {
    let customhearders = new HttpHeaders({
      'authorization': this.comservice.myAccessToken
    });
    const formData = new FormData();
    formData.append('document', fileToUpload);
    return this.http.post(this.url + "/course/document/" + courseId, formData,
      { headers: customhearders });
  }

  changePassword(phone, password, newpassword, role) {
    return this.http.request<any>("POST",
      this.url + "/changePassword",
      {
        body: {
          "phone": phone,
          "password": password,
          "newpassword": newpassword,
          "userRole": role
        }, headers: this.authHeaders
      });
  }

  forgetPassword(phone, role): Observable<any> {
    return this.http.post(this.url + "/forgetPassword", {
      "phoneNo": phone,
      "userRole": role,
      "testFlag": false
    })
  };

  resetPassword(phone, access, password, role): Observable<any> {
    return this.http.post(this.url + "/resetPassword", {
      "userRole": role,
      "phoneNo": phone,
      "otp": access,
      "password": password
    })
  };

  postCourseReview(ratingToUpdateRating: string, reviewToUpdateRating: string,
    courseIdToUpdateRating: number) {
    return this.http.request<any>("POST",
      this.url + "/course/postReview",
      {
        body: {
          "review": reviewToUpdateRating,
          "studentId": this.comservice.myUserId,
          "courseId": courseIdToUpdateRating,
          "rating": ratingToUpdateRating
        }, headers: this.authHeaders
      });
  }

  postTeacherReview(ratingToUpdateRating: string, reviewToUpdateRating: string,
    teacherIdToUpdateRating: number) {
    return this.http.request<any>("POST",
      this.url + "/teacher/postReview",
      {
        body: {
          "review": reviewToUpdateRating,
          "studentId": this.comservice.myUserId,
          "teacherId": teacherIdToUpdateRating,
          "rating": ratingToUpdateRating
        }, headers: this.authHeaders
      });
  }
  postCcReview(ratingToUpdateRating: string, reviewToUpdateRating: string,
    ccIdToUpdateRating: number) {
    return this.http.request<any>("POST",
      this.url + "/cc/postReview",
      {
        body: {
          "review": reviewToUpdateRating,
          "studentId": this.comservice.myUserId,
          "ccId": ccIdToUpdateRating,
          "rating": ratingToUpdateRating
        }, headers: this.authHeaders
      });
  }
  scheduleLaterCourse(courseToAdd) {
    return this.http.request<any>("POST",
      this.url + "/course/create",
      {
        body: {
          "createdByID": this.comservice.myUserId,
          "createdByRole": this.comservice.myUserRole,
          "classID": courseToAdd.classId,
          "teacher": courseToAdd.teacherId,
          "cc": courseToAdd.ccId,
          "subject": courseToAdd.subjectId,
          "startDate": courseToAdd.startDate,
          "endDate": courseToAdd.endDate,
          "frequency": courseToAdd.frequency,
          "startTime": courseToAdd.startTime,
          "endTime": courseToAdd.endTime,
          "title": courseToAdd.title,
          "description": courseToAdd.description,
          "cost": courseToAdd.cost,
          "doubtClearance": courseToAdd.doubtClearance,
          "onlineForum": courseToAdd.onlineForum,
          "periodicTests": courseToAdd.periodicTests,
          "studyMaterial": courseToAdd.studyMaterial,
          "availableOnPhone": courseToAdd.availableOnPhone,
          "maxStudent": courseToAdd.maxStudent,
          "homework": courseToAdd.homework,
          "boardInformation": courseToAdd.boardId
        }, headers: this.authHeaders
      });
  }
  registerRestrictedStudent(ccId: number, studentName: string, phoneNumber: string,
    boardId: any, classId: any, studentPassword: string) {
    return this.http.request<any>("POST",
      this.url + "/cc/student/" + ccId,
      {
        body: {
          "studentName": studentName,
          "studentPhoneNumber": phoneNumber,
          "classId": classId,
          "studentPassword": studentPassword,
          "boardId": boardId
        }, headers: this.authHeaders
      });
  }

  registerRestrictedTeacher(ccId: number, teacherName: string,
    teacherQualification: string, teacherSkill: string, phoneNumber: string, teacherExperience: number, 
    subjectId: any, classId: any, teacherPassword: string, boardId) {
    return this.http.request<any>("POST",
      this.url + "/cc/teacher/register",
      {
        body: {
          "ccId": ccId,
          "teacherName": teacherName,
          "teacherQualification": teacherQualification,
          "teacherSkill": teacherSkill,
          "phoneNumber": phoneNumber,
          "teacherExperience": teacherExperience,
          "subjectId": subjectId,
          "classId": classId,
          "boardId":boardId,
          "teacherPassword": teacherPassword,
        }, headers: this.authHeaders
      });
  }

  paymentInitiate(cost, payable, startDateNotEnrolled, endDateNotEnrolled, courseIdNotEnrolled, studentId, Monthly) {
    return this.http.request<any>("POST",
      this.url + "/enroll/student/fee",
      {
        body: {
          "courseCost": cost,
          "payableAmount": payable,
          "startDate": startDateNotEnrolled,
          "endDate": endDateNotEnrolled,
          "studentId": studentId,
          "courseId": courseIdNotEnrolled,
          "frequency": Monthly
        }, headers: this.authHeaders
      });
  }
  studentTransaction(paymentStartDate, paymentEndDate, amountPaid, stduentFeeId, courseIdNotEnrolled, studentId, transactionDate) {
    return this.http.request<any>("POST",
      this.url + "/enroll/student/transaction",
      {
        body: {
          "paidAmount": amountPaid,
          "tStartDate": paymentStartDate,
          "tEndDate": paymentEndDate,
          "stduentCourseFeeId": stduentFeeId,
          "studentId": studentId,
          "courseId": courseIdNotEnrolled,
          "tDate": transactionDate
        }, headers: this.authHeaders
      });
  }
  updateRestrictedTeacher(ccId: number, teacherId: any, teacherName: string,
    teacherQualification: string, teacherSkill: string, phoneNumber: string, teacherExperience: number, 
    subjectId: any, classId: any, teacherPassword: string, boardId) {
    return this.http.request<any>("PUT",
      this.url + `/cc/teacher/update/${ccId}/${teacherId}`,
      {
        body: {
          "ccId": ccId,
          "teacherName": teacherName,
          "teacherQualification": teacherQualification,
          "teacherSkill": teacherSkill,
          "phoneNumber": phoneNumber,
          "teacherExperience": teacherExperience,
          "subjectId": subjectId,
          "classId": classId,
          "boardId": boardId,
          "teacherPassword": teacherPassword,
        }, headers: this.authHeaders
      });
  }

  updateFees(courseCost, payable, startDate, endDate, frequency, studentId, courseId, studentFeeId) {
    return this.http.request<any>("PUT",
      this.url + "/enroll/student/fee/" + studentFeeId,
      {
        body: {
          "courseCost": courseCost,
          "payableAmount": payable,
          "startDate": startDate,
          "endDate": endDate,
          "studentId": studentId,
          "courseId": courseId,
          "frequency": frequency

        }, headers: this.authHeaders
      });
  }
  modifyTransactionDetails(startDate, endDate, paidAmount, studentId, courseId, studentFeeId, studentFeeTransactionId, transactionDate) {
    return this.http.request<any>("PUT",
      this.url + "/enroll/student/transaction/" + studentFeeTransactionId,
      {
        body: {
          "paidAmount": paidAmount,
          "tStartDate": startDate,
          "tEndDate": endDate,
          "stduentCourseFeeId": studentFeeId,
          "studentId": studentId,
          "courseId": courseId,
          "tDate": transactionDate
        }, headers: this.authHeaders
      });
  }
  getFeeAndTransactionDetails(studentId, courseId): Observable<any> {
    return this.http.get(this.url + "/enroll/student/" + studentId + "/" + courseId,
      { headers: this.authHeaders });
  }
  exploreCourseDetails(courseId): Observable<any> {
    return this.http.get(this.url + "/course/details?courseId=" + courseId);
  }
  exploreCourseDetailsForStudents(courseId): Observable<any> {
    return this.http.get(this.url + "/course/details/student?courseId=" + courseId + "&studentId=" + this.comservice.myUserId,
      { headers: this.authHeaders });
  }
  exploreTeacherDetails(teacherId): Observable<any> {
    return this.http.get(this.url + "/teacher/details?teacherId=" + teacherId);
  }
  getCourseWithStudentList(userId, userRole): Observable<any> {
    return this.http.get(this.url + "/student/list/" + userId + "/" + userRole,
      { headers: this.authHeaders });
  }

  changeFirstLoginStatus(userRole, userId) {
    return this.http.get(this.url + "/loginStatus/" + userRole + "/" + userId,
      { headers: this.authHeaders })
  }

  getCoursePaymentStats(): Observable<any> {
    return this.http.get(this.url + "/cc/fee/" + this.comservice.myUserId,
      { headers: this.authHeaders });
  }

  getPaymentDetailsByCourseId(courseId) {
    return this.http.get(this.url + "/cc/fee/details/" + courseId,
      { headers: this.authHeaders });
  }

  getPaymentDetailsByStudentId(studentId): Observable<any> {
    return this.http.get(this.url + "/student/fee/" + studentId,
      { headers: this.authHeaders });
  }
  uploadNotes(fileToUpload: File, courseId, notesTitle): Observable<any> {
    let customhearders = new HttpHeaders({
      'authorization': this.comservice.myAccessToken
    });
    const formData = new FormData();
    formData.append('note', fileToUpload);
    return this.http.post(this.url + "/cc/note/" + courseId + "/" + this.comservice.myUserId + "/" + notesTitle, formData,
      { headers: customhearders });
  }

  updateStudentFromCc(studentInfoToUpdate: any, classId: any, boardId: any): Observable<any> {
    // let ccId = localStorage.getItem("ccId");
    return this.http.put(this.url + "/cc/student/update/" + this.comservice.myUserId + "/" + studentInfoToUpdate.studentId, {
      "studentName": studentInfoToUpdate.studentName,
      "stduentEmail": studentInfoToUpdate.email,
      "classId": classId,
      "address1": studentInfoToUpdate.address1,
      "address2": studentInfoToUpdate.address2,
      "pincode": studentInfoToUpdate.pincode,
      "city": studentInfoToUpdate.city,
      "state": studentInfoToUpdate.state,
      "phoneNo": studentInfoToUpdate.phoneNo,
      "studentPassword": studentInfoToUpdate.studentPassword,
      "boardId": boardId
    }, { headers: this.authHeaders });
  }

  resetOtp(userRole, phoneNumber): Observable<any> {
    return this.http.get(this.url + "/newuser/genrateNewOtp/" + userRole + "/" + phoneNumber + "/" + false)

  }

  getTeacherReport(teacherId, startDate, endDate): Observable<any> {
    return this.http.get(this.url + "/course/report/" + teacherId + "/" + startDate + "/" + endDate,
      { headers: this.authHeaders });
  }


  getStudentReport(studentId, startDate, endDate): Observable<any> {
    return this.http.get(this.url + "/student/course/report/" + studentId + "/" + startDate + "/" + endDate,
      { headers: this.authHeaders });
  }



  getClassBoardSubject(): Observable<any> {
    return this.http.get(this.url + "/class/board/subject",
      { headers: this.authHeaders });
  }

  getCBS(ccId): Observable<any> {
    return this.http.get(this.url + "/cbs/" + ccId,
      { headers: this.authHeaders });
  }



  addSubject(subjectName): Observable<any> {
    return this.http.post(this.url + "/subject", { "subjectName": subjectName }, { headers: this.authHeaders });
  }

  addClass(className): Observable<any> {
    return this.http.post(this.url + "/class", { "className": className }, { headers: this.authHeaders });
  }

  addBoard(boardName): Observable<any> {
    return this.http.post(this.url + "/board", { "boardName": boardName }, { headers: this.authHeaders });
  }

  getBoard(): Observable<any> {
    return this.http.get(this.url + "/board", { headers: this.authHeaders });
  }

  addCBS(classId, boardId, subjectId, ccId): Observable<any> {
    return this.http.post(this.url + "/class/board/subject", {
      "classId": classId,
      "boardId": boardId,
      "subjectIds": subjectId,
      "ccId": ccId
    }, { headers: this.authHeaders });
  }

  getStudentDetails(studentId): Observable<any> {
    return this.http.get(this.url + "/student/details/" + studentId, { headers: this.authHeaders });
  }

  setCcPreferences(payload): Observable<any> {
    return this.http.put(this.url + "/cc/preference", payload, { headers: this.authHeaders });
  }

  addFEL(payload): Observable<any> {
    return this.http.post(this.url + "/class/board", payload, { headers: this.authHeaders })
  }
  buyPackage(): Observable<any> {
    return this.http.post(this.url + "/user/package", {
      "packageId": 3,
      "planId": 1,
      "userId": this.comservice.myUserId,
      "packageType": "Free"
    }, { headers: this.authHeaders });
  }

  unEnrollStudentList(courseId, studentId) {
    return this.http.request<any>("DELETE",
      this.url + "/enroll/course-student",
      {
        body: {
          "courseId": courseId,
          "studentId": studentId
        }, headers: this.authHeaders
      });
  }

  endMeetingForcefully(scheduleClassId): Observable<any> {
    return this.http.request<any>("GET", this.url + "/liveClass/endMeeting?scId=" + scheduleClassId,
      { headers: this.authHeaders })
  }

  deleteRecording(scIdToDeleteArray): Observable<any> {
    return this.http.request<any>("DELETE", this.url + "/liveClass/recording",
      {
        body: {
          "recordingIds": scIdToDeleteArray
        }, headers: this.authHeaders
      }
    )
  }

  getUserPackage(): Observable<any> {
    return this.http.request<any>("GET", this.url + "/cc/usage/" + this.comservice.myUserId,
      { headers: this.authHeaders })
  }
  uploadExcel(fileToUpload: File): Observable<any> {
    let customhearders = new HttpHeaders({
      'authorization': this.comservice.myAccessToken
    });
    const formData = new FormData();
    formData.append('file', fileToUpload);
    return this.http.post(this.url + "/excelData/student/" + this.comservice.myUserId, formData,
      { headers: customhearders });
  }

  deleteSc(scIdToDelete, permissionToDelete): Observable<any> {
    return this.http.request<any>("DELETE", this.url + "/sc-class/" + scIdToDelete + "/" + permissionToDelete,
      { headers: this.authHeaders }
    )
  }
  deleteCourse(courseId,deletePermission,enrollmentPermission): Observable<any> {
    return this.http.request<any>("DELETE", this.url + "/course/" + courseId + "/" + false + "/" +
    enrollmentPermission + "/" + false + "/" + deletePermission,
      { headers: this.authHeaders }
    )
  }

}




