import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { CBS } from './shared/models/CBS.model';
import { formatDate } from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class CommonServiceService {
  isloggedin: boolean;
  firstTimelogged: boolean;
  myUserRole: string;
  myUserId: any;
  myUserName: string;
  myAccessToken: string;
  //teacherClass: any;
  //lat: any;
  myProfilePic: any;
  //techerQualification: string;
  myProfileUrl: any;
  //profileUrl: any;
  ccId: any;
  fcmToken: any;
  exploreHistory: string;
  showNavbar: boolean;
  ccAddress1: any;
  ccAddress2: any;
  ccName: any;
  ccPhone: any;
  studentName: any;
  studentId: any
  courseCost: any;
  courseTitle: any;
  discount: any;
  startDate: any;
  endDate: any;
  amountPaid: any;
  installment: any;
  cbsObj: CBS;
  pendingAmount: any;
  allowAddSchedule: boolean;
  allowCreateCourse: boolean;
  allowEditSchedule: boolean;
  allowEnrollStudent: boolean;
  allowPaymentModification: boolean;
  allowUploadNotes: boolean;
  allowRating: boolean;
  ccPicturePath: any;
  enquiryForCourse: boolean;
  userPreferences: any
  showExploreCoursesAndTeacher: any;
  preference = {
    ccPreference: {
      sendSMSForUpcomingSC: false,
      sendSMSForUpdatedSC: false,
      sendSMSForAddNewSC: false,
      sendSMSForCourseEnrollment: false,
      sendSMSForCourseUpdate: false
    },
    ccTeacherPreference: {
      displayMyRecordings: false,
      allowUploadNotes: false,
      allowAddSchedule: false,
      allowEditSchedule: false,
      allowCreateCourse: false,
      allowPaymentModification: false,
      displayMyStudents: false,
      allowEnrollStudent: false

    },
    ccStudentPreference: {
      displayMyRecordings: false,
      enquiryForCourse: false,
      displayMyPayments: false,
      allowRating: false
    },
    ccLiveClassPreference: {
      welcomeTextValue: "CourseTitle",
      recordingAvailable: false,
      autoStartStopRecording: false,
      maximunParticipant: 0,
      endMeetingUponEndTime: false,
      allowTeacherStartStopRecording: false
    }
  }
  //receipt : Receipt = new Receipt;

  private ccSource = new BehaviorSubject('');
  currentCc = this.ccSource.asObservable();

  private preferencesSource = new BehaviorSubject({} as any);
  currentPreferences = this.ccSource.asObservable();

  changeCcName(ccName: string) {
    this.ccSource.next(ccName)
  }

  changePreferences(preferences: any) {
    this.preferencesSource.next(preferences)
  }

  public getStudents(): any {
    const studentsObservable = new Observable(observer => {
      setTimeout(() => {
        observer.next(this.userPreferences);
      }, 1000);
    });

    return studentsObservable;
  }
  constructor() {
    this.showNavbar = true;
    this.exploreHistory = localStorage.getItem("exploreHistory");
    this.myUserId = parseInt(localStorage.getItem("userId"));
    this.myUserName = localStorage.getItem("username");
    this.myUserRole = localStorage.getItem("role");
    this.myAccessToken = localStorage.getItem("access_token");
    this.myProfilePic = localStorage.getItem("myProfilePic");
    this.myProfileUrl = localStorage.getItem("myProfileUrl");
    this.ccAddress1 = localStorage.getItem("ccAddress1");
    this.ccAddress2 = localStorage.getItem("ccAddress2");
    this.ccName = localStorage.getItem("ccName");
    this.ccId = localStorage.getItem("ccId");
    this.ccPhone = localStorage.getItem("ccPhone");
    this.studentName = localStorage.getItem("studentName");
    this.studentId = localStorage.getItem("studentId");
    this.courseCost = localStorage.getItem("courseCost");
    this.courseTitle = localStorage.getItem("courseTitle");
    this.discount = localStorage.getItem("discount");
    this.startDate = localStorage.getItem("startDate");
    this.endDate = localStorage.getItem("endate");
    this.amountPaid = localStorage.getItem("amountPaid");
    this.pendingAmount = localStorage.getItem("pendingAmount");
    this.installment = localStorage.getItem("installment");
    this.fcmToken = localStorage.getItem("FCMtoken");
    this.ccId = localStorage.getItem("ccId");
    this.ccPicturePath = localStorage.getItem("ccPicturePath");
    //this.isRestrictedAccess = localStorage.getItem("isRestrictedAccess");
    if (localStorage.getItem("isloggedin") == 'true' || localStorage.getItem("isloggedin") == "true") {
      this.isloggedin = true;
    }
    // this.profileUrl = "/profile";
  }



  getCCInfo() {
    this.ccAddress1 = localStorage.getItem("ccAddress1");
    this.ccAddress2 = localStorage.getItem("ccAddress2");
    this.ccName = localStorage.getItem("ccName");
    this.ccId = localStorage.getItem("ccId");
    this.ccPhone = localStorage.getItem("ccPhone");
  }

  removeCcInfo() {
    localStorage.removeItem("ccId");
    localStorage.removeItem("ccAddress1");
    localStorage.removeItem("ccAddress2");
    localStorage.removeItem("ccName");
    localStorage.removeItem("ccPhone");
  }

  userLoggedIn(myUserRole, myUserId, myUserName, myAccessToken, myProfileUrl) {

    this.myUserId = myUserId;
    this.myUserRole = myUserRole;
    this.myUserName = myUserName
    this.myAccessToken = myAccessToken;
    this.isloggedin = true;
    this.myProfileUrl = myProfileUrl;
    localStorage.setItem("userId", myUserId);
    localStorage.setItem("username", myUserName);
    localStorage.setItem("role", myUserRole);
    localStorage.setItem("access_token", myAccessToken);
    localStorage.setItem("myProfileUrl", myProfileUrl);
    localStorage.setItem("isloggedin", "true");

  }
  userProfilePic(myProfilePic) {
    this.myProfilePic = myProfilePic;
    localStorage.setItem("myProfilePic", myProfilePic);
  }

  firstLogin() {
    let firstLogin = "true";
    localStorage.setItem("firstLogin", firstLogin);
  }

  userLoggedOut() {
    this.myUserRole = "";
    this.myUserId = null;
    this.isloggedin = false;
    this.myUserName = "";
    localStorage.removeItem("userId");
    localStorage.removeItem("role");
    localStorage.removeItem("isloggedin");
    localStorage.removeItem("username");
    localStorage.removeItem("access_token");
    localStorage.removeItem("myProfileUrl");
    //localStorage.removeItem("isRestrictedAccess");
    localStorage.removeItem("FCMtoken");
    localStorage.removeItem("firstLogin");
    localStorage.removeItem("ccId");
    localStorage.removeItem("ccAddress1");
    localStorage.removeItem("ccAddress2");
    localStorage.removeItem("ccName");
    localStorage.removeItem("ccPhone");
    localStorage.removeItem("studentName");
    localStorage.removeItem("studentId");
    localStorage.removeItem("courseCost");
    localStorage.removeItem("courseTitle");
    localStorage.removeItem("discount");
    localStorage.removeItem("startDate");
    localStorage.removeItem("endate");
    localStorage.removeItem("amountPaid");
    localStorage.removeItem("installment");
  }

  isUserLoggedIn(): boolean {
    return this.isloggedin;
  }

  setUserLoggedIn(userLoggedIn: boolean) {
    this.isloggedin = userLoggedIn;
  }

  ccDetails(ccAddress1, ccAddress2, ccName, ccPhone, ccId, ccPicturePath) {
    this.ccAddress1 = ccAddress1;
    this.ccAddress2 = ccAddress2;
    this.ccName = ccName;
    this.ccPhone = ccPhone;
    this.ccId = ccId;
    this.ccPicturePath = ccPicturePath;
    localStorage.setItem("ccAddress1", ccAddress1);
    localStorage.setItem("ccAddress2", ccAddress2);
    localStorage.setItem("ccName", ccName);
    localStorage.setItem("ccPhone", ccPhone);
    localStorage.setItem("ccId", ccId);
    localStorage.setItem("ccPicturePath", ccPicturePath);

    //this.receipt = new Receipt("sjdn");
  }

  formatFrequencyForExplore(freq) {
    if (freq != null) {
      var splitfreq = freq.split(",");
      var formttedfreq = "";
      for (var i = 0; i < splitfreq.length; i++) {
        switch (splitfreq[i]) {
          case "Monday": {
            formttedfreq += "Mon ";
            break;
          }
          case "Tuesday": {
            formttedfreq += "Tue ";
            break;
          }
          case "Wednesday": {
            formttedfreq += "Wed ";
            break;
          }
          case "Thursday": {
            formttedfreq += "Thu ";
            break;
          }
          case "Friday": {
            formttedfreq += "Fri ";
            break;
          }
          case "Saturday": {
            formttedfreq += "Sat ";
            break;
          }
          case "Sunday": {
            formttedfreq += "Sun ";
            break;
          }
          default: {
            break;
          }
        }
        ;
      }
      return formttedfreq;
    }
  }


  ccTeacherPreference(allowAddSchedule, allowCreateCourse, allowEditSchedule, allowEnrollStudent,
    allowPaymentModification, allowUploadNotes, showExploreCoursesAndTeacher) {

    this.allowAddSchedule = allowAddSchedule;
    this.allowCreateCourse = allowCreateCourse;
    this.allowEditSchedule = allowEditSchedule;
    this.allowEnrollStudent = allowEnrollStudent;
    this.allowPaymentModification = allowPaymentModification;
    this.allowUploadNotes = allowUploadNotes;
    this.showExploreCoursesAndTeacher = showExploreCoursesAndTeacher;

  }

  ccStudentPreference(allowRating, enquiryForCourse, showExploreCoursesAndTeacher) {
    this.allowRating = allowRating;
    this.enquiryForCourse = enquiryForCourse;
    this.showExploreCoursesAndTeacher = showExploreCoursesAndTeacher;
  }

  convert12To24HourFormat(time): string {
    if (time) {
      let timesplit = time.split(" ");
      if (timesplit[1] === "AM") {
        let time12split = timesplit[0].split(":");
        if (time12split[0] == "12") {
          time12split[0] = "00";
          return time12split[0] + ":" + time12split[1];
        }
        if (time12split[0] != "12") {
          return timesplit[0];
        }
      }
      if (timesplit[1] === "PM") {
        let splittime = timesplit[0].split(":");
        let newtime = "";
        if (splittime[0] < 12) {
          newtime += (parseInt(splittime[0]) + 12) + ":" + splittime[1];
        }
        else {
          newtime += (parseInt(splittime[0])) + ":" + splittime[1];
        }
        return newtime;
      }
    }
  }

  getFirstOfMonth(date) {
    let sd: Date = new Date(date.getFullYear(), date.getMonth(), 1);
    return formatDate(sd, "yyyy-MM-dd", "en-US");
  }
  getLastOfMonth(date) {
    let ed: Date = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    return formatDate(ed, "yyyy-MM-dd", "en-US");
  }

  compareNumberAndChar(a, b) {
    var ax = [], bx = [];
    a.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { ax.push([$1 || Infinity, $2 || ""]) });
    b.replace(/(\d+)|(\D+)/g, function (_, $1, $2) { bx.push([$1 || Infinity, $2 || ""]) });
    while (ax.length && bx.length) {
      var an = ax.shift();
      var bn = bx.shift();
      var nn = (an[0] - bn[0]) || an[1].localeCompare(bn[1]);
      if (nn) return nn;
    }
    return ax.length - bx.length;
 }


 sortCharArray(x,y){
  let a = x.toUpperCase(),
  b = y.toUpperCase();
  return a == b ? 0 : a > b ? 1 : -1;
 }


}


